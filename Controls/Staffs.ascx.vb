﻿Imports DevExpress.Web
Partial Class Controls_Staffs
    Inherits System.Web.UI.UserControl
    Public Event SelectedItemChanged(ByVal newstudent As Enrollment.Staff)
    Protected Sub combo1_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)
        Dim data = (New Cls_staffs).SelectAllFilter(e.Filter)
        Me.FillDropPartyIDGrid(data)
    End Sub

    Protected Sub combo1_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        If Not String.IsNullOrEmpty(e.Value) Then
            Dim data = (New Cls_staffs).SelectThisByRefstaffid(e.Value)
            Me.FillDropPartyIDGrid(data)
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack = False Then
            Dim data = (New Cls_staffs).SelectAllstaffs()
            Me.FillDropPartyIDGrid(data)
        End If
    End Sub

    Private Sub FillDropPartyIDGrid(ByVal data As List(Of Enrollment.Staff))
        With Me.combo1
            .DataSource = data
            .Columns.Clear()
            .Columns.Add("SN").Width = 120
            .Columns.Add("Name").Width = 170
            .Columns.Add("Subject").Width = 120
            .ValueField = "SN"
            .TextField = "Name"
            .TextFormatString = "{1}"
            .DataBind()
        End With
    End Sub

    Public Property SelectedValue() As String
        Get
            Return Me.combo1.Value
        End Get
        Set(ByVal value As String)
            Me.combo1.Value = value
        End Set
    End Property

    Public Property SelectedText() As String
        Get
            Return Me.combo1.Text
        End Get
        Set(ByVal value As String)
            Me.combo1.Text = value
        End Set
    End Property
End Class
