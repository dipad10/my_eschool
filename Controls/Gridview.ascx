﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Gridview.ascx.vb" Inherits="Controls_Gridview" %>
<asp:GridView ID="GridView1" CssClass="Grid2 text-secondary table striped cell-hovered hovered"  Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                   <Columns>
                         <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                       <asp:HyperLinkField HeaderText="Student No." DataNavigateUrlFields="IDNo" DataTextField="IDNo"
                    DataNavigateUrlFormatString="/Account/AddCertificate.aspx?edit-id={0}"/>
                       <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                       <asp:BoundField DataField="Gender" HeaderText="Gender" SortExpression="Gender" />
                       <asp:BoundField DataField="AdmissionDate" DataFormatString="{0:dd MMM yyyy}" HeaderText="AdmissionDate" SortExpression="AdmissionDate" />
                       <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
             
                  
                   
            
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-secondary" Height="30px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>