﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Controls_Navbar
    Inherits System.Web.UI.UserControl
    Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uname") = "" Then
            Response.Redirect("/Login.aspx")
            Exit Sub

        Else
            Dim userrec As Enrollment.SecUser = (New Cls_secusers).SelectThisID(Session(("uname")))
            Select Case userrec.permission
                Case "Admin"
                    moduleaccounts.Visible = True
                    moduleadmin.Visible = True

                    modulehostel.Visible = True
                    modulehr.Visible = True
                    modulereports.Visible = True
                    modulestudent.Visible = True
                    moduletransport.Visible = True

                Case "Accountant"
                    moduleaccounts.Visible = True
                    moduleadmin.Visible = False

                    modulehostel.Visible = False
                    modulehr.Visible = True
                    modulereports.Visible = True
                    modulestudent.Visible = False
                    moduletransport.Visible = False
                Case "Registrar"
                    moduleaccounts.Visible = False
                    moduleadmin.Visible = False

                    modulehostel.Visible = True
                    modulehr.Visible = True
                    modulereports.Visible = True
                    modulestudent.Visible = True
                    moduletransport.Visible = True

            End Select

            If Date.Now.Hour < 12 Then
                welcome.InnerText = "Good Morning, " & Session("uname").ToString() & ""

            ElseIf Date.Now.Hour < 17 Then
                welcome.InnerText = "Good Afternoon, " & Session("uname").ToString() & ""
            Else
                welcome.InnerText = "Good Evening, " & Session("uname").ToString() & ""


            End If
        End If
       




        '_Connection.Open()
        'Dim _cmd As SqlCommand = _Connection.CreateCommand()
        '_cmd.CommandType = CommandType.Text
        '_cmd.CommandText = "select * from SecUsers where Username=( '" + Session("uname") + "') and Password=( '" + Session("password") + "')"
        '_cmd.ExecuteNonQuery()
        'Dim dr As SqlDataReader
        'dr = _cmd.ExecuteReader()
        'If dr.Read() Then


        '    If dr("permission").ToString() = "Admin" Then



        '    ElseIf dr("permission").ToString() = "Accountant" Then


        '    End If
        'Else
        '    Response.Write("<script>alert('Invalid username or Password');</script>")
        'End If

        '_Connection.Close()
        '_Connection.Dispose()
        'dr.Close()




    End Sub
End Class
