﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Navbar.ascx.vb" Inherits="Controls_Navbar" %>
<header class="app-bar fixed-top bg-darkCobalt" data-role="appbar">
    <%--<div class="container">--%>
        <a href="/" class="app-bar-element branding"><img src="/images/wn8.png" style="height: 28px; display: inline-block; margin-right: 10px;"> E-school</a>

   
    <ul class="app-bar-menu small-dropdown">
            <li runat="server" id="Dashboard1" data-flexorderorigin="12" data-flexorder="13">
                <a href="/Admin/Dashboard.aspx" class=""><span class="text-secondary text-bold">Dashboard</span></a>
                
            </li>
            <li id="modulestudent" runat="server" data-flexorderorigin="1" data-flexorder="2" class="">
                <a href="#" class="dropdown-toggle text-secondary"><span class="mif-school icon text-secondary text-bold">Students</span></a>
                <ul class="d-menu" data-role="dropdown" data-no-close="true" style="display: none;">
                    <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary">Student's Registration</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/Students_edit.aspx">Create New student</a></li>
                            
                        </ul>
                    </li>
                   <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/Students.aspx">Student's list</a></li>
                   <li class="text-secondary text-bold"><a href="/Admin/StudentReg/SubjectsAllocation.aspx">Subject Allocations</a></li>
                     <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Grade_computation.aspx">Grade Computations</a></li>
                     <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary">Setups</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/Subjects.aspx">Setup subjects</a></li>
                         <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/classes.aspx">Setup classes</a></li>
                             <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/classes.aspx">Setup Session Year</a></li>
                             <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/Subjects.aspx">Set Active Session</a></li>
                         <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/StudentReg/classes.aspx">Set Active Term</a></li>

                        </ul>
                    </li>
                 </ul>
                   </li>
                  
            <li id="moduleaccounts" runat="server" data-flexorderorigin="2" data-flexorder="3" class="">
                <a href="#" class="dropdown-toggle"><span class="icon mif-coins text-secondary text-bold">Accounts</span></a>
                <ul class="d-menu" data-role="dropdown" data-no-close="true" style="display: none;">
                    <li class="text-secondary text-bold"><a href="/Admin/Accounts/Receivables.aspx">Receivables</a></li>
                    <li class="text-secondary text-bold"><a href="/Admin/Accounts/Payables.aspx">Payables</a></li>
                    <li class="text-secondary text-bold"><a href="/Admin/Accounts/ledger_journals.aspx">General Journals</a></li>
                    <li class="text-secondary text-bold"><a href="templates/admin-sidebar-appbar.html">Invoices</a></li>
                       <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary">Assets Mgt</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                              
                              <li class="text-secondary text-bold"><a href="/Admin/Accounts/Asset_Purchases.aspx">Asset Purchases</a></li>
                    <li class="text-secondary text-bold"><a href="/Admin/Accounts/Asset_Disposals.aspx">Asset Disposal</a></li>
                     <li class="text-secondary text-bold"><a href="/Admin/Accounts/Depreciation_run.aspx">Depreciation run</a></li>
                    <li class="text-secondary text-bold"><a href="/Admin/Accounts/Depreciation_value.aspx">Depreciated Value</a></li>
                     <li class="text-secondary text-bold"><a href="/Admin/Accounts/Asset_Transfers.aspx">Asset transfers</a></li>
                        </ul>
                    </li>
                <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary">Maintainance</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a class="text-secondary" href="/Admin/Accounts/chart_of_accounts.aspx">Chart of Accounts</a></li>                           
                        </ul>
                    </li>
                     </ul>
            </li>

             <li runat="server" id="modulehr" data-flexorderorigin="4" data-flexorder="5">
                <a href="#" class="dropdown-toggle"><span class="icon mif-users text-secondary text-bold">Human Resources</span></a>
                <ul class="d-menu" data-role="dropdown" data-no-close="true">
                     <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary text-bold">Employees</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Educational Background</a></li>
                        </ul>
                    </li>
                   <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle">Process Payslips</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Payslips</a></li>
                        </ul>
                    </li>
                     <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary text-bold">Setups</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Allowances</a></li>
                             <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Deductions</a></li>
                             <li class="text-secondary  text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Paysheets</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

             <li id="modulehostel" runat="server" data-flexorderorigin="6" data-flexorder="7">
                <a href="#" class="dropdown-toggle text-secondary text-bold"><span class="icon mif-hotel text-secondary text-bold">Hostel</span></a>
                <ul class="d-menu" data-role="dropdown" data-no-close="true">
                     <li class="text-secondary">
                        <a href="#" class="dropdown-toggle text-secondary text-bold">Employees</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Educational Background</a></li>
                        </ul>
                    </li>
                   <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle">Process Payslips</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Payslips</a></li>
                        </ul>
                    </li>
                     <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle text-secondary text-bold">Setups</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Allowances</a></li>
                             <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Deductions</a></li>
                             <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Paysheets</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
             <li runat="server" id="moduletransport" data-flexorderorigin="8" data-flexorder="9">
                <a href="#" class="dropdown-toggle"><span class="icon mif-drive-eta text-secondary text-bold">Transport</span></a> 
                <ul class="d-menu" data-role="dropdown" data-no-close="true">
                     <li class="text-secondary">
                        <a href="#" class="dropdown-toggle text-secondary text-bold">Employees</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Educational Background</a></li>
                        </ul>
                    </li>
                   <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle">Process Payslips</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li><a href="/Admin/StudentReg/Students_edit.aspx">Payslips</a></li>
                        </ul>
                    </li>
                     <li class="text-secondary text-bold">
                        <a href="#" class="dropdown-toggle">Setups</a>
                        <ul class="d-menu" data-role="dropdown" data-no-close="true">
                            <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Allowances</a></li>
                             <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Deductions</a></li>
                             <li class="text-secondary text-bold"><a href="/Admin/StudentReg/Students_edit.aspx">Paysheets</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li id="modulereports" runat="server" data-flexorderorigin="10" data-flexorder="11">
                <a href="/Admin/Report/Reports.aspx" class=""><span class="icon mif-cabinet text-secondary text-bold">Reports</span></a>
              
            </li>

            <li runat="server" id="moduleadmin" data-flexorderorigin="12" data-flexorder="13">
                <a href="/Admin/Administrator/Home.aspx"class=""><span class="icon mif-tools text-secondary text-bold">Admin</span></a>
                
            </li>
           
        </ul>
    <div class="app-bar-element place-right">
        <a class=" fg-white"><span class="mif-bell"></span></a>
        <div style="width: 220px;" class="app-bar-drop-container bg-white fg-dark place-right"
                data-role="dropdown" data-no-close="true">
            <div class="padding10">
               
                   <ul class="unstyled-list fg-dark">
                    <li><a href="#" class="fg-white1 text-secondary text-bold fg-hover-yellow">Status</a></li>
                    <li class="text-small"><a href="#" class="text-small"></a>ifeoma updated receipt RCP/10/2012</li>
                    <li><a href="/Login.aspx?msg=logoff" class="fg-white3 text-secondary text-bold fg-hover-yellow">Logout</a></li>
                </ul>
               
            </div>
        </div>
    </div>
        <div class="app-bar-element place-right">
            <span id="welcome" runat="server" class="dropdown-toggle text-secondary text-bold"><span class="mif-user"></span> </span>
            <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" style="width: 220px;">
        
                <ul class="unstyled-list fg-dark">
                    <li><a href="#" class="fg-white1 text-secondary text-bold fg-hover-yellow">Profile</a></li>
                    <li><a href="#" class="fg-white2 text-secondary text-bold fg-hover-yellow">Security</a></li>
                    <li><a href="/Login.aspx?msg=logoff"  class="fg-white3 text-secondary text-bold fg-hover-yellow">Logout</a></li>
                </ul>
            </div>
        </div>
        <span class="app-bar-pull"></span>
    
    <div class="app-bar-pullbutton automatic" style="display: none;"></div><div class="clearfix" style="width: 0;"></div><nav class="app-bar-pullmenu hidden flexstyle-app-bar-menu" style="display: none;"><ul class="app-bar-pullmenubar hidden app-bar-menu"></ul></nav><%--</div>--%>
       
</header>


