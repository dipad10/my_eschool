﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="Students_edit.aspx.vb" Inherits="Admin_StudentReg_Students_edit" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<%--<%@ Register src="../../Controls/Scanner.ascx" tagname="Scanner" tagprefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
    <script type="text/javascript">

        function SlamPix(imgId, fileId) {
            var Image1 = document.getElementById(imgId);
            var selFilePath = document.getElementsByName(fileId)[0].value;

            if (selFilePath == '') {
                Image1.src = "/images/nophoto.jpg";
            } else {
                Image1.src = "/images/nophoto.jpg";
            }
            //alert(document.getElementsByName(fileId)[0].value);
        }
</script>
           <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" ConfirmText="Are you sure you want to Save this Record" TargetControlID="btnsavepersonal" runat="server" />
               <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" ConfirmText="Are you sure you want to Save this Document(s)" TargetControlID="btnsavedocuments" runat="server" />
               <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender3" ConfirmText="Are you sure you want to Save this Record" TargetControlID="btnsaveparent" runat="server" />
            

   <h4 runat="server" id="addnew" class="title">Add New student</h4>
          <h4 runat="server" id="profilename" visible="false" class="title">Student Profile (<asp:Label ID="lblstudentname"  Font-Names="Ebrima" runat="server" CssClass="text-bold fg-darkBlue uppercase title"></asp:Label>, <asp:Label ID="lblregno" CssClass="text-bold fg-darkBlue title" runat="server" Font-Names="Ebrima"></asp:Label>)</h4>

<table>
    <tr>
        <td>

            <asp:Button Text="Personal Details" ID="Tab1"  runat="server"
                     /></td>
        <td>
             <asp:Button Text="Parent Details" ID="Tab2" OnClick="Tab2_Click" runat="server"
                     />
        </td>
        <td>
             <asp:Button Text="Documents" BorderStyle="None" ID="Tab3" OnClick="Tab3_Click" runat="server"
                     />
        </td>

        <td>
             <asp:Button Text="Course Reg" BorderStyle="None" OnClick="Tab4_Click" ID="Tab4" runat="server" />
        </td>
    </tr>
</table>
    <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                      <div data-role="panel" class="panel">
    
    <div class="content">
        <div class="grid">
            <div class="row cells3">
                <div class="cell">
                    <span>
                        <asp:TextBox ID="txtIDno" BorderColor="Orange" Visible="false" runat="server"></asp:TextBox></span>
                    <p class="text-secondary text-bold">Firstname*</p>
                    <span><asp:TextBox ID="txtfirstname" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>     
                    
                  <p class="text-secondary text-bold">Date of Birth*</p>
                    <span> 
                         <dx:ASPxDateEdit ID="dateEdit" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Date="2009-11-02 09:23" Width="200">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>

                    </span>

                    <p class="text-secondary text-bold">Gender*</p>
                    <span><asp:DropDownList ID="ddlgender" BorderColor="Orange" Width="200px" runat="server">
                            <asp:ListItem>Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                        </asp:DropDownList>      </span>
                                 
                <p class="text-secondary text-bold">State*</p>

                <span><asp:DropDownList ID="ddlstate" BorderColor="Orange" Width="200px" runat="server">
                            <asp:ListItem Selected="True" Value="null">--select state--</asp:ListItem>
                            <asp:ListItem> Abia State</asp:ListItem>
 <asp:ListItem> Adamawa State</asp:ListItem>
<asp:ListItem>Akwa Ibom State</asp:ListItem>
<asp:ListItem>Anambra State</asp:ListItem>
  <asp:ListItem>Bauchi State</asp:ListItem>
 <asp:ListItem>Bayelsa State</asp:ListItem>
<asp:ListItem>Benue State</asp:ListItem>
<asp:ListItem>Borno State</asp:ListItem>
  <asp:ListItem>Cross River State</asp:ListItem>
 <asp:ListItem>Delta State</asp:ListItem>
<asp:ListItem>Ebonyi State</asp:ListItem>
 <asp:ListItem>Edo State</asp:ListItem>
 <asp:ListItem>Ekiti State</asp:ListItem>
<asp:ListItem>Enugu State</asp:ListItem>
<asp:ListItem>Gombe State</asp:ListItem>
  <asp:ListItem>Imo State</asp:ListItem>
 <asp:ListItem>Jigawa State</asp:ListItem>
<asp:ListItem>Kaduna State</asp:ListItem>
<asp:ListItem>Kano State</asp:ListItem>
  <asp:ListItem>Katsina State</asp:ListItem>
 <asp:ListItem>Kebbi State</asp:ListItem>
<asp:ListItem>Kogi State</asp:ListItem>
 <asp:ListItem>Kwara State</asp:ListItem>
<asp:ListItem>Lagos State</asp:ListItem>
<asp:ListItem>Nasarawa State</asp:ListItem>
  <asp:ListItem>Niger State</asp:ListItem>
 <asp:ListItem>Ogun State</asp:ListItem>
<asp:ListItem>Ondo State</asp:ListItem>
<asp:ListItem>Osun State</asp:ListItem>
  <asp:ListItem>Oyo State</asp:ListItem>
 <asp:ListItem>Plateau State</asp:ListItem>
<asp:ListItem>Rivers State</asp:ListItem>
 <asp:ListItem>Sokoto State</asp:ListItem>
 <asp:ListItem>Taraba State</asp:ListItem>
<asp:ListItem>Yobe State</asp:ListItem>
<asp:ListItem>Zamfara State</asp:ListItem>
  <asp:ListItem>Abuja</asp:ListItem>
                        </asp:DropDownList></span>


                       <p class="text-secondary text-bold">LGA*</p>
                    <span><asp:TextBox ID="txtlga" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                </div>


                <div class="cell">
                    <p class="text-secondary text-bold">Middlename*</p>
                   <span><asp:TextBox ID="txtmiddlename" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                    <p class="text-secondary text-bold">Admission Date*</p>
                     <span> 
                         <dx:ASPxDateEdit ID="txtadmissiondate" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Date="2009-11-02 09:23" Width="200">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>

                    </span>
                        <p class="text-secondary text-bold">Religion*</p>
                   <span><asp:DropDownList ID="ddlreligion" BorderWidth="1" BorderColor="Orange" Width="200px" runat="server">
                            <asp:ListItem>Muslim</asp:ListItem>
                            <asp:ListItem>Christainity</asp:ListItem>
                    
                        </asp:DropDownList> </span>
                    <p class="text-secondary text-bold">Place of Birth*</p>
                    <span><asp:TextBox ID="txtpob" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                   <p class="text-secondary text-bold">Blood Group*</p>
                    <span><asp:TextBox ID="txtbloodgroup" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                </div>


                <div class="cell">
                    <p class="text-secondary text-bold">Lastname*</p>
                   <span><asp:TextBox ID="txtlastname" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                     <p class="text-secondary text-bold">Session*</p>
                   <span><asp:DropDownList ID="ddlsession" BorderWidth="1" BorderColor="Orange" Width="200px" runat="server">
                            <asp:ListItem>2010/2011</asp:ListItem>
                            <asp:ListItem>2011/2012</asp:ListItem>
                       <asp:ListItem>2012/2013</asp:ListItem>
                        <asp:ListItem>2013/2014</asp:ListItem>
                        <asp:ListItem>2014/2015</asp:ListItem>
                        <asp:ListItem>2015/2016</asp:ListItem>
                        <asp:ListItem>2016/2017</asp:ListItem>
                          <asp:ListItem>2017/2018</asp:ListItem>
                          <asp:ListItem>2018/2019</asp:ListItem>
                          <asp:ListItem>2019/2020</asp:ListItem>
                          <asp:ListItem>2020/2021</asp:ListItem>
                        </asp:DropDownList> </span>
                    <p class="text-secondary text-bold">Address*</p>
                   <span><asp:TextBox ID="txtaddress" BorderWidth="1" CssClass="" TextMode="MultiLine" BorderColor="Orange" runat="server"></asp:TextBox></span>
                    <p class="text-secondary text-bold">Nationality*</p>
                    <span><asp:TextBox ID="txtnationality" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                  
                    <p class="text-secondary text-bold"></p>
                    <span><asp:CheckBox ID="chkboarding" Text="Boarding" CssClass="text-secondary text-bold" TextAlign="Right" runat="server" /></span>
                  
                    
                    
                </div>
            </div>
        </div>
        <asp:Button ID="btnsavepersonal" runat="server" OnClick="btnsavepersonal_Click" CssClass="mini-button button fg-white bg-darkBlue" Height="29px" Text="Save & Continue" />
    </div>
</div>
                    </asp:View>


                    <asp:View ID="View2" runat="server">
                        <div data-role="panel" class="panel">
    <div class="content">
        <div class="grid">
            <div class="row cell">
                <span class="text-bold title">
                    STUDENT NO : <asp:Label ID="lblstudentno" runat="server" Text=""></asp:Label></span>
            </div>
        </div>
        <div class="grid">
            <div class="row cells3">
                <div class="cell">
                   
                    <p class="text-secondary text-bold">First name*</p>
                    <span><asp:TextBox ID="txtparentfirst" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>     
                   
                
              <p class="text-secondary text-bold">Education*</p>
                    <span><asp:TextBox ID="Education" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span> 
          
           <p class="text-secondary text-bold">Address*</p>
                    <span><asp:TextBox ID="txtparentaddress" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>                                                   
  
                </div>
                <div class="cell">
                    <p class="text-secondary text-bold">Lastname*</p>
                   <span><asp:TextBox ID="txtparentlast" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                     <p class="text-secondary text-bold">Occupation*</p>


                    <span><asp:TextBox ID="Occupation" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span> 
                    <p class="text-secondary text-bold">Mobilephone*</p>
                   <span><asp:TextBox ID="Mobilephone" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>

                </div>
                <div class="cell">
                    <p class="text-secondary text-bold">Relation*</p>
                   <span><asp:DropDownList ID="ddlrelation" BorderWidth="1" BorderColor="Orange" Width="200px" runat="server">
                            <asp:ListItem>Father</asp:ListItem>
                            <asp:ListItem>Mother</asp:ListItem>
                       <asp:ListItem>Others</asp:ListItem>
                        </asp:DropDownList> </span>
                   
                       <p class="text-secondary text-bold">Email*</p>
                    <span><asp:TextBox ID="txtemail" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>    
                   <p class="text-secondary text-bold">Office phone</p>
                   <span><asp:TextBox ID="txtofficephone" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                </div>
            </div>
        </div>
        <asp:Button ID="btnsaveparent" runat="server" OnClick="btnsaveparent_Click" CssClass="mini-button button fg-white bg-darkBlue" Height="29px" Text="Save & Continue" />




    </div>
</div>
                    </asp:View>


                    <asp:View ID="View3" runat="server">
                        <div data-role="panel" class="panel">
    
    <div class="content">
          <div class="grid">
            <div class="row cell">
               <span class="text-bold title">
                    STUDENT NO : <asp:Label ID="lblstudentname4" runat="server" Text=""></asp:Label></span>
            </div>
        </div>
        <div class="grid">
            <div class="row cell">
                <p class="text-secondary text-bold">Document:</p>
                    <span>
                        <asp:DropDownList ID="DropDownList1" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" BorderWidth="1" BorderColor="Orange" Width="200" runat="server">
                          
                            <asp:ListItem Enabled="true" Value="Picture">Student photo</asp:ListItem>
                        <asp:ListItem Value="BirthCertificate">Birthcertificate</asp:ListItem>
                            <asp:ListItem Value="ClearanceForm">Clearance form</asp:ListItem>
                    </asp:DropDownList>
                    </span>
                      <p class="text-secondary text-bold">Upload file:</p>
                    <span>
                     
                        <br />

                   <%-- <img id="Img1" height="150" width="150" alt="" runat="server" src="/webroot/web/modules/individual/photos/no-photo.jpg"/>--%>
                <asp:Image ID="Image1" runat="server" BorderWidth="1" BorderColor="DarkOrange" Width="100" Height="100" />

   
<br />
<input id="File1" type="file" size="4" name="File1" runat="server"/>
                    </span>
               
                </div>
             
                
            </div>
                      <asp:Button ID="btnsaveonly" runat="server" OnClick="btnsaveonly_Click" CssClass="mini-button button fg-white bg-darkBlue" Height="29px" Text="Save" />

              <asp:Button ID="btnsavedocuments" runat="server" OnClick="btnsave_Click" CssClass="mini-button button fg-white bg-darkBlue" Height="29px" Text="Save and Goto Student Profile" />
 
    <button runat="server" id="btnskipdoc" onserverclick="btnskipdoc_ServerClick" class="mini-button button fg-white place-right bg-darkBlue"><span class="mif-arrow-right"></span> Skip</button>

    </div>
</div>
                    </asp:View>

        <asp:View ID="View4" runat="server">
                        <div data-role="panel" class="panel">
    
    <div class="content">
          <div class="grid">
            <div class="row cell">
               <span class="text-bold title">
                    STUDENT NO : <asp:Label ID="lblstudentno4" runat="server" Text=""></asp:Label></span>
            </div>
        </div>
        <asp:Panel ID="pnllistcourses" Visible="false" runat="server">
            
               <asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                        
                         <asp:HyperLinkField HeaderText="CourseID" DataNavigateUrlFields="SN" DataTextField="SN"
                    DataNavigateUrlFormatString="Add_course_edit.aspx?courseID={0}">

                     
                         <ItemStyle CssClass="fg-red" />
                         </asp:HyperLinkField>

                     
                       <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname" />
                       <asp:BoundField DataField="LastName" HeaderText="Lastname" SortExpression="Lastname" />
                       <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Status" />
              <asp:BoundField DataField="Term" HeaderText="Term" SortExpression="Term" />
                   <asp:BoundField DataField="cousesdone" HeaderText="Courses Registered" SortExpression="coursesdone" />
                   
            
                          </Columns>
                 <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
        </asp:Panel>
        <div>
        <asp:Button ID="cmdnewcourse" OnClick="cmdnewcourse_Click" CssClass="bg-lightGreen button button-shadow fg-white text-secondary" Height="24px" runat="server" Text="Add new course" />

        </div>
</div>
            </div>
                    </asp:View>
                </asp:MultiView>


</asp:Content>

