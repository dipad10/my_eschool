﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="Student_profile.aspx.vb" Inherits="Admin_StudentReg_Student_profile" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <%-- for image update path--%>
       <script type="text/javascript">

           function SlamPix(imgId, fileId) {
               var Image1 = document.getElementById(imgId);
               var selFilePath = document.getElementsByName(fileId)[0].value;

               if (selFilePath == '') {
                   Image1.src = "/Admin/StudentReg/photos/no-photo.jpg";
               } else {
                   Image1.src = "/Admin/StudentReg/photos/sel-photo.jpg";
               }
               //alert(document.getElementsByName(fileId)[0].value);
           }



</script>
    <style type="text/css">
          .ajax__tab_xp .ajax__tab_header {
              font-family: "Helvetica Neue", Arial, Sans-Serif;
              font-size: 12px;
              font-weight: bold;
              display: block;
          }

      
      </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
      <h4 class="title">Student Profile (<asp:Label ID="lblstudentname"  Font-Names="Ebrima" runat="server" CssClass="text-bold fg-darkBlue uppercase title"></asp:Label>, <asp:Label ID="lblregno" CssClass="text-bold fg-darkBlue title" runat="server" Font-Names="Ebrima"></asp:Label>)</h4>

    <ajaxToolkit:TabContainer ID="TabContainer1" ActiveTabIndex="1" Width="100%" BorderWidth="0" runat="server">
      
         <ajaxToolkit:TabPanel runat="server" HeaderText="Personal Details" ID="TabPanel1">
             <ContentTemplate>
                 <div style="z-index:999;" class="panel" id="panel">
       
    <div class="content">
        <table width="100%">
            <tr height="50px">
                 <td>
 <span class="text-small text-bold">Student No:</span>
                </td>
                <td>
                      <span>
                       <asp:TextBox ID="txtstudentno" ReadOnly="true" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                </td>
              
                  <td>
                    
                </td>
               <td>
                                       <asp:Image ID="Image1" BorderWidth="1" BorderColor="DarkOrange" Width="100" Height="100" runat="server" />
                   <br />
                       <input id="File1" type="file" size="4" name="File1" runat="server"/>

                  
               </td>
               <td>

               </td>
            </tr>

             <tr height="50px">
                <td>
 <span class="text-small text-bold">Firstname:</span>
                  
                </td>
                <td>
                     <span>
                       <asp:TextBox ID="txtfirstname" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                </td>
                <td>
                     <span class="text-small text-bold">Lastname:</span>
                  
                   
                </td>
                <td>
                     <span>
                       <asp:TextBox ID="txtlastname" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                   
                </td>
            </tr>

             <tr>
                <td>
                        <span class="text-small text-bold">Middle name:</span>
                </td>
                <td>
                      <span>
                       <asp:TextBox ID="txtmiddlename" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                </td>
                <td>
                    
                    <span class="text-small text-bold">Address:</span>
                  
                </td>
                <td>
                     <span>
                       <asp:TextBox ID="txtaddress" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                </td>
            </tr>

             <tr height="50px">
                <td>
                    
                    <span class="text-small text-bold">Nationality:</span>
                  
                </td>
                <td>
                     <span>
                       <asp:TextBox ID="txtnationality" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>

                </td>
                <td>
                      <span class="text-small text-bold">Date of birth:</span>
                  
                </td>
                <td>
                      <span style="z-index:99999;"> 
                         <dx:ASPxDateEdit ID="dateEdit" runat="server" CssClass="text-secondary" EditFormatString="dd MMM yyyy" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="170">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>

                    </span>
                </td>
            </tr>
            <tr>
                <td>
                          <span class="text-small text-bold">Gender:</span>
                   
                </td>
                  <td>
                      <span><asp:DropDownList ID="ddlgender" CssClass="text-secondary" BorderColor="Orange" Width="170px" runat="server">
                            <asp:ListItem>Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                        </asp:DropDownList>      </span>
                  </td>

                  <td>
                       <span class="text-small text-bold">State:</span>
                 
                  </td>

                  <td>
                      <span>
                          <asp:DropDownList ID="ddlstate" CssClass="text-secondary" BorderColor="Orange" Width="200px" runat="server">
                            <asp:ListItem Selected="True" Value="null">--select state--</asp:ListItem>
                            <asp:ListItem> Abia State</asp:ListItem>
 <asp:ListItem> Adamawa State</asp:ListItem>
<asp:ListItem>Akwa Ibom State</asp:ListItem>
<asp:ListItem>Anambra State</asp:ListItem>
  <asp:ListItem>Bauchi State</asp:ListItem>
 <asp:ListItem>Bayelsa State</asp:ListItem>
<asp:ListItem>Benue State</asp:ListItem>
<asp:ListItem>Borno State</asp:ListItem>
  <asp:ListItem>Cross River State</asp:ListItem>
 <asp:ListItem>Delta State</asp:ListItem>
<asp:ListItem>Ebonyi State</asp:ListItem>
 <asp:ListItem>Edo State</asp:ListItem>
 <asp:ListItem>Ekiti State</asp:ListItem>
<asp:ListItem>Enugu State</asp:ListItem>
<asp:ListItem>Gombe State</asp:ListItem>
  <asp:ListItem>Imo State</asp:ListItem>
 <asp:ListItem>Jigawa State</asp:ListItem>
<asp:ListItem>Kaduna State</asp:ListItem>
<asp:ListItem>Kano State</asp:ListItem>
  <asp:ListItem>Katsina State</asp:ListItem>
 <asp:ListItem>Kebbi State</asp:ListItem>
<asp:ListItem>Kogi State</asp:ListItem>
 <asp:ListItem>Kwara State</asp:ListItem>
<asp:ListItem>Lagos State</asp:ListItem>
<asp:ListItem>Nasarawa State</asp:ListItem>
  <asp:ListItem>Niger State</asp:ListItem>
 <asp:ListItem>Ogun State</asp:ListItem>
<asp:ListItem>Ondo State</asp:ListItem>
<asp:ListItem>Osun State</asp:ListItem>
  <asp:ListItem>Oyo State</asp:ListItem>
 <asp:ListItem>Plateau State</asp:ListItem>
<asp:ListItem>Rivers State</asp:ListItem>
 <asp:ListItem>Sokoto State</asp:ListItem>
 <asp:ListItem>Taraba State</asp:ListItem>
<asp:ListItem>Yobe State</asp:ListItem>
<asp:ListItem>Zamfara State</asp:ListItem>
  <asp:ListItem>Abuja</asp:ListItem>
                        </asp:DropDownList>
                      </span>
                      
                        <span>

                   <%--    <asp:TextBox ID="txtstate" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>--%>
                   </span>
                  </td>

                
            </tr>

            <tr height="50px">
                <td>
                      <span class="text-small text-bold">Admission date:</span>
                  
                </td>
                <td>
                     <span> 
                         <dx:ASPxDateEdit ID="txtadmissiondate" runat="server" EditFormatString="dd MMM yyyy" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Date="2009-11-02 09:23" Width="170px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>

                    </span>
                </td>
                <td>
                       <span class="text-small text-bold">Place of birth:</span>
                  
                </td>
                <td>
                     <span>
                       <asp:TextBox ID="txtpob" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                </td>
              
            </tr>
            <tr>
                <td>
                     <span class="text-small text-bold">LGA:</span>
                  
                </td>
                 <td>
                     <span>
                       <asp:TextBox ID="txtlga" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox>
                   </span>
                </td>
              
            </tr>
        </table>

       <div class=" align-center">
          <span>  <button runat="server" id="btnsavepersonal" onserverclick="btnsavepersonal_ServerClick" class="button bg-green fg-white small-button"><span class=" mif-file-archive"></span> Save</button></span>

                                 <span>  <button runat="server" id="btncancel" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>
    </div>
    </div>
    </div>

             </ContentTemplate>
        </ajaxToolkit:TabPanel>

        
         <ajaxToolkit:TabPanel runat="server" HeaderText="Parent Details" ID="TabPanel2">
             <ContentTemplate>
<div style="z-index:1;" class="panel" id="panel2">
        
    <div class="content">
        <table width="100%">
            <tr>
                <td><p class="text-small text-bold">Parent First name*:</p></td>
                 <td> <span><asp:TextBox ID="txtparentfirst" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>     
                   </td>
                 <td>
                     <p class="text-small text-bold">Parent Lastname*:</p>
                 </td>

                 <td>
                     <span><asp:TextBox ID="txtparentlast" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span>
                 </td>
            </tr>
            <tr>
                <td><p class="text-small text-bold">Education*:</p></td>
                 <td><span><asp:TextBox ID="Education" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span> </td>
                 <td> <p class="text-small text-bold">Address*:</p></td>
                 <td><span><asp:TextBox ID="txtparentaddress" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span></td>
            </tr>
            <tr>
                <td><p class="text-small text-bold">Occupation*</p></td>
                <td><span><asp:TextBox ID="Occupation" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span> </td>
                <td><p class="text-small text-bold">Mobilephone*:</p></td>
                <td><span><asp:TextBox ID="Mobilephone" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span></td>
            </tr>
            <tr>
                <td><p class="text-small text-bold">Relation*:</p></td>
                 <td><span>
                     <asp:DropDownList ID="ddlrelation" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" Width="199px" runat="server">
                         <asp:ListItem>FATHER</asp:ListItem>
                         <asp:ListItem>MOTHER</asp:ListItem>
                         <asp:ListItem>OTHERS</asp:ListItem>
                     </asp:DropDownList></span>

                 </td>
                 <td><p class="text-small text-bold">Email*:</p></td>
                 <td><span><asp:TextBox ID="txtemail" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span> </td>
            </tr>

            <tr>
                <td><p class="text-small text-bold">Office phone:</p></td>
                 <td> <span><asp:TextBox ID="txtofficephone" CssClass="text-secondary" BorderWidth="1" BorderColor="Orange" runat="server"></asp:TextBox></span></td>
                 <td></td>
                 <td></td>
            </tr>
        </table>
        <div class=" align-center">
          <span>  <button runat="server" id="btnsaveparent" onserverclick="btnsaveparent_ServerClick" class="button bg-green fg-white small-button"><span class=" mif-file-archive"></span> Save</button></span>

                                 <span>  <button runat="server" id="Button2" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>
    </div>

        </div>
         </div>
             </ContentTemplate>
             
        </ajaxToolkit:TabPanel>

         <ajaxToolkit:TabPanel runat="server" HeaderText="Documents" ID="TabPanel4">
             <ContentTemplate>
                 <div class="panel" id="panel4">
         
    <div class="content">
          <asp:GridView ID="gridviewdoc" OnSelectedIndexChanged="gridviewdoc_SelectedIndexChanged" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                        
                        <asp:CommandField ShowSelectButton="True" SelectText="Download" ControlStyle-ForeColor="Blue"/>
                   
            
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-secondary" Height="30px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
        <div class=" align-center">
          <span>  <button runat="server" id="btnsavedocument" onserverclick="btnsavedocument_ServerClick" class="button bg-green fg-white small-button"><span class=" mif-file-archive"></span> Save</button></span>

                                 <span>  <button runat="server" id="Button3" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>
    </div>
        </div>
         </div>
             </ContentTemplate>
        </ajaxToolkit:TabPanel>
        
         <ajaxToolkit:TabPanel runat="server" HeaderText="Courses Registered" ID="TabPanel3">
           
             <ContentTemplate>
                 <div class="panel" id="panel3">
         
   <div class="content bg-white">
    
               <asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                        
                         <asp:HyperLinkField HeaderText="CourseID" DataNavigateUrlFields="SN" DataTextField="SN"
                    DataNavigateUrlFormatString="Add_course_edit.aspx?courseID={0}">

                     
                         <ItemStyle CssClass="fg-red" />
                         </asp:HyperLinkField>

                     
                       <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname" />
                       <asp:BoundField DataField="LastName" HeaderText="Lastname" SortExpression="Lastname" />
                       <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Status" />
              <asp:BoundField DataField="Term" HeaderText="Term" SortExpression="Term" />
                   <asp:BoundField DataField="cousesdone" HeaderText="Courses Registered" SortExpression="coursesdone" />
                   
            
                          </Columns>
                 <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
                  <asp:Label ID="lblcoursenotregistered" Visible="False" CssClass="fg-darkRed text-bold align-center" runat="server" Text="Courses have not been registered for this student"></asp:Label>
          <span> <button runat="server" id="btnaddcourse" onserverclick="btnaddcourse_ServerClick" class="mini-button button fg-white bg-lightGreen"><span class="mif-arrow-right text-bold"></span> Add new Course(s)</button></span>
       <div class=" align-center">
          <span>  <button runat="server" id="btnsavecourse" onserverclick="btnsavecourse_ServerClick" class="button bg-green fg-white small-button"><span class=" mif-file-archive"></span> Save</button></span>

                                 <span>  <button runat="server" id="Button4" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>
    </div>  
       
         </div>
         </div>
             </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    



     
   
    


    

    
</asp:Content>

