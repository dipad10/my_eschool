﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Admin_StudentReg_photos_Classes
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If _Connection.State = ConnectionState.Open Then
            _Connection.Close()

        End If
        If Not Page.IsPostBack Then

            loadGrid()



        End If
    End Sub
    Private Sub loadGrid()

        'Dim rec As Enrollment.School_setting = (New cls_school_settings).Selectschool(0)


        _Connection.Open()
        Dim _cmd As SqlCommand = _Connection.CreateCommand()
        _cmd.CommandType = CommandType.Text
        _cmd.CommandText = "select * from classes ORDER BY SN ASC"
        _cmd.ExecuteNonQuery()


        Dim dt As New DataTable()
        Dim sda As New SqlDataAdapter(_cmd)

        sda.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            Msgbox1.ShowHelp("No classes found")
        End If
    End Sub


   

    Protected Sub btnnew_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("classes_edit.aspx")
    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To GridView1.Rows.Count - 1
            Dim cb As CheckBox = DirectCast(GridView1.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
            If cb.Checked = True Then
                count += 1
                If cb IsNot Nothing AndAlso cb.Checked Then
                    Dim sn As String = DirectCast(GridView1.Rows(i).Cells(1).Controls(0), HyperLink).Text
                    _Connection.Open()
                    Dim _cmd As SqlCommand = _Connection.CreateCommand()
                    _cmd.CommandType = CommandType.Text
                    _cmd.CommandText = "delete from classes where SN=('" & sn & "')"
                    _cmd.ExecuteNonQuery()
                    _Connection.Close()

                    'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)

                    Msgbox1.Showsuccess("" & count & " class(s) deleted successfully!")

                End If

            Else

            End If

        Next
    End Sub
End Class
