﻿

Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Admin_StudentReg_SubjectAllocation_edit
    Inherits System.Web.UI.Page
    Dim Conn As New SqlConnection(m_strconnString)
    Protected Sub btnadd_Click(sender As Object, e As EventArgs)
        If ListBox1.SelectedIndex = 0 Or ListBox1.SelectedIndex = -1 Then
            Response.Write("<script>alert('please select an item');</script>")
            Exit Sub
        Else

            For Each item As ListItem In ListBox1.Items
                If item.Selected And Not ListBox2.Items.Contains(item) Then
                    Dim newItem As New ListItem(item.Text, item.Value)
                    ListBox2.Items.Add(newItem)

                End If

            Next
        End If
    End Sub

    Protected Sub btnsavecourse_ServerClick(sender As Object, e As EventArgs)
        Dim classname As String = Request.QueryString("class")
        Dim count As Integer = 0
        Dim connect5 As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

        connect5.Open()

        For i As Integer = 0 To ListBox2.Items.Count - 1
            If ListBox2.Items(i).Selected = False Then
                count += 1
                Dim _cmd5 As SqlCommand = connect5.CreateCommand()
                _cmd5.CommandType = CommandType.Text
                _cmd5.CommandText = "INSERT INTO SubjectAllocation ([class], [subjects]) VALUES (@class, @subjects)"
                _cmd5.Parameters.AddWithValue("@class", classname)
                _cmd5.Parameters.AddWithValue("@subjects", ListBox2.Items(i).ToString())
              
                _cmd5.ExecuteNonQuery()

            End If

        Next
        Dim a As New Cls_classes
        Dim rec As Enrollment.Class = a.SelectThisClassname(classname)
        rec.CoursesRegisterd = count
      
        Dim res As ResponseInfo = a.Update(rec)
        If res.ErrorCode = 0 Then
            Msgbox1.Showsuccess("" & count & " Subjects Allocated to " & classname & "")
            connect5.Close()

        Else
            Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim classname As String = Request.QueryString("class")
            If classname <> "" Then
                lblallocate.InnerText = "Allocate Subjects to Class " & classname & ""
            End If
            loadGrid()
            loadsubjects()

        End If
    End Sub

    Private Sub loadGrid()

        'Dim rec As Enrollment.School_setting = (New cls_school_settings).Selectschool(0)

        Dim classname As String = Request.QueryString("class")
        Conn.Open()
        Dim _cmd As SqlCommand = Conn.CreateCommand()
        _cmd.CommandType = CommandType.Text
        _cmd.CommandText = "select * from SubjectAllocation where class='" & classname & "'"
        _cmd.ExecuteNonQuery()


        Dim dt As New DataTable()
        Dim sda As New SqlDataAdapter(_cmd)

        sda.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            Msgbox1.ShowHelp("No classes found")
        End If
    End Sub

    Private Sub loadsubjects()



        Using mycommand As SqlCommand = Conn.CreateCommand
            mycommand.CommandType = CommandType.Text
            mycommand.CommandText = "select * from subjects"

            Dim _dt1 As DataTable = New DataTable()
            Dim _da1 As SqlDataAdapter = New SqlDataAdapter(mycommand)
            _da1.Fill(_dt1)
            If _dt1.Rows.Count > 0 Then
                ListBox1.DataSource = _dt1
                ListBox1.DataTextField = "subjectname"
                ListBox1.DataValueField = "subjectname"
                ListBox1.DataBind()
                ListBox1.Items.Insert(0, New ListItem(String.Empty, String.Empty))

            End If
        End Using

    End Sub

    Protected Sub btnremove_ServerClick(sender As Object, e As EventArgs)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            Dim cb As CheckBox = DirectCast(GridView1.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
            If cb.Checked = True Then
                If cb IsNot Nothing AndAlso cb.Checked Then
                    Dim serial As String = DirectCast(GridView1.Rows(i).Cells(1).Controls(0), HyperLink).Text
                    Dim connect4 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                    connect4.Open()
                    Dim _cmd As SqlCommand = connect4.CreateCommand()
                    _cmd.CommandType = CommandType.Text
                    _cmd.CommandText = "delete from subjectAllocation where SN=('" & serial & "')"
                    _cmd.ExecuteNonQuery()
                    connect4.Close()
                    Response.Write("<script>alert('subjects Deleted successfully');</script>")

                End If

            End If
        Next
    End Sub
End Class
