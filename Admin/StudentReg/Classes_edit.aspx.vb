﻿
Partial Class Admin_StudentReg_Classes_edit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sn As String = Request.QueryString("sn")
            If sn <> "" Then

                Dim rec As Enrollment.Class = (New Cls_classes).SelectThisClass(sn)
                txtclassid.Text = rec.SN
                txtclassname.Text = rec.Class
                staffs.SelectedText = rec.Teacher
                ddlcategory.SelectedValue = rec.Category
                lblclass.InnerText = "class " & sn & " Edit "
            Else
                lblclass.InnerText = "New class"
            End If
        End If
    End Sub
    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        Dim A As New Cls_classes
        Dim classid As String = Request.QueryString("sn")
        If classid <> "" Then
            'update record
            Dim rec As Enrollment.Class = A.SelectThisClass(classid)
            rec.Class = txtclassname.Text
            rec.Teacher = staffs.SelectedText
            rec.Category = ddlcategory.SelectedValue
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='classes.aspx';", True)


            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If

        Else
            'create fresh record
            Dim rec As Enrollment.Class = A.SelectThisClassname(txtclassname.Text)
            If rec.Class Is Nothing Then  'if d classname does not exist before in the system
                rec.Class = txtclassname.Text
                rec.Teacher = staffs.SelectedText
                rec.Category = ddlcategory.SelectedValue
                Dim res As ResponseInfo = A.Insert(rec)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='classes.aspx';", True)


                Else
                    Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                    'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
                End If
            Else

                'if it exists
                Me.Msgbox1.ShowHelp("This class already exists in the system! Pls use a different class name")



            End If




        End If
    End Sub

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub


End Class
