﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging
Partial Class Admin_StudentReg_Students_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim studentID As String = Request.QueryString("IDNo")
            If studentID <> "" Then
                'get personal details into textboxes
                Dim rec As Enrollment.StudentRegistration = (New Cls_StudentRegistration).SelectThisIDNo(studentID)
                profilename.Visible = True
                addnew.Visible = False
                lblregno.Text = rec.IDNo
                lblstudentname.Text = rec.LastName & " " & rec.FirstName
                txtIDno.Text = rec.IDNo
                txtmiddlename.Text = rec.MiddleName
                txtaddress.Text = rec.Address
                txtnationality.Text = rec.Nationality
                txtfirstname.Text = rec.FirstName
                dateEdit.Date = Format(rec.DOB, "dd MMM yyyy")
                ddlgender.SelectedValue = rec.Gender
                ddlstate.SelectedValue = rec.State.ToString()

                txtlastname.Text = rec.LastName.ToString()
                txtadmissiondate.Date = Format(rec.AdmissionDate, "dd MMM yyyy")
                txtpob.Text = rec.POB
                txtlga.Text = rec.LGA
                If rec.Enrollment_type = "BOARDING" Then
                    chkboarding.Checked = True
                Else
                    chkboarding.Checked = False

                End If

                txtparentfirst.Text = rec.Parentfirstname
                txtparentlast.Text = rec.Parentlastname
                ddlrelation.SelectedValue = rec.Relation
                Education.Text = rec.ParentEducation
                Occupation.Text = rec.ParentOccupation
                txtemail.Text = rec.Parentemail
                Mobilephone.Text = rec.Mobilephone
                txtofficephone.Text = rec.OfficePhone


                'get picture

                If rec.Picture Is Nothing Then
                    Image1.ImageUrl = "~/images/nophoto.jpg"
                   
                Else
                    Dim image As [Byte]() = rec.Picture.ToArray
                    Dim src As String = "data:image/jpeg;base64," + Convert.ToBase64String(image)
                    Image1.ImageUrl = src
                End If

                'get courses

                Dim Recs As List(Of Enrollment.CourseReg) = (New Cls_coursereg).SelectThisReg(studentID)

                pnllistcourses.Visible = True

                GridView1.DataSource = Recs
                GridView1.DataBind()
                If GridView1.Rows.Count = 0 Then
                    If MainView.ActiveViewIndex = 3 Then
                        Msgbox1.ShowHelp("Courses have not been registered for this student. Click to add new course")
                    End If
                End If
                Tab1.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                Tab2.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                Tab2.Enabled = True
                Tab3.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                Tab3.Enabled = True
                Tab4.Enabled = True
                Tab4.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                MainView.ActiveViewIndex = 0
            Else
                Tab1.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                Tab2.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                Tab2.Enabled = False
                Tab3.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                Tab3.Enabled = False
                Tab4.Enabled = False
                Tab4.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                MainView.ActiveViewIndex = 0
            End If
          

          

        End If



    End Sub


    Protected Sub btnsavepersonal_Click(sender As Object, e As EventArgs)
        '--------SAVING PERSONAL DETAILS AND GETING STUDENT NUMBER---------'
       

        Dim studentID As String = Request.QueryString("IDNo")
        Dim s As New Cls_StudentRegistration
        If studentID <> "" Then
            Dim rec As Enrollment.StudentRegistration = s.SelectThisIDNo(studentID)
            rec.FirstName = txtfirstname.Text
            rec.LastName = txtlastname.Text
            rec.DOB = dateEdit.Date
            rec.AdmissionDate = txtadmissiondate.Date
            rec.Address = txtaddress.Text
            rec.POB = txtpob.Text
            rec.Gender = ddlgender.SelectedValue
            rec.Nationality = txtnationality.Text
            rec.State = ddlstate.Text
            rec.LGA = txtlga.Text
            rec.BloodGroup = txtbloodgroup.Text
            Dim enrollmenttype As String
            If chkboarding.Checked = True Then
                enrollmenttype = "BOARDING"
            Else
                enrollmenttype = "DAY"
            End If
            rec.Enrollment_type = enrollmenttype
            rec.Field1 = ddlreligion.SelectedValue
            Dim res As ResponseInfo = s.Update(rec)
            If res.ErrorCode = 0 Then

                Msgbox1.Showsuccess("Data saved successfully")
            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If


        Else

            'insert fresh record

            Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

            _Connection.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'Registration' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, _Connection)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)

            _Connection.Close()

            Dim ID As String = "BES" & "/" & Date.Now.Year.ToString() & "/" & Format(NValue, "0000")
            Me.txtIDno.Text = ID
            Session("idnumber") = txtIDno.Text
            Session("firstname") = txtfirstname.Text
            Session("lastname") = txtlastname.Text
            Session("middlename") = txtmiddlename.Text

            _Connection.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='Registration' "
            Dim comm1 As New SqlCommand(sqlNoUpD, _Connection)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()


            Dim enrollmenttype As String
            If chkboarding.Checked = True Then
                enrollmenttype = "BOARDING"
            Else
                enrollmenttype = "DAY"
            End If

            Dim cmd5 As SqlCommand = _Connection.CreateCommand()
            cmd5.CommandType = CommandType.Text
            cmd5.CommandText = "Insert into StudentRegistration ([IDNo], [FirstName], [MiddleName], [LastName], [DOB], [AdmissionDate], [Address], [Gender], [POB], [Nationality], [State], [LGA], [BloodGroup], [Notetype], [Deleted], [Active], [Status], [SubmittedBy], [SubmittedOn], [session], [Enrollment_type], [Field1]) values (@idno, @fname, @mname, @lname, @dob, @admdate, @address, @sex, @pob, @nation, @state, @lga, @blood, @note, @delete, @active, @status, @subby, @subon, @session, @type, @religion)"
            cmd5.Parameters.AddWithValue("@idno", txtIDno.Text)
            cmd5.Parameters.AddWithValue("@fname", txtfirstname.Text)
            cmd5.Parameters.AddWithValue("@mname", txtmiddlename.Text)
            cmd5.Parameters.AddWithValue("@lname", txtlastname.Text)
            cmd5.Parameters.AddWithValue("@dob", dateEdit.Date)
            cmd5.Parameters.AddWithValue("@admdate", txtadmissiondate.Date)
            cmd5.Parameters.AddWithValue("@address", txtaddress.Text)
            cmd5.Parameters.AddWithValue("@sex", ddlgender.SelectedValue)
            cmd5.Parameters.AddWithValue("@pob", txtpob.Text)
            cmd5.Parameters.AddWithValue("@nation", txtnationality.Text)
            cmd5.Parameters.AddWithValue("@state", ddlstate.SelectedValue)
            cmd5.Parameters.AddWithValue("@lga", txtlga.Text)
            cmd5.Parameters.AddWithValue("@blood", txtbloodgroup.Text)
            cmd5.Parameters.AddWithValue("@note", "Enrollment")
            cmd5.Parameters.AddWithValue("@delete", "0")
            cmd5.Parameters.AddWithValue("@active", "1")
            cmd5.Parameters.AddWithValue("@status", "PENDING")
            cmd5.Parameters.AddWithValue("@subby", Session("uname").ToString())
            cmd5.Parameters.AddWithValue("@subon", Date.Now.ToString("dd MMMM yyyy"))
            cmd5.Parameters.AddWithValue("@session", ddlsession.SelectedValue)
            cmd5.Parameters.AddWithValue("@type", enrollmenttype)
            cmd5.Parameters.AddWithValue("@religion", ddlreligion.SelectedValue)
            cmd5.ExecuteNonQuery()

            _Connection.Close()

            MainView.ActiveViewIndex = 1
            Tab1.Enabled = False
            Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
            Tab2.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
            Tab2.Enabled = True
            Tab3.Enabled = False
            Tab3.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
            Tab4.Enabled = False
            Tab4.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
            Msgbox1.Showsuccess("Data saved successfully")
            lblstudentno.Text = Session("idnumber")
        End If
     


    End Sub


    Protected Sub btnsaveparent_Click(sender As Object, e As EventArgs)
        'update details by using querystring
        Dim studentID As String = Request.QueryString("IDNo")
        Dim s As New Cls_StudentRegistration
        If studentID <> "" Then
            Dim rec As Enrollment.StudentRegistration = s.SelectThisIDNo(studentID)
            rec.Parentfirstname = txtparentfirst.Text
            rec.Parentlastname = txtparentlast.Text
            rec.Relation = ddlrelation.SelectedValue
            rec.ParentEducation = Education.Text
            rec.ParentOccupation = Occupation.Text
            rec.Parentemail = txtemail.Text
            rec.Parentaddress = txtparentaddress.Text
            rec.Mobilephone = Mobilephone.Text
            rec.OfficePhone = txtofficephone.Text

            Dim res As ResponseInfo = s.Update(rec)
            If res.ErrorCode = 0 Then

                Msgbox1.Showsuccess("Data saved successfully")
            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If


        Else

            'update by using session
            Dim studentno As String = Session("idnumber")
            Dim sn As New Cls_StudentRegistration
            If studentno <> "" Then
                Dim rec As Enrollment.StudentRegistration = sn.SelectThisIDNo(studentno)
                rec.Parentfirstname = txtparentfirst.Text
                rec.Parentlastname = txtparentlast.Text
                rec.Relation = ddlrelation.SelectedValue
                rec.ParentEducation = Education.Text
                rec.ParentOccupation = Occupation.Text
                rec.Parentemail = txtemail.Text
                rec.Parentaddress = txtparentaddress.Text
                rec.Mobilephone = Mobilephone.Text
                rec.OfficePhone = txtofficephone.Text

                Dim res As ResponseInfo = sn.Update(rec)
                If res.ErrorCode = 0 Then
                    MainView.ActiveViewIndex = 2
                    Tab1.Enabled = False
                    Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                    Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                    Tab2.Enabled = False
                    Tab3.Enabled = True
                    Tab3.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                    Tab4.Enabled = False
                    Tab4.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
                    Msgbox1.Showsuccess("Data saved successfully")
                Else
                    Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                    'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
                End If


            End If

        End If


    End Sub

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        'update documents by querystring
        If File1.PostedFile.FileName = "" Then
            Msgbox1.ShowError("pls select a photo or skip")
            Exit Sub

        End If
        Dim studentID As String = Request.QueryString("IDNo")
       
        If studentID <> "" Then

            Select Case DropDownList1.SelectedValue
                Case "Picture"
                    Try
                        Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                        Dim height As Integer = img2.Height
                        ' get the height of image in pixel.
                        Dim width As Integer = img2.Width

                        If height <> width Then
                            Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                            Exit Sub

                        Else
                            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                            connect.Open()

                            Dim com As SqlCommand = connect.CreateCommand
                            com.CommandType = CommandType.Text
                            com.CommandText = "UPDATE StudentRegistration SET Picture=@picture WHERE IDNo=@no"
                            com.Parameters.AddWithValue("@picture", GetPhotoAsByteArray)
                            com.Parameters.AddWithValue("@no", studentID)

                            com.ExecuteNonQuery()
                            connect.Close()

                            MainView.ActiveViewIndex = 3
                            Tab1.Enabled = False
                            Tab2.Enabled = False
                            Tab3.Enabled = False
                            Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                            Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                            Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                            Tab4.Enabled = True
                            Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                            Msgbox1.Showsuccess("Data saved successfully!")
                        End If
                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try


                Case "BirthCertificate"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@BirthCertificate", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", studentID)

                                    com.ExecuteNonQuery()
                                    connect.Close()

                                    MainView.ActiveViewIndex = 3
                                    Tab1.Enabled = False
                                    Tab2.Enabled = False
                                    Tab3.Enabled = False
                                    Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab4.Enabled = True
                                    Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                    Msgbox1.Showsuccess("Data saved successfully!")

                                End If
                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))


                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@BirthCertificate", bytes)
                                com.Parameters.AddWithValue("@no", studentID)

                                com.ExecuteNonQuery()
                                connect.Close()
                                MainView.ActiveViewIndex = 3
                                Tab1.Enabled = False
                                Tab2.Enabled = False
                                Tab3.Enabled = False
                                Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab4.Enabled = True
                                Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                Msgbox1.Showsuccess("Data saved successfully!")

                        End Select



                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

                Case "ClearanceForm"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@ClearanceForm", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", studentID)

                                    com.ExecuteNonQuery()
                                    connect.Close()

                                    MainView.ActiveViewIndex = 3
                                    Tab1.Enabled = False
                                    Tab2.Enabled = False
                                    Tab3.Enabled = False
                                    Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab4.Enabled = True
                                    Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                    Msgbox1.Showsuccess("Data saved successfully!")
                                End If

                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))
                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@ClearanceForm", bytes)
                                com.Parameters.AddWithValue("@no", studentID)

                                com.ExecuteNonQuery()
                                connect.Close()

                                MainView.ActiveViewIndex = 3
                                Tab1.Enabled = False
                                Tab2.Enabled = False
                                Tab3.Enabled = False
                                Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab4.Enabled = True
                                Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                Msgbox1.Showsuccess("Data saved successfully!")
                        End Select




                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

            End Select

        Else
           

            'update by session number
            Select Case DropDownList1.SelectedValue
                Case "Picture"
                    Try
                        Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                        Dim height As Integer = img2.Height
                        ' get the height of image in pixel.
                        Dim width As Integer = img2.Width

                        If height <> width Then
                            Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                            Exit Sub

                        Else
                            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                            connect.Open()

                            Dim com As SqlCommand = connect.CreateCommand
                            com.CommandType = CommandType.Text
                            com.CommandText = "UPDATE StudentRegistration SET Picture=@picture WHERE IDNo=@no"
                            com.Parameters.AddWithValue("@picture", GetPhotoAsByteArray)
                            com.Parameters.AddWithValue("@no", Session("idnumber"))

                            com.ExecuteNonQuery()
                            connect.Close()

                            MainView.ActiveViewIndex = 3
                            Tab1.Enabled = False
                            Tab2.Enabled = False
                            Tab3.Enabled = False
                            Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                            Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                            Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                            Tab4.Enabled = True
                            Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                            Msgbox1.Showsuccess("Data saved successfully!")
                        End If
                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try


                Case "BirthCertificate"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@BirthCertificate", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", Session("idnumber"))

                                    com.ExecuteNonQuery()
                                    connect.Close()

                                    MainView.ActiveViewIndex = 3
                                    Tab1.Enabled = False
                                    Tab2.Enabled = False
                                    Tab3.Enabled = False
                                    Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab4.Enabled = True
                                    Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                    Msgbox1.Showsuccess("Data saved successfully!")

                                End If
                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))


                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@BirthCertificate", bytes)
                                com.Parameters.AddWithValue("@no", Session("idnumber"))

                                com.ExecuteNonQuery()
                                connect.Close()
                                MainView.ActiveViewIndex = 3
                                Tab1.Enabled = False
                                Tab2.Enabled = False
                                Tab3.Enabled = False
                                Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab4.Enabled = True
                                Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                Msgbox1.Showsuccess("Data saved successfully!")

                        End Select



                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

                Case "ClearanceForm"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@ClearanceForm", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", Session("idnumber"))

                                    com.ExecuteNonQuery()
                                    connect.Close()

                                    MainView.ActiveViewIndex = 3
                                    Tab1.Enabled = False
                                    Tab2.Enabled = False
                                    Tab3.Enabled = False
                                    Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                    Tab4.Enabled = True
                                    Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                    Msgbox1.Showsuccess("Data saved successfully!")
                                End If

                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))
                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@ClearanceForm", bytes)
                                com.Parameters.AddWithValue("@no", Session("idnumber"))

                                com.ExecuteNonQuery()
                                connect.Close()

                                MainView.ActiveViewIndex = 3
                                Tab1.Enabled = False
                                Tab2.Enabled = False
                                Tab3.Enabled = False
                                Tab1.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab2.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
                                Tab4.Enabled = True
                                Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
                                Msgbox1.Showsuccess("Data saved successfully!")
                        End Select




                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

            End Select

        End If

        'Check if the resolution of the image is 500x500 in other to avoid the picture coming out in landscape mode'

        ' get the width of image in pixel.
        'Dim fileSize As Integer = (FileUpload1.PostedFile.ContentLength) / 1024
        'get the size of image file.



        'End If

        'save documents and goto courses reg view'


        'If height >= 500 And width >= 500 Then
        '    Response.Write("<script>alert('File size not be exceed than 500 KB, or 500x500 px');</script>")
        '    Exit Sub

        'Else




    End Sub
    Protected Sub btnsaveonly_Click(sender As Object, e As EventArgs)
        If File1.PostedFile.FileName = "" Then
            Msgbox1.ShowError("pls select a photo or skip")
            Exit Sub

        End If
        Dim studentID As String = Request.QueryString("IDNo")
        If studentID <> "" Then
            'update by querystring
            Select Case DropDownList1.SelectedValue
                Case "Picture"
                    Try
                        Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                        Dim height As Integer = img2.Height
                        ' get the height of image in pixel.
                        Dim width As Integer = img2.Width

                        If height <> width Then
                            Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                            Exit Sub

                        Else
                            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                            connect.Open()

                            Dim com As SqlCommand = connect.CreateCommand
                            com.CommandType = CommandType.Text
                            com.CommandText = "UPDATE StudentRegistration SET Picture=@picture WHERE IDNo=@no"
                            com.Parameters.AddWithValue("@picture", GetPhotoAsByteArray)
                            com.Parameters.AddWithValue("@no", studentID)

                            com.ExecuteNonQuery()
                            connect.Close()

                            Msgbox1.Showsuccess("Data saved successfully!")
                        End If

                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

                Case "BirthCertificate"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@BirthCertificate", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", studentID)

                                    com.ExecuteNonQuery()
                                    connect.Close()
                                    Msgbox1.Showsuccess("Data saved successfully!")

                                End If
                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))


                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@BirthCertificate", bytes)
                                com.Parameters.AddWithValue("@no", studentID)

                                com.ExecuteNonQuery()
                                connect.Close()
                                Msgbox1.Showsuccess("Data saved successfully!")
                        End Select

                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

                Case "ClearanceForm"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@ClearanceForm", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", studentID)

                                    com.ExecuteNonQuery()
                                    connect.Close()

                                    Msgbox1.Showsuccess("Data saved successfully!")
                                End If

                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))
                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@ClearanceForm", bytes)
                                com.Parameters.AddWithValue("@no", studentID)

                                com.ExecuteNonQuery()
                                connect.Close()

                                Msgbox1.Showsuccess("Data saved successfully!")
                        End Select


                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

            End Select


        Else

            'update by session number



            Select Case DropDownList1.SelectedValue
                Case "Picture"
                    Try
                        Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                        Dim height As Integer = img2.Height
                        ' get the height of image in pixel.
                        Dim width As Integer = img2.Width

                        If height <> width Then
                            Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                            Exit Sub

                        Else
                            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                            connect.Open()

                            Dim com As SqlCommand = connect.CreateCommand
                            com.CommandType = CommandType.Text
                            com.CommandText = "UPDATE StudentRegistration SET Picture=@picture WHERE IDNo=@no"
                            com.Parameters.AddWithValue("@picture", GetPhotoAsByteArray)
                            com.Parameters.AddWithValue("@no", Session("idnumber"))

                            com.ExecuteNonQuery()
                            connect.Close()

                            Msgbox1.Showsuccess("Data saved successfully!")
                        End If

                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

                Case "BirthCertificate"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@BirthCertificate", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", Session("idnumber"))

                                    com.ExecuteNonQuery()
                                    connect.Close()
                                    Msgbox1.Showsuccess("Data saved successfully!")

                                End If
                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))


                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET BirthCertificate=@BirthCertificate WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@BirthCertificate", bytes)
                                com.Parameters.AddWithValue("@no", Session("idnumber"))

                                com.ExecuteNonQuery()
                                connect.Close()
                                Msgbox1.Showsuccess("Data saved successfully!")
                        End Select

                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

                Case "ClearanceForm"
                    Try
                        Dim ext As String = System.IO.Path.GetExtension(Me.File1.PostedFile.FileName)
                        Select Case ext
                            Case ".JPG", ".PNG", ".GIF", ".JPEG", ".jpg", ".png", ".gif", ".jpeg"
                                Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
                                Dim height As Integer = img2.Height
                                ' get the height of image in pixel.
                                Dim width As Integer = img2.Width

                                If height <> width Then
                                    Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                                    Exit Sub

                                Else
                                    Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                    connect.Open()

                                    Dim com As SqlCommand = connect.CreateCommand
                                    com.CommandType = CommandType.Text
                                    com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                    com.Parameters.AddWithValue("@ClearanceForm", GetPhotoAsByteArray)
                                    com.Parameters.AddWithValue("@no", Session("idnumber"))

                                    com.ExecuteNonQuery()
                                    connect.Close()

                                    Msgbox1.Showsuccess("Data saved successfully!")
                                End If

                            Case ".pdf"
                                Dim fs As Stream = File1.PostedFile.InputStream
                                Dim br As New BinaryReader(fs)
                                Dim bytes As [Byte]() = br.ReadBytes(CType(fs.Length, Int32))
                                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                                connect.Open()

                                Dim com As SqlCommand = connect.CreateCommand
                                com.CommandType = CommandType.Text
                                com.CommandText = "UPDATE StudentRegistration SET ClearanceForm=@ClearanceForm WHERE IDNo=@no"
                                com.Parameters.AddWithValue("@ClearanceForm", bytes)
                                com.Parameters.AddWithValue("@no", Session("idnumber"))

                                com.ExecuteNonQuery()
                                connect.Close()

                                Msgbox1.Showsuccess("Data saved successfully!")
                        End Select


                    Catch ex As Exception
                        Msgbox1.ShowError(ex.Message)
                    End Try

            End Select

        End If
      
    End Sub
    Protected Sub btnskipdoc_ServerClick(sender As Object, e As EventArgs)
   
        MainView.ActiveViewIndex = 3
        Tab3.Enabled = False
        Tab3.CssClass = "bg-darkGreen mini-button text-secondary fg-white"
        Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
    End Sub

    Protected Sub btnskipcourse_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/StudentReg/Students.aspx")
    End Sub

    



    


    


    
    Protected Function GetFormatString(ByVal value As Object) As String
        Return If(value Is Nothing, String.Empty, value.ToString())
    End Function


    '-------END OF STUDENT COURSES------------'

    'scanner file upload'
    'Public Sub ShowPhoto2(ByVal Photo As System.Drawing.Image, ByVal IDNo As String)
    '    Try
    '        Dim TempFile As String = "photos/" & Replace(IDNo, "/", "") & ".bmp"
    '        Photo.Save(Server.MapPath(TempFile))
    '        Me.Img1.Src = Left(Request.Url.AbsolutePath, Request.Url.AbsolutePath.LastIndexOf("/") + 1) & TempFile
    '    Catch
    '        Dim TempFile As String = "photos/error.bmp"
    '        Me.Img1.Src = Left(Request.Url.AbsolutePath, Request.Url.AbsolutePath.LastIndexOf("/") + 1) & TempFile
    '    End Try

    '    Me.File1.Visible = True
    'End Sub

    'Public Property Width() As Long
    '    Get
    '        Return Me.Img1.Width
    '    End Get
    '    Set(ByVal value As Long)
    '        Me.Img1.Width = value
    '    End Set
    'End Property

    'Public Property Height() As Long
    '    Get
    '        Return Me.Img1.Height
    '    End Get
    '    Set(ByVal value As Long)
    '        Me.Img1.Height = value
    '    End Set
    'End Property

    Public Function GetPhotoAsByteArray() As Byte()
        Try
            If Request.Files.Count > 0 Then
                'on upload

                Dim SavePath As String = Server.MapPath("photos/" & Guid.NewGuid.ToString)
                Request.Files(0).SaveAs(SavePath)

                Dim ms As New IO.MemoryStream
                Dim img As Drawing.Image = Drawing.Image.FromFile(SavePath)
                img.Save(ms, Imaging.ImageFormat.Jpeg)
                img.Dispose()
                IO.File.Delete(SavePath)
                Return ms.ToArray
            Else
                Dim B(-1) As Byte
                Return B
            End If
        Catch
            Dim B(-1) As Byte
            Return B
        End Try
    End Function

    'Public Sub ShowPhoto(ByVal Photo As System.Drawing.Image, ByVal IDNo As String)
    '    Try
    '        Dim TempFile As String = "photos/" & Replace(IDNo, "/", "") & ".bmp"
    '        Photo.Save(Server.MapPath(TempFile))
    '        Me.Img1.Src = Left(Request.Url.AbsolutePath, Request.Url.AbsolutePath.LastIndexOf("/") + 1) & TempFile


    '    Catch
    '        Dim TempFile As String = "photos/error.bmp"
    '        Me.Img1.Src = Left(Request.Url.AbsolutePath, Request.Url.AbsolutePath.LastIndexOf("/") + 1) & TempFile
    '        Me.Img1.Src = ""
    '    End Try
    '    Me.File1.Visible = False
    'End Sub

    'Public Function GetPhotoAsByteArrayUpdate() As Byte()
    '    Try
    '        If Request.Files.Count > 0 Then 'on upload
    '            Dim SavePath As String = Server.MapPath("photos/" & Guid.NewGuid.ToString)
    '            Request.Files(0).SaveAs(SavePath)

    '            Dim ms As New IO.MemoryStream
    '            Dim img As Drawing.Image = Drawing.Image.FromFile(SavePath)
    '            img.Save(ms, Imaging.ImageFormat.Jpeg)
    '            img.Dispose()
    '            IO.File.Delete(SavePath)
    '            Return ms.ToArray
    '        Else
    '            Dim B(-1) As Byte
    '            Return B
    '            'length is 0
    '        End If
    '    Catch
    '        Dim B(-1) As Byte
    '        Return B
    '    End Try
    'End Function

    

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Select Case DropDownList1.SelectedValue
            Case "BirthCertificate"
                Image1.Visible = False
            Case "ClearanceForm"
                Image1.Visible = False
            Case "picture"
                Image1.Visible = True

        End Select
    End Sub

    Protected Sub cmdnewcourse_Click(sender As Object, e As EventArgs)
        Dim studentID As String = Request.QueryString("IDNo")
        If studentID <> "" Then
            Response.Redirect("/Admin/StudentReg/Course_reg.aspx?IDNo=" & studentID & "")
        Else
            Response.Redirect("/Admin/StudentReg/Course_reg.aspx?IDNo=" & Session("idnumber") & "")
        End If

    End Sub

    Protected Sub Tab2_Click(sender As Object, e As EventArgs)
        
            MainView.ActiveViewIndex = 1
            Tab2.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
            Tab1.CssClass = "bg-lightGreen mini-button text-secondary fg-white"

      
    End Sub

    Protected Sub Tab3_Click(sender As Object, e As EventArgs)

        MainView.ActiveViewIndex = 2
        Tab3.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
        Tab1.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
        Tab2.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
    End Sub

    Protected Sub Tab4_Click(sender As Object, e As EventArgs)
        MainView.ActiveViewIndex = 3
        Tab4.CssClass = "bg-darkCobalt mini-button text-secondary fg-white"
        Tab1.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
        Tab2.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
        Tab3.CssClass = "bg-lightGreen mini-button text-secondary fg-white"
    End Sub
End Class
