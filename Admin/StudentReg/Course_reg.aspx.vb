﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web
Partial Class Admin_StudentReg_Course_reg
    Inherits System.Web.UI.Page
    Dim getdetailsconnect As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Dim studentID As String = Request.QueryString("IDNo")
            If studentID <> "" Then
                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                connect.Open()

                Dim com As SqlCommand = connect.CreateCommand
                com.CommandType = CommandType.Text
                com.CommandText = "select * from StudentRegistration where IDNo=('" & studentID & "')"
                com.ExecuteNonQuery()
                Dim dr As SqlDataReader
                dr = com.ExecuteReader()
                If dr.Read Then
                    combo1.Text = studentID
                    txtfirstname.Text = dr("FirstName").ToString()
                    txtmiddlename.Text = dr("MiddleName").ToString()
                    txtlastname.Text = dr("LastName").ToString()

                    btnaddcourse.Visible = True
                End If

            End If






            Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)


                myconnection.Open()
                Using mycommand As SqlCommand = myconnection.CreateCommand
                    mycommand.CommandType = CommandType.Text
                    mycommand.CommandText = "select DepName from CodDepartment"

                    Dim _dt As DataTable = New DataTable()
                    Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                    _da.Fill(_dt)
                    If _dt.Rows.Count > 0 Then
                        ddlclass.Items.Insert(0, New ListItem(String.Empty, String.Empty))
                        ddlclass.DataSource = _dt
                        ddlclass.DataTextField = "DepName"
                        ddlclass.DataValueField = "DepName"
                        ddlclass.DataBind()
                        ddlclass.Items.Insert(0, New ListItem(String.Empty, String.Empty))
                    End If
                    myconnection.Close()

                End Using
            End Using

            panelcourses.Visible = False

            Panelgridview.Visible = False
            'btnaddcourse.Visible = False
            btnremove.Visible = False

        End If


        If Not Page.IsPostBack Then
            Me.bindcombo()


        End If
    End Sub

    

    Private Sub bindcombo()
        Dim constr As String = ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT IDNo, FirstName, LastName, Gender FROM StudentRegistration"
                cmd.Connection = con

                Dim dt As New DataTable()
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dt)
                    combo1.DataSource = dt
                    combo1.Columns.Clear()
                    combo1.Columns.Add("IDNo").Width = 120
                    combo1.Columns.Add("FirstName").Width = 170
                    combo1.Columns.Add("LastName").Width = 200

                    combo1.ValueField = "IDNo"
                    combo1.TextField = "FirstName"
                    combo1.TextField = "LastName"

                    combo1.TextFormatString = "{0}"
                    combo1.DataBind()

                End Using
            End Using
        End Using


    End Sub









    Protected Sub combo1_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)



        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource1.SelectCommand = "SELECT [IDNo], [FirstName], [LastName] FROM (select [IDNo], [FirstName], [LastName], row_number()over(order by t.[LastName]) as [rn] from [StudentRegistration] as t where (([FirstName] + ' ' + [LastName] + ' ' + [IDNo]) LIKE @filter)) as st where st.[rn] between @startIndex and @endIndex"

        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("filter", TypeCode.String, String.Format("%{0}%", e.Filter))
        SqlDataSource1.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString())
        SqlDataSource1.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString())
        comboBox.DataSource = SqlDataSource1
        comboBox.DataBind()




    End Sub

    Protected Sub combo1_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        Dim value As Long = 0
        If e.Value Is Nothing OrElse (Not Int64.TryParse(e.Value.ToString(), value)) Then
            Return
        End If
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource1.SelectCommand = "SELECT IDNo, LastName, FirstName FROM StudentRegistration WHERE (IDNo = @ID) ORDER BY FirstName"

        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("ID", TypeCode.Int64, e.Value.ToString())
        comboBox.DataSource = SqlDataSource1
        comboBox.DataBind()

    End Sub

    Protected Sub combo1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim studentcon As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
        studentcon.Open()

        Dim com8 As SqlCommand = studentcon.CreateCommand
        com8.CommandType = CommandType.Text
        com8.CommandText = "select * from StudentRegistration where IDNo=( '" + combo1.Text + "')"
        com8.ExecuteNonQuery()
        Dim dr8 As SqlDataReader

        dr8 = com8.ExecuteReader()
        If dr8.Read Then
            txtfirstname.Text = dr8("FirstName").ToString()
            txtmiddlename.Text = dr8("MiddleName").ToString()
            txtlastname.Text = dr8("LastName").ToString()
        End If

        studentcon.Close()





        getdetailsconnect.Open()
        Dim _cmd2 As SqlCommand = getdetailsconnect.CreateCommand()
        _cmd2.CommandType = CommandType.Text
        _cmd2.CommandText = "select * from CourseReg where RegID=( '" + combo1.Text + "')"
        _cmd2.ExecuteNonQuery()
        Dim dr2 As SqlDataReader

        dr2 = _cmd2.ExecuteReader()
        If dr2.Read Then

            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
            connect.Open()

            Dim com As SqlCommand = connect.CreateCommand
            com.CommandType = CommandType.Text
            com.CommandText = "select * from CourseReg where RegID=( '" + combo1.Text + "')"
            Dim dt3 As New DataTable()
            Dim sda3 As New SqlDataAdapter(com)

            sda3.Fill(dt3)
            GridView1.DataSource = dt3
            GridView1.DataBind()
            Panelgridview.Visible = True
            btnaddcourse.Visible = True
            btnremove.Visible = True
            com.ExecuteNonQuery()
            connect.Close()


        Else
            Panelgridview.Visible = False

            btnaddcourse.Visible = True
            Msgbox1.ShowError("Courses have not been registered for " & combo1.Text & "")
        End If
        dr2.Close()
        getdetailsconnect.Close()
        getdetailsconnect.Dispose()

    End Sub

    Protected Sub ddlclass_SelectedIndexChanged(sender As Object, e As EventArgs)
        Using myconnection2 As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

            myconnection2.Open()
            Using mycommand As SqlCommand = myconnection2.CreateCommand
                mycommand.CommandType = CommandType.Text
                mycommand.CommandText = "select Courses from Departmentcourses where Department=('" & ddlclass.SelectedValue & "')"

                Dim _dt1 As DataTable = New DataTable()
                Dim _da1 As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da1.Fill(_dt1)
                If _dt1.Rows.Count > 0 Then
                    ListBox1.DataSource = _dt1
                    ListBox1.DataTextField = "Courses"
                    ListBox1.DataValueField = "Courses"
                    ListBox1.DataBind()
                    ListBox1.Items.Insert(0, New ListItem(String.Empty, String.Empty))

                End If
            End Using
        End Using
        panelcourses.Visible = True

    End Sub


    Protected Sub btnaddall_ServerClick(sender As Object, e As EventArgs)
        If ListBox1.SelectedIndex = 0 Or ListBox1.SelectedIndex = -1 Then
            Response.Write("<script>alert('please select an item');</script>")
            Exit Sub
        Else

            For Each item As ListItem In ListBox1.Items
                If item.Selected And Not ListBox2.Items.Contains(item) Then
                    Dim newItem As New ListItem(item.Text, item.Value)
                    ListBox2.Items.Add(newItem)

                End If

            Next
        End If
    End Sub

    Protected Sub btnsavecourse_ServerClick(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        Dim connect5 As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

        connect5.Open()

        For i As Integer = 0 To ListBox2.Items.Count - 1
            If ListBox2.Items(i).Selected = False Then
                count += 1
                Dim _cmd5 As SqlCommand = connect5.CreateCommand()
                _cmd5.CommandType = CommandType.Text
                _cmd5.CommandText = "INSERT INTO CourseReg ([RegID], [LastName], [FirstName], [MiddleName], [Class], [Term], [cousesdone]) VALUES (@regid, @lname, @fname, @mname, @class, @term, @course)"
                _cmd5.Parameters.AddWithValue("@regid", combo1.Text)
                _cmd5.Parameters.AddWithValue("@lname", txtlastname.Text)
                _cmd5.Parameters.AddWithValue("@fname", txtfirstname.Text)
                _cmd5.Parameters.AddWithValue("@mname", txtmiddlename.Text)
                _cmd5.Parameters.AddWithValue("@class", ddlclass.SelectedValue)
                _cmd5.Parameters.AddWithValue("@term", DropDownList1.SelectedValue)
                _cmd5.Parameters.AddWithValue("@course", ListBox2.Items(i).ToString())
                _cmd5.ExecuteNonQuery()





            End If

        Next
        Dim a As New Cls_Aggregates
        Dim rec As New Enrollment.Aggregate
        rec.CourseRegistered = count
        rec.RegID = combo1.Text
        rec.Term = DropDownList1.SelectedValue
        rec.TransID = System.Guid.NewGuid.ToString
        Dim res As ResponseInfo = A.Insert(rec)
        If res.ErrorCode = 0 Then
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Course(s) registered successfully');window.location='Student_profile.aspx?IDNo=" & combo1.Text & "';", True)
            connect5.Close()

        Else
            Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
        End If







    End Sub

    Protected Sub btnremove_ServerClick(sender As Object, e As EventArgs)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            Dim cb As CheckBox = DirectCast(GridView1.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
            If cb.Checked = True Then
                If cb IsNot Nothing AndAlso cb.Checked Then
                    Dim serial As String = DirectCast(GridView1.Rows(i).Cells(1).Controls(0), HyperLink).Text
                    Dim connect4 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                    connect4.Open()
                    Dim _cmd As SqlCommand = connect4.CreateCommand()
                    _cmd.CommandType = CommandType.Text
                    _cmd.CommandText = "delete from CourseReg where SN=('" & serial & "')"
                    _cmd.ExecuteNonQuery()
                    connect4.Close()
                    Response.Write("<script>alert('Courses Deleted successfully');</script>")

                End If

            End If
        Next
    End Sub

    Protected Sub btnaddcourse_ServerClick(sender As Object, e As EventArgs)
        panelcourses.Visible = True
        btnaddcourse.Visible = False

    End Sub


    

    

   
  
End Class
