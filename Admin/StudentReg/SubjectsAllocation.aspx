﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="SubjectsAllocation.aspx.vb" Inherits="Admin_StudentReg_SubjectsAllocation" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <h4 class="title">Subjects Allocation</h4>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>


    <div class="panel">
           <div class="content">
               <h5 class="hint-text">Allocate subjects to class</h5>
               <hr />
               


               
              
<asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                         <asp:HyperLinkField HeaderText="class" ItemStyle-CssClass="fg-red" DataNavigateUrlFields="class" DataTextField="class"
                    DataNavigateUrlFormatString="SubjectAllocation_edit.aspx?class={0}"/>
                          <asp:BoundField DataField="Category" HeaderText="Class category" SortExpression="Category" />
                     
                     
                   <asp:TemplateField HeaderText="Allocate">
                    <ItemTemplate>
                       <a href="SubjectAllocation_edit.aspx?class=<%#Eval("class")%>" class=""><span class="mif-pencil"></span></a>
                    </ItemTemplate>
                </asp:TemplateField>
            
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-secondary" Height="30px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
     

             
              
         <span> <button runat="server" id="btnallocate" onserverclick="btnallocate_ServerClick" class="mini-button button fg-white bg-lightGreen"><span class="mif-arrow-right text-bold"></span> Allocate</button></span>


               
               
               </div>
         </div>
</asp:Content>

