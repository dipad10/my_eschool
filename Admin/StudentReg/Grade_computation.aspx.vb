﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Admin_StudentReg_Grade_computation
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If _Connection.State = ConnectionState.Open Then
            _Connection.Close()

        End If
        If Not Page.IsPostBack Then

            loadGrid()
        End If
    End Sub


    Private Sub loadGrid()

        _Connection.Open()
        Dim _cmd As SqlCommand = _Connection.CreateCommand()
        _cmd.CommandType = CommandType.Text
        _cmd.CommandText = "select * from GradeCompute ORDER BY RegID DESC"
        _cmd.ExecuteNonQuery()


        Dim dt As New DataTable()
        Dim sda As New SqlDataAdapter(_cmd)

        sda.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            Msgbox1.ShowHelp("No record found")
        End If
    End Sub

    Protected Sub btnnewcompute_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/StudentReg/Grade_computation_edit.aspx")
    End Sub

    

    Protected Sub btnprocess_Click(sender As Object, e As EventArgs)
        'Dim SelectedIDs As New ArrayList

        'Dim count As Integer = 0

        'For i As Integer = 0 To GridView1.Rows.Count - 1
        '    Dim cb As CheckBox = DirectCast(GridView1.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
        '    If cb.Checked = True Then
        '        count += 1

        '        'Dim num As Integer = 0
        '        Dim SerialNumbers As ArrayList = SelectedIDs
        '        If cb IsNot Nothing AndAlso cb.Checked Then
        '            'Put your process here to process the students result using stored procedure into the database

        '            For Each SN In SerialNumbers
        '                Dim Old As Enrollment.GradeCompute = (New Cls_GradeCompute).SelectThisSN(SN)
        '                Dim A As New Enrollment.DataClassesDataContext
        '            Next


        '            Msgbox1.Showsuccess("" & count & "  students result processed!")

        '        End If

        '    Else

        '    End If

        'Next

        ''Dim OptionType As String = Request.QueryString("Option")
        ''Dim Gridview1 As GridView = Me.GridView1
        ''Dim SelectedIDs As New ArrayList

        ' ''For Each r As GridViewRow In Gridview1.Rows
        ' ''    If CType(r.Cells(0).Controls(0), ).checked Then

        ' ''    End If
        ' ''Next
        ''For Each r As GridViewRow In Gridview1.Rows
        ''    If CType(r.Cells(0).Controls(0), SelectorField.sele).Checked Then
        ''        SelectedIDs.Add(r.Cells(1).Text)
        ''    End If
        ''Next


    End Sub

    Protected Sub btnapply_Click(sender As Object, e As EventArgs)

    End Sub

    
End Class
