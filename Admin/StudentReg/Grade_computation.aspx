﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="Grade_computation.aspx.vb" Inherits="Admin_StudentReg_Grade_computation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
    <h4 class="title">Grade Computations</h4>
     <div class="accordion" data-role="accordion" data-close-any="true">
      <div class="accordion">
    <div class="frame">
        <div class="heading">Search filter</div>
        <div class="content"><table>
         <tbody>
             <tr>
                 <td><span class=" text-small">Where</span></td> <td><span><asp:DropDownList BorderColor="Orange" Width="110px" CssClass="text-small" Height="20px" BorderWidth="1" ID="DropDownList1" runat="server">
                     <asp:ListItem Value="IDNo">Student No</asp:ListItem>
                     <asp:ListItem Value="FirstName">Student name</asp:ListItem>
                    
                     
                            </asp:DropDownList></span> </td>
                 <td><span class="text-small">Contains</span></td> <td><asp:TextBox ID="TextBox1" Width="110px" Height="20px" BorderColor="Orange" BorderWidth="1" CssClass="text-small" runat="server"></asp:TextBox></td>
                 <td><span class="text-small">Between</span> </td>
                      <td>

                     
                         <dx:ASPxDateEdit ID="datebetween" runat="server" CssClass="text-small" Height="20px" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="110px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                  </td>
                  
                 <td><span class="text-small">And</span>  </td> 
                         <td>
                              <dx:ASPxDateEdit ID="dateand" CssClass="text-small" Height="20px" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="110px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                         </td>
                        
<td>                                                         <button runat="server" id="btnapply" height="20px" onserverclick="btnapply_Click" class="button bg-green fg-white text-small mini-button"><span class=" mif-search"></span> Search</button>

</td>
             </tr>
             
         </tbody>

     </table></div>
    </div>
          </div>
         
        </div>
    <div>
        <div class="panel">
           <div class="content bg-white">
               <asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <asp:CheckBox ID="chkall" runat="server" onclick="checkAll(this);" />
                             </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" onclick = "Check_Click(this)" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                       
                       
                       <asp:HyperLinkField HeaderText="Student No." DataNavigateUrlFields="RegID,Subject" DataTextField="RegID"
                    DataNavigateUrlFormatString="Grade_computation_edit.aspx?edit-id={0}&amp;Subject={1}"/>

                          <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                        <asp:BoundField DataField="LastName" HeaderText="Surname" SortExpression="LastName" />

                       <asp:BoundField DataField="AcademicSession" HeaderText="Session" SortExpression="AcademicSession" />    
<asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />    
                   <asp:BoundField DataField="CATotal" HeaderText="CATotal" SortExpression="CATotal" />    
                     <asp:BoundField DataField="ExamScore" HeaderText="ExamScore" SortExpression="ExamScore" />    
                        <asp:BoundField DataField="TotalExamScore" HeaderText="TotalExamscore" SortExpression="TotalExamScore" />    
                   
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                          <button runat="server" id="btnnewcompute" onserverclick="btnnewcompute_ServerClick" class="button bg-lightGreen fg-white mini-button"><span class="mif-plus"></span> New Grade Computation</button>
                      </td>
                          <td>
                              <button runat="server" id="btncancel" OnClientClick="JavaScript:window.history.back(1); return false;" class="button bg-green fg-white mini-button"><span class="mif-backward"></span> Cancel</button>

                           </td>
                           <td>
                               <asp:Button ID="btnprocess" Visible="true" Height="23px" OnClick="btnprocess_Click" CssClass="button bg-green text-small fg-white mini-button" runat="server" Text="Process Result" />
                           </td>
                           
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>
    </div>
</asp:Content>

