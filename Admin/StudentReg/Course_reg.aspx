﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="Course_reg.aspx.vb" Inherits="Admin_StudentReg_Course_reg" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
     <h4 class="title">Subjects Allocation</h4>
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
     <div class="panel">
           <div class="content">
               <h5 class="hint-text">Register student course(s)</h5>
               <hr />
               <div class="grid">
                   <div class="row cell">
                       <table width="80%">
    <tr height="50px">
        <td width="230px"><span class="text-secondary bold">Search Student by Reg No/Name:</span></td>
        <td>
            <span>
    <dx:ASPxComboBox ID="combo1" OnSelectedIndexChanged="combo1_SelectedIndexChanged" AutoPostBack="true" AutoResizeWithContainer="True" AnimationType="Slide" ValueType="System.String" IncrementalFilteringMode="Contains" Border-BorderColor="DarkBlue" OnItemsRequestedByFilterCondition="combo1_ItemsRequestedByFilterCondition" CallbackPageSize="10" EnableCallbackMode="True" OnItemRequestedByValue="combo1_ItemRequestedByValue" runat="server" EnableTheming="True" Theme="SoftOrange">

</dx:ASPxComboBox>
</span>
        </td>
    </tr>
                           <tr>
                               <td>

                               <span class="text-secondary text-bold">Firstname</span>    <asp:TextBox ID="txtfirstname" ReadOnly="true" BorderColor="Orange" CssClass="uppercase" BorderWidth="1" runat="server"></asp:TextBox></td>
                               <td width="230px">
                                <span class="text-secondary  text-bold">Middlename</span>   <asp:TextBox ID="txtmiddlename" ReadOnly="true" BorderColor="Orange" CssClass="uppercase" BorderWidth="1" runat="server"></asp:TextBox>
                               </td>
                               <td width="230px">
                                  <span class="text-secondary  text-bold">Lastname</span> <asp:TextBox ID="txtlastname" ReadOnly="true" BorderColor="Orange" CssClass="uppercase" BorderWidth="1" runat="server"></asp:TextBox>
                               </td>
                           </tr>
</table>
                    </div>
                   </div>


               
               <asp:Panel ID="Panelgridview" runat="server">
<asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                         <asp:HyperLinkField HeaderText="CourseID" ItemStyle-CssClass="fg-red" DataNavigateUrlFields="SN" DataTextField="SN"
                    DataNavigateUrlFormatString="Student_profile.aspx?IDNo={0}"/>

                     
                       <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname" />
                       <asp:BoundField DataField="LastName" HeaderText="Lastname" SortExpression="Lastname" />
                       <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Status" />
              <asp:BoundField DataField="Term" HeaderText="Term" SortExpression="Term" />
                   <asp:BoundField DataField="cousesdone" HeaderText="Courses Registered" SortExpression="coursesdone" />
                   
            
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-secondary" Height="30px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
       <span> <button runat="server" id="btnremove" onserverclick="btnremove_ServerClick" class="mini-button button fg-white bg-darkRed"><span class="mif-arrow-right text-bold"></span>Remove Course(s)</button></span>

               </asp:Panel>
               <br />
         <span> <button runat="server" id="btnaddcourse" onserverclick="btnaddcourse_ServerClick" class="mini-button button fg-white bg-lightGreen"><span class="mif-arrow-right text-bold"></span> Add new Course(s)</button></span>

               <asp:Panel ID="panelcourses" Visible="false" runat="server">
                   <div class="grid">
                   <div class="row cells2">
                       <div class="cell">
                       <p class="text-secondary text-bold">Class:</p>
                           <span>
                               <asp:DropDownList ID="ddlclass" OnSelectedIndexChanged="ddlclass_SelectedIndexChanged" AutoPostBack="true" Width="200" runat="server">
                                  
                               </asp:DropDownList></span>
                            <asp:ListBox ID="ListBox1" SelectionMode="Multiple" Height="200"
                                Width="300" runat="server">
                                
                            </asp:ListBox>
                       </div>
                       
                       

                        <div class="cell">

                              <p class="text-secondary text-bold">Term:</p>
                           <span>
                               <asp:DropDownList ID="DropDownList1" Width="200" runat="server">
                                    <asp:ListItem Value="null" Enabled="true">--Select term--</asp:ListItem>
                                   <asp:ListItem Value="1st Term">1st Term</asp:ListItem>
                                   <asp:ListItem Value="2nd Term">2nd Term</asp:ListItem>
                                   <asp:ListItem Value="3rd Term">3rd Term</asp:ListItem>
                                   
                               </asp:DropDownList></span>
                            <asp:ListBox ID="ListBox2" Height="200"
                                Width="300" runat="server">
                                
                            </asp:ListBox>
                       </div>

                   </div>
                   <br />
                              <span>

                                  <asp:Button ID="Button1" OnClick="btnaddall_ServerClick" runat="server" Text="Add selected items" />
                              </span>
                                 <span> <button runat="server" id="btnsavecourse" onserverclick="btnsavecourse_ServerClick" class="mini-button button fg-white bg-darkBlue"><span class="mif-arrow-right text-bold"></span> Save Course(s)</button></span>


               </div>

               </asp:Panel>
               
               </div>
         </div>





    

<asp:SqlDataSource ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:Enrollment_Net10ConnectionString %>" runat="server"></asp:SqlDataSource>

</asp:Content>

