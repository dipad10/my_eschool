﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Imports DevExpress.XtraGrid.Views.Base

Partial Class webroot_web_modules_StudentReg_StudentInfo
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    If _Connection.State = ConnectionState.Open Then
    '        _Connection.Close()

    '    End If
    '    If Not Page.IsPostBack Then

    '        loadGrid()

    '        datebetween.Date = Date.Now.ToString("dd-MMM-yyyy")
    '        dateand.Date = Date.Now.ToString("dd-MMM-yyyy")
    '    End If
    'End Sub

    'Private Sub loadGrid()
    '    _Connection.Open()
    '    Dim _cmd As SqlCommand = _Connection.CreateCommand()
    '    _cmd.CommandType = CommandType.Text
    '    _cmd.CommandText = "select * from StudentRegistration ORDER BY IDNo DESC"
    '    _cmd.ExecuteNonQuery()


    '    Dim dt As New DataTable()
    '    Dim sda As New SqlDataAdapter(_cmd)

    '    sda.Fill(dt)
    '    GridView1.DataSource = dt
    '    GridView1.DataBind()

    '    If GridView1.Rows.Count = 0 Then
    '        Response.Write("<script>alert('No record found');</script>")
    '    End If
    'End Sub
  



   

    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("Students_edit.aspx")
    End Sub

  
  

    

    'Protected Sub btnexport_ServerClick(sender As Object, e As EventArgs)
    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.AddHeader("content-disposition", "attachment;filename=StudentRecords.xls")
    '    Response.Charset = ""
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Using sw As New StringWriter()
    '        Dim hw As New HtmlTextWriter(sw)

    '        'To Export all pages
    '        GridView1.AllowPaging = False
    '        Me.loadGrid()

    '        GridView1.HeaderRow.BackColor = Color.White
    '        For Each cell As TableCell In GridView1.HeaderRow.Cells
    '            cell.BackColor = GridView1.HeaderStyle.BackColor
    '        Next
    '        For Each row As GridViewRow In GridView1.Rows
    '            row.BackColor = Color.White
    '            For Each cell As TableCell In row.Cells
    '                If row.RowIndex Mod 2 = 0 Then
    '                    cell.BackColor = GridView1.AlternatingRowStyle.BackColor
    '                Else
    '                    cell.BackColor = GridView1.RowStyle.BackColor
    '                End If
    '                cell.CssClass = "textmode"
    '            Next
    '        Next

    '        GridView1.RenderControl(hw)
    '        'style to format numbers to string
    '        Dim style As String = "<style> .textmode { } </style>"
    '        Response.Write(style)
    '        Response.Output.Write(sw.ToString())
    '        Response.Flush()
    '        Response.[End]()
    '    End Using
    'End Sub
    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        ' Verifies that the control is rendered
    End Sub


   
    'Protected Sub btnapply_ServerClick(sender As Object, e As EventArgs)
    '    If txtcontains.Text <> "" Then
    '        Select Case ddlfilter.SelectedValue
    '            Case "FirstName"
    '                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
    '                connect.Open()

    '                Dim com As SqlCommand = connect.CreateCommand
    '                com.CommandType = CommandType.Text
    '                com.CommandText = "select * from StudentRegistration where FirstName like '%" & txtcontains.Text & "%'"
    '                'com.Parameters.AddWithValue("@name", DropDownList1.SelectedValue)


    '                com.ExecuteNonQuery()

    '                Dim dt As New DataTable()
    '                Using sda As New SqlDataAdapter(com)
    '                    sda.Fill(dt)
    '                    GridView1.DataSource = dt
    '                    GridView1.DataBind()
    '                    If GridView1.Rows.Count = 0 Then
    '                        Response.Write("<script>alert('No record found');</script>")
    '                    End If
    '                End Using

    '                connect.Close()

    '            Case "IDNo"
    '                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
    '                connect.Open()

    '                Dim com As SqlCommand = connect.CreateCommand
    '                com.CommandType = CommandType.Text
    '                com.CommandText = "select * from StudentRegistration where IDNo like '%" & txtcontains.Text & "%'"
    '                'com.Parameters.AddWithValue("@name", DropDownList1.SelectedValue)


    '                com.ExecuteNonQuery()

    '                Dim dt As New DataTable()
    '                Using sda As New SqlDataAdapter(com)
    '                    sda.Fill(dt)
    '                    GridView1.DataSource = dt
    '                    GridView1.DataBind()
    '                    If GridView1.Rows.Count = 0 Then
    '                        Response.Write("<script>alert('No record found');</script>")
    '                    End If
    '                End Using

    '                connect.Close()
    '        End Select


    '    Else  'if txt contain is empty then it should search by date
    '        Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
    '        connect.Open()

    '        Dim com As SqlCommand = connect.CreateCommand
    '        com.CommandType = CommandType.Text
    '        com.CommandText = " SELECT * FROM StudentRegistration WHERE CAST(AdmissionDate AS date) BETWEEN '" & datebetween.Date & "'  and '" & dateand.Date & "' "


    '        com.ExecuteNonQuery()

    '        Dim dt As New DataTable()
    '        Using sda As New SqlDataAdapter(com)
    '            sda.Fill(dt)
    '            GridView1.DataSource = dt
    '            GridView1.DataBind()
    '            If GridView1.Rows.Count = 0 Then
    '                Response.Write("<script>alert('No record found');</script>")
    '            End If
    '        End Using

    '        connect.Close()

    '    End If

    'End Sub

    'Protected Sub ddlsort_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Select Case ddlsort.SelectedValue
    '        Case "A"
    '            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
    '            connect.Open()

    '            Dim com As SqlCommand = connect.CreateCommand
    '            com.CommandType = CommandType.Text
    '            com.CommandText = "select * from StudentRegistration where Enrollment_type='DAY'"
    '            'com.Parameters.AddWithValue("@name", DropDownList1.SelectedValue)


    '            com.ExecuteNonQuery()

    '            Dim dt As New DataTable()
    '            Using sda As New SqlDataAdapter(com)
    '                sda.Fill(dt)
    '                GridView1.DataSource = dt
    '                GridView1.DataBind()
    '                If GridView1.Rows.Count = 0 Then
    '                    Response.Write("<script>alert('No record found');</script>")
    '                End If
    '            End Using

    '            connect.Close()

    '        Case "B"
    '            Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
    '            connect.Open()

    '            Dim com As SqlCommand = connect.CreateCommand
    '            com.CommandType = CommandType.Text
    '            com.CommandText = "select * from StudentRegistration where Enrollment_type='BOARDING'"
    '            'com.Parameters.AddWithValue("@name", DropDownList1.SelectedValue)


    '            com.ExecuteNonQuery()

    '            Dim dt As New DataTable()
    '            Using sda As New SqlDataAdapter(com)
    '                sda.Fill(dt)
    '                GridView1.DataSource = dt
    '                GridView1.DataBind()
    '                If GridView1.Rows.Count = 0 Then
    '                    Response.Write("<script>alert('No record found');</script>")
    '                End If
    '            End Using

    '            connect.Close()
    '    End Select

    'End Sub
    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim studentno As String = ASPxGridView1.GetSelectedFieldValues("IDNo")(i).ToString
            _Connection.Open()
            Dim _cmd As SqlCommand = _Connection.CreateCommand()
            _cmd.CommandType = CommandType.Text
            _cmd.CommandText = "delete from StudentRegistration where IDNo=('" & studentno & "')"
            _cmd.ExecuteNonQuery()
            _Connection.Close()

            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "swal('Successful!', 'Student deleted Successfully!', 'success');window.location='Students.aspx';", True)

            'Msgbox1.Showsuccess("" & count & " Student(s) deleted successfully!")



        Next

  

    End Sub
    Protected Sub cmdprint_Click(sender As Object, e As EventArgs)

        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1

            Dim studentno As String = ASPxGridView1.GetSelectedFieldValues("IDNo")(i).ToString
            Dim report As New Student_profile


            ' Obtain a parameter, and set its value.
            report.Parameters("studentsID").Value = studentno

            ' Hide the Parameters UI from end-users.
            report.Parameters("studentsID").Visible = False

            ' Show the report's print preview.
            report.CreateDocument()
            Dim ms As New MemoryStream()

            Try
                report.PrintingSystem.SaveDocument(ms)
                ms.Seek(0, SeekOrigin.Begin)
                Dim array As Byte() = ms.ToArray()
                Session("Report") = array
            Finally
                ms.Close()
            End Try

            ShowPreview()



        Next


       
    End Sub

    Sub ShowPreview()
        'Response.Redirect("ASPNETDocumentViewer.aspx")
        Response.Redirect("/Admin/Report/Viewer.aspx")
    End Sub

  
End Class
