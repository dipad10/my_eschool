﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web
Partial Class Admin_StudentReg_Grade_computation_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtfirstname.Enabled = False
        txtmiddlename.Enabled = False
        txtlastname.Enabled = False
        txtterm.Enabled = False
        txtCourseReg.Enabled = False
        txtAggCourse.Enabled = False

        If Not Page.IsPostBack Then
            Dim studentid As String = Request.QueryString("edit-id")
            Dim subjectid As String = Request.QueryString("subject")


            BindDt(Me.DataGrid1, "Table1")

            If studentid <> "" Then
                'fetch old records into textboxes
                Dim rec As Enrollment.GradeCompute = (New Cls_GradeCompute).SelectThis(studentid, subjectid)
                combo1.Text = rec.RegID
                combo1.Enabled = False
                txtfirstname.Text = rec.FirstName
                txtlastname.Text = rec.LastName
                txtmiddlename.Text = rec.Othernames
                ddlacademicsession.SelectedValue = rec.AcademicSession
               

                txtterm.Text = rec.Term
                txtca1.Text = rec.CA1
                txtca2.Text = rec.CA2
                txtca3.Text = rec.CA3
                txtca4.Text = rec.CA4
                ddlsubjects.SelectedValue = rec.Subject
                txtscore.Text = rec.ExamScore
                txtcomments.Text = rec.Comment

                lblgradecomputeheader.InnerText = "Student '" & studentid & "' Grade computations Edit"



                Dim agg As Enrollment.Aggregate = (New Cls_Aggregates).SelectThis(studentid)
                txtCourseReg.Text = agg.CourseRegistered
                txtAggCourse.Text = agg.Aggregate
                bindsubjects()
            Else
                lblgradecomputeheader.InnerText = "New Grade Computation"
            End If
            Me.bindcombo()

            'BindDt(Me.GridView1, "tableopt1")
            CreateGrid(studentid)
           
        End If
    End Sub

    Private Sub bindcombo()
        Dim constr As String = ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT IDNo, FirstName, LastName FROM StudentRegistration"
                cmd.Connection = con

                Dim dt As New DataTable()
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dt)
                    combo1.DataSource = dt
                    combo1.Columns.Clear()
                    combo1.Columns.Add("IDNo").Width = 120
                    combo1.Columns.Add("FirstName").Width = 170
                    combo1.Columns.Add("LastName").Width = 200

                    combo1.ValueField = "IDNo"
                    combo1.TextField = "FirstName"


                    combo1.TextFormatString = "{0}"
                    combo1.DataBind()

                End Using
            End Using
        End Using


    End Sub

    Protected Sub combo1_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)



        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource1.SelectCommand = "SELECT [IDNo], [FirstName], [LastName] FROM (select [IDNo], [FirstName], [LastName], row_number()over(order by t.[LastName]) as [rn] from [StudentRegistration] as t where (([FirstName] + ' ' + [LastName] + ' ' + [IDNo]) LIKE @filter)) as st where st.[rn] between @startIndex and @endIndex"

        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("filter", TypeCode.String, String.Format("%{0}%", e.Filter))
        SqlDataSource1.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString())
        SqlDataSource1.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString())
        comboBox.DataSource = SqlDataSource1
        comboBox.DataBind()

    End Sub

    Protected Sub combo1_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        Dim value As Long = 0
        If e.Value Is Nothing OrElse (Not Int64.TryParse(e.Value.ToString(), value)) Then
            Return
        End If
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource1.SelectCommand = "SELECT IDNo, LastName, FirstName FROM StudentRegistration WHERE (IDNo = @ID) ORDER BY FirstName"

        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("ID", TypeCode.String, e.Value.ToString())
        comboBox.DataSource = SqlDataSource1
        comboBox.DataBind()

    End Sub

    Protected Sub combo1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim rec As Enrollment.StudentRegistration = (New Cls_StudentRegistration).SelectThisIDNo(combo1.Text)
        Dim rec2 As Enrollment.Session = (New Cls_Session).Selectcurrentsession()
        Dim rec3 As Enrollment.Term = (New Cls_Term).Selectcurrentterm

        txtfirstname.Text = rec.FirstName
        txtlastname.Text = rec.LastName
        txtmiddlename.Text = rec.MiddleName
        Dim classname As String = rec.Field2
        Dim getcoursereg As Enrollment.Class = (New Cls_classes).SelectThisClassname(classname)

        ddlacademicsession.SelectedValue = rec2.Session
        txtterm.Text = rec3.Term
        txtCourseReg.Text = getcoursereg.CoursesRegisterd

        bindsubjects()
    




    End Sub

    Private Sub bindsubjects()
        Dim studentid As String = Request.QueryString("edit-id")
        Dim subjectid As String = Request.QueryString("subject")
        'if its for editing pick score from grade computing.


        If studentid <> "" Then
            Try
                Dim constr As String = ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString
                Using con As New SqlConnection(constr)
                    Using cmd As New SqlCommand()
                        cmd.CommandText = "SELECT * FROM GradeCompute where RegID=@regid and subject=@subject"
                        cmd.Parameters.AddWithValue("@regid", studentid)
                        cmd.Parameters.AddWithValue("@subject", subjectid)
                        cmd.Connection = con

                        Dim dt As New DataTable()

                        Using sda As New SqlDataAdapter(cmd)
                            sda.Fill(dt)
                            ddlsubjects.DataSource = dt
                            ddlsubjects.DataTextField = "subject"
                            ddlsubjects.DataValueField = "subject"
                            ddlsubjects.DataBind()

                        End Using

                        If ddlsubjects.Items.Count <= 0 Then
                            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Course(s) have not been registered for this student');window.location='Course_reg.aspx?IDNo=" & combo1.Text & "';", True)

                        End If
                    End Using
                End Using

            Catch ex As Exception
                Msgbox1.ShowError(ex.Message)
            End Try
        Else
            Try

                Dim stud As Enrollment.StudentRegistration = (New Cls_StudentRegistration).SelectThisIDNo(combo1.Text)
                Dim classname As String = stud.Field2

                Dim subjects As List(Of Enrollment.SubjectAllocation) = (New Cls_subjectallocation).Selectsubjectlist(classname)


                ddlsubjects.DataSource = subjects
                ddlsubjects.DataTextField = "subjects"
                ddlsubjects.DataValueField = "subjects"
                ddlsubjects.DataBind()



                If ddlsubjects.Items.Count <= 0 Then
                    Msgbox1.ShowHelp("Subjects has not been allocated to " & classname & "")
                End If

            Catch ex As Exception
                Msgbox1.ShowError(ex.Message)
            End Try

        End If
    End Sub
    Private Sub CreateGrid(ByVal RegID As String)

        Dim Recs As List(Of Enrollment.GradeCompute) = (New Cls_GradeCompute).SelectThisRegID(RegID)

        Dim da As DataTable = GetDt("table1")
        'A loop is needed here to list all existing records
        For Each r In Recs
            Dim dr As DataRow = da.NewRow()
            dr(0) = da.Rows.Count + 1
            dr(1) = r.RegID
            dr(2) = r.AcademicSession
            dr(3) = r.Term
            dr(4) = r.Subject
            dr(5) = r.Comment
            dr(6) = CDbl(r.CA1).ToString
            dr(7) = CDbl(r.CA2).ToString
            dr(8) = CDbl(r.ExamScore).ToString
            dr(9) = CDbl(r.CA3).ToString

            da.Rows.Add(dr)
        Next

        saveDt("table1", da)
        BindDt(DataGrid1, "table1")
    End Sub
    Private Sub saveDt(ByVal tablename As String, ByVal newDt As DataTable)
        ViewState(tablename) = newDt
    End Sub
    Private Sub BindDt(ByVal DtGrid As DataGrid, ByVal tablename As String)
        Dim Dt As DataTable = GetDt(tablename)
        Dim Dv As New DataView(Dt)
        ViewState(tablename) = Dt
        DtGrid.DataSource = Dv
        DtGrid.DataBind()

    End Sub
    Private Function GetDt(ByVal tablename As String) As DataTable
        If TypeOf ViewState(tablename) Is DataTable Then
            Return CType(ViewState(tablename), DataTable)
        Else
            Dim Dt As New DataTable
            Dt.Columns.Add(New DataColumn("SN", GetType(Int32)))
            Dt.Columns.Add(New DataColumn("Reg No", GetType(String)))
            Dt.Columns.Add(New DataColumn("Session", GetType(String)))
            Dt.Columns.Add(New DataColumn("Term", GetType(String)))
            Dt.Columns.Add(New DataColumn("Subject", GetType(String)))
            Dt.Columns.Add(New DataColumn("Comment", GetType(String)))
            Dt.Columns.Add(New DataColumn("CA1", GetType(Decimal)))
            Dt.Columns.Add(New DataColumn("CA2", GetType(Decimal)))
            Dt.Columns.Add(New DataColumn("Exam Score", GetType(Decimal)))
            Dt.Columns.Add(New DataColumn("Total CA", GetType(Decimal)))


            Return Dt
        End If
    End Function

    'Protected Sub btnsave_ServerClick(sender As Object, e As EventArgs) Handles btnsave.ServerClick

    '    Dim subject As String = Request.QueryString("subject")
    '    Dim RegID As String = Request.QueryString("edit-id")

    '    Dim dt As DataTable = GetDt("table1")
    '    Dim r As DataRow, Q As Int16 = 1

    '    Dim Conn As New SqlConnection(m_strconnString)
    '    Conn.Open()
    '    Dim Trans As SqlTransaction = Conn.BeginTransaction

    '    Dim D As New Cls_GradeCompute
    '    For Each r In dt.Rows
    '        r(0) = Q : Q += 1

    '        'This code block uses d ReceiptID declared as a variable to call d querystring for d edit function
    '        If RegID <> "" Then

    '            Dim Rec As Enrollment.GradeCompute = D.SelectThis(RegID, subject)

    '            'Rec.RegID = r(1)
    '            'Rec.FirstName = Me.txtfirstname.Text
    '            'Rec.LastName = Me.txtlastname.Text
    '            'Rec.Othernames = Me.txtmiddlename.Text
    '            'Rec.AcademicSession = r(2) 'Me.DropDownList1.SelectedValue
    '            'Rec.Term = r(3) 'Me.txtterm.Text
    '            'Rec.Subject = r(4) 'Me.ddlsubjects.SelectedValue
    '            'Rec.CA1 = r(5) 'Me.txtca1.Text
    '            'Rec.CA2 = r(6) 'Me.txtca2.Text
    '            'Rec.CA3 = Me.txtca3.Text
    '            'Rec.CA4 = Me.txtca4.Text
    '            'Rec.CATotal = Me.txttotalCa.Text
    '            'Rec.ExamScore = r(7) 'me.txtscore.text
    '            'Rec.TotalExamScore = Me.txttotalexam.Text
    '            'Rec.Comment = r(8) 'Me.txtcomments.Text
    '            'Rec.TransGUID = System.Guid.NewGuid.ToString
    '            'Rec.Status = "PENDING"
    '            'Rec.SubmittedBy = Session("uname").ToString()
    '            'Rec.SubmittedOn = Date.Now.ToString("dd MMMM yyyy")
    '            'Rec.ModifiedBy = Session("uname").ToString()
    '            'Rec.ModifiedOn = Date.Now.ToString("dd MMMM yyyy")
    '            'Rec.Tag = "AUTO_GRADE_PROCESS"

    '            ''Comitt the postings into Database here
    '            ''Dim response As ResponseInfo = D.Update(Rec)
    '            'Dim Response As ResponseInfo = (New Cls_GradeCompute).Insert(Rec)
    '            'If response.ErrorCode = 0 Then
    '            'Else
    '            '    Trans.Rollback()
    '            '    Me.Msgbox1.ShowError(response.ErrorMessage & "" & response.ExtraMessage)

    '            '    Return

    '            'End If

    '        Else

    '            Dim Rec As New Enrollment.GradeCompute
    '            Rec.RegID = r(1)
    '            Rec.FirstName = Me.txtfirstname.Text
    '            Rec.LastName = Me.txtlastname.Text
    '            Rec.Othernames = Me.txtmiddlename.Text
    '            Rec.AcademicSession = r(2) 'Me.DropDownList1.SelectedValue
    '            Rec.Term = r(3) 'Me.txtterm.Text
    '            Rec.Subject = r(4) 'Me.ddlsubjects.SelectedValue
    '            Rec.CA1 = r(6) 'Me.txtca1.Text
    '            Rec.CA2 = r(7) 'Me.txtca2.Text
    '            Rec.CA3 = Me.txtca3.Text
    '            Rec.CA4 = Me.txtca4.Text
    '            Rec.CATotal = Me.txttotalCa.Text
    '            Rec.ExamScore = r(8) 'me.txtscore.text
    '            Rec.TotalExamScore = Me.txttotalexam.Text
    '            Rec.Comment = r(5) 'Me.txtcomments.Text
    '            Rec.TransGUID = System.Guid.NewGuid.ToString
    '            Rec.Status = "PENDING"
    '            Rec.SubmittedBy = Session("uname").ToString()
    '            Rec.SubmittedOn = Date.Now.ToString("dd MMMM yyyy")
    '            Rec.ModifiedBy = Session("uname").ToString()
    '            Rec.ModifiedOn = Date.Now.ToString("dd MMMM yyyy")
    '            Rec.Tag = "AUTO_GRADE_PROCESS"

    '            'Comitt the postings into Database here
    '            Dim Response As ResponseInfo = (New Cls_GradeCompute).Insert(Rec)
    '            If Response.ErrorCode = 0 Then
    '                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Grade_computation.aspx';", True)

    '            Else
    '                Trans.Rollback()
    '                Me.Msgbox1.ShowError(Response.ErrorMessage & "" & Response.ExtraMessage)
    '                Return
    '            End If
    '        End If
    '    Next
    '    Trans.Commit()
    '    Return

    'End Sub

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Private Sub grade_computation_edit_load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Dt As New DataTable
        Dt.Columns.Add(New DataColumn("Reg No", GetType(String)))
        Dt.Columns.Add(New DataColumn("Session", GetType(String)))
        Dt.Columns.Add(New DataColumn("Term", GetType(String)))
        Dt.Columns.Add(New DataColumn("Subject", GetType(String)))
        Dt.Columns.Add(New DataColumn("Comment", GetType(String)))
        Dt.Columns.Add(New DataColumn("CA1", GetType(Decimal)))
        Dt.Columns.Add(New DataColumn("CA2", GetType(Decimal)))
        Dt.Columns.Add(New DataColumn("Exam Score", GetType(Decimal)))
        Dt.Columns.Add(New DataColumn("Total CA", GetType(Decimal)))

    End Sub

    Protected Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click

        If Me.txtscore.Text >= Me.txttotalexam.Text Then
            Msgbox1.ShowError("Exam Score can not be greater than (60)")
            Exit Sub
        End If
        If Me.txtscore.Text = 0 Then
            Msgbox1.ShowError("Exam Score can not be 0")
            Exit Sub
        End If
        If Me.txtca1.Text >= Me.txttotalCa.Text Then
            Msgbox1.ShowError("CA Score can not be greater than (40)")
            Exit Sub
        End If
        If Me.txtca1.Text = 0 Then
            Msgbox1.ShowError("CA Score can not be 0")
            Exit Sub
        End If
        If Me.txtca2.Text >= Me.txttotalCa.Text Then
            Msgbox1.ShowError("CA Score can not be greater than (40)")
            Exit Sub
        End If
        If Me.txtca2.Text = 0 Then
            Msgbox1.ShowError("CA Score can not be 0")
            Exit Sub
        End If
        Me.txtca3.Text = CDbl(Me.txtca1.Text) + CDbl(Me.txtca2.Text)
        If Me.txtca3.Text >= Me.txttotalCa.Text Then
            Msgbox1.ShowError("Total CA Score can not be greater than (40)")
            Exit Sub
        End If
        Me.txtSubjectScore.Text = CDbl(Me.txtSubjectScore.Text) + CDbl(Me.txtscore.Text)

        Dim dt As DataTable = GetDt("table1")
        Dim dr As DataRow = dt.NewRow
        dr(0) = dt.Rows.Count + 1
        dr(1) = combo1.Text
        dr(2) = Me.ddlacademicsession.SelectedValue
        dr(3) = Me.txtterm.Text
        dr(4) = Me.ddlsubjects.SelectedValue
        dr(5) = Me.txtcomments.Text
        dr(6) = CDbl(Me.txtca1.Text)
        dr(7) = CDbl(Me.txtca2.Text)
        dr(8) = CDbl(Me.txtscore.Text)
        dr(9) = CDbl(Me.txtca3.Text)
        dt.Rows.Add(dr)
        BindDt(DataGrid1, "table1")


        Dim r As DataRow, P As Decimal
        For Each r In dt.Rows
            P += r(9)
        Next
        Me.txtterm.Text = dr(3)
        Me.ddlacademicsession.SelectedValue = dr(2)
        'Me.ddlsubjects.SelectedValue = ""
        Me.txtca1.Text = dr(6)
        Me.txtca2.Text = dr(7)
        Me.txtscore.Text = dr(8)
        Me.txtcomments.Text = dr(5)
        Me.txtca3.Text = dr(9)

    End Sub

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs) Handles cmdsave.Click
        Dim subject As String = Request.QueryString("subject")
        Dim RegID As String = Request.QueryString("edit-id")

        Dim Conn1 As New SqlConnection(m_strconnString)
        Conn1.Open()

        Dim Test As Integer
        Dim Test2 As Integer

        Test = CDbl(Me.txtAggCourse.Text) * 1
        Test2 = CDbl(Me.txttotalexam.Text) + CDbl(Me.txttotalCa.Text)
        Test = Test2 * CDbl(Me.txtCourseReg.Text)

        Dim Comm As SqlCommand = Conn1.CreateCommand
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "insert into Aggregates([RegID], [AcademicSession], [Term], [CourseRegistered], [SubjectScore], [Aggregate], [TransID]) values(@Rid, @Asession, @Term, @Cregistered, @Sscore, @Agg, @TID)"
        Comm.Parameters.AddWithValue("@Rid", combo1.Text)
        Comm.Parameters.AddWithValue("@Asession", ddlacademicsession.SelectedValue)
        Comm.Parameters.AddWithValue("@Term", Me.txtterm.Text)
        Comm.Parameters.AddWithValue("@Cregistered", Me.txtCourseReg.Text)
        Comm.Parameters.AddWithValue("@Sscore", CDbl(Me.txtSubjectScore.Text))
        Comm.Parameters.AddWithValue("@Agg", Test)
        Comm.Parameters.AddWithValue("@TID", System.Guid.NewGuid.ToString)
        Comm.ExecuteNonQuery()
        Conn1.Close()

        Dim dt As DataTable = GetDt("table1")
        Dim r As DataRow, Q As Int16 = 1

        Dim Conn As New SqlConnection(m_strconnString)
        Conn.Open()
        Dim Trans As SqlTransaction = Conn.BeginTransaction

        Dim D As New Cls_GradeCompute
        Dim A As New Cls_Aggregates
        'Dim Test As Integer
        'Dim Test2 As Integer

        'Test = CDbl(Me.txtAggCourse.Text) * 1
        'Test2 = CDbl(Me.txttotalexam.Text) + CDbl(Me.txttotalCa.Text)
        'Test = Test2 * CDbl(Me.txtCourseReg.Text)

            'This code block uses d ReceiptID declared as a variable to call d querystring for d edit function
            If RegID <> "" Then
                'update record
            Dim Rec As Enrollment.GradeCompute = D.SelectThis(RegID, subject)
            'Dim Ass As Enrollment.Aggregate = A.SelectThis(RegID)
            For Each r In dt.Rows
                r(0) = Q : Q += 1

                Rec.RegID = r(1)
                Rec.FirstName = Me.txtfirstname.Text
                Rec.LastName = Me.txtlastname.Text
                Rec.Othernames = Me.txtmiddlename.Text
                Rec.AcademicSession = r(2) 'Me.DropDownList1.SelectedValue
                Rec.Term = r(3) 'Me.txtterm.Text
                Rec.Subject = r(4) 'Me.ddlsubjects.SelectedValue
                Rec.CA1 = r(6) 'Me.txtca1.Text
                Rec.CA2 = r(7) 'Me.txtca2.Text
                Rec.CA3 = r(9)
                Rec.CA4 = Me.txtca4.Text
                Rec.CATotal = Me.txttotalCa.Text
                Rec.ExamScore = r(8) 'me.txtscore.text
                Rec.TotalExamScore = Me.txttotalexam.Text
                Rec.Comment = r(5) 'Me.txtcomments.Text
                Rec.TransGUID = System.Guid.NewGuid.ToString
                Rec.Status = "PENDING"
                Rec.SubmittedBy = Session("uname").ToString()
                Rec.SubmittedOn = Date.Now.ToString("dd MMMM yyyy")
                Rec.ModifiedBy = Session("uname").ToString()
                Rec.ModifiedOn = Date.Now.ToString("dd MMMM yyyy")
                Rec.Tag = "AUTO_GRADE_PROCESS"

                'Comitt the postings into Database here
                Dim Response As ResponseInfo = (New Cls_GradeCompute).Update(Rec)
                If Response.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Grade_computation.aspx';", True)

                Else
                    Trans.Rollback()
                    Me.Msgbox1.ShowError(Response.ErrorMessage & "" & Response.ExtraMessage)
                    Return
                End If
            Next

        Else 'create fresh record
            Dim Rec As New Enrollment.GradeCompute
            'Dim Ass As New Enrollment.Aggregate
            For Each r In dt.Rows
                r(0) = Q : Q += 1

                'Dim Comm As SqlCommand = Conn.CreateCommand
                'Comm.CommandType = CommandType.Text
                'Comm.CommandText = "insert into Aggregates([RegID], [AcademicSession], [Term], [CourseRegistered], [SubjectScore], [Aggregate], [TransID]) values(@Rid, @Asession, @Term, @Cregistered, @Sscore, @Agg, @TID)"
                'Comm.Parameters.AddWithValue("@Rid", combo1.Text)
                'Comm.Parameters.AddWithValue("@Asession", ddlacademicsession.SelectedValue)
                'Comm.Parameters.AddWithValue("@Term", Me.txtterm.Text)
                'Comm.Parameters.AddWithValue("@Cregistered", Me.txtCourseReg.Text)
                'Comm.Parameters.AddWithValue("@Sscore", CDbl(Me.txtSubjectScore.Text))
                'Comm.Parameters.AddWithValue("@Agg", Test)
                'Comm.Parameters.AddWithValue("@TID", System.Guid.NewGuid.ToString)
                'Comm.ExecuteNonQuery()

                Rec.RegID = r(1)
                Rec.FirstName = Me.txtfirstname.Text
                Rec.LastName = Me.txtlastname.Text
                Rec.Othernames = Me.txtmiddlename.Text
                Rec.AcademicSession = r(2) 'Me.DropDownList1.SelectedValue
                Rec.Term = r(3) 'Me.txtterm.Text
                Rec.Subject = r(4) 'Me.ddlsubjects.SelectedValue
                Rec.CA1 = r(6).ToString 'Me.txtca1.Text
                Rec.CA2 = r(7).ToString 'Me.txtca2.Text
                Rec.CA3 = r(9).ToString
                Rec.CA4 = Me.txtca4.Text
                Rec.CATotal = Me.txttotalCa.Text
                Rec.ExamScore = r(8).ToString 'me.txtscore.text
                Rec.TotalExamScore = Me.txttotalexam.Text
                Rec.Comment = r(5) 'Me.txtcomments.Text
                Rec.TransGUID = System.Guid.NewGuid.ToString
                Rec.Status = "PENDING"
                Rec.SubmittedBy = Session("uname").ToString()
                Rec.SubmittedOn = Date.Now.ToString("dd MMMM yyyy")
                Rec.ModifiedBy = Session("uname").ToString()
                Rec.ModifiedOn = Date.Now.ToString("dd MMMM yyyy")
                Rec.Tag = "AUTO_GRADE_PROCESS"

                'Comitt the postings into Database here

                Dim Response As ResponseInfo = (New Cls_GradeCompute).Insert(Rec)
                If Response.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Grade_computation.aspx';", True)

                Else
                    Trans.Rollback()
                    Me.Msgbox1.ShowError(Response.ErrorMessage & "" & Response.ExtraMessage)
                    Return
                End If
            Next

        End If

            Trans.Commit()
            Return
    End Sub
    Protected Sub btncancel_ServerClick1(sender As Object, e As EventArgs) Handles btncancel.ServerClick

    End Sub
End Class
