﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="Classes_edit.aspx.vb" Inherits="Admin_StudentReg_Classes_edit" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Staffs.ascx" TagName="staffs" TagPrefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>

      <h4 id="lblclass" runat="server" class="title"></h4>
      <div data-role="panel" class="panel">
    
   
        <div style="z-index:999;" class="content">
              <h5 class="hint-text">Class Details</h5>
            <hr />
            <div>
                <table width="100%">
                <tr height="50px">
                     <td>
                         <span class="text-secondary text-bold">Class ID: </span>
                    </td>
                    <td>
                      <span>  <asp:TextBox ID="txtclassid" Enabled="false" Text="AUTO" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                    <td> <span class="text-secondary text-bold">Class Name: </span></td>
                    <td>
                       <span>  <asp:TextBox ID="txtclassname" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                    <td>
                         <span class="text-secondary text-bold">H.O.D: </span>
                    </td>
                    <td>
                       <uc2:staffs ID="staffs" runat="server"></uc2:staffs>
                    </td>
                </tr>
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                             <span>  
                                 <asp:DropDownList ID="ddlcategory" BorderWidth="1" BorderColor="Orange" Width="200px" CssClass="text-secondary" runat="server">
                                     <asp:ListItem>PRIMARY</asp:ListItem>
                                      <asp:ListItem>JUNIOR SECONDARY</asp:ListItem>
                                      <asp:ListItem>SENIOR SECONDARY</asp:ListItem>
                                 </asp:DropDownList>
                                 </span>
                        </td>
                    </tr>
          </table>
               
            </div>
        </div>
         <div class="align-center">
                   <span>  &nbsp;<asp:Button ID="cmdsave" OnClick="cmdsave_Click" runat="server" CssClass="button bg-green fg-white small-button" Text="Submit" /></span>

                                 <span>  <button runat="server" id="btncancel" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>

                </div>
</div>
</asp:Content>

