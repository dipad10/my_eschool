﻿Imports System
Imports System.Data
Imports System.Threading
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class Admin_StudentReg_Student_profile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim studentID As String = Request.QueryString("IDNo")
            If studentID <> "" Then
                Dim connect As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                connect.Open()

                Dim com As SqlCommand = connect.CreateCommand
                com.CommandType = CommandType.Text
                com.CommandText = "select * from StudentRegistration where IDNo=('" & studentID & "')"
                com.ExecuteNonQuery()
                Dim dr As SqlDataReader
                dr = com.ExecuteReader()
                If dr.Read Then
                

                    lblregno.Text = dr("IDNo")
                    lblstudentname.Text = dr("LastName") & " " & dr("FirstName")
                    txtstudentno.Text = dr("IDNo")
                    txtmiddlename.Text = dr("MiddleName").ToString()
                    txtaddress.Text = dr("Address").ToString()
                    txtnationality.Text = dr("Nationality").ToString()
                    txtfirstname.Text = dr("FirstName").ToString()
                    dateEdit.Date = Format(dr("DOB"), "dd MMM yyyy")
                    ddlgender.SelectedValue = dr("Gender").ToString()
                    ddlstate.SelectedValue = dr("State").ToString()

                    txtlastname.Text = dr("LastName").ToString()
                    txtadmissiondate.Date = Format(dr("AdmissionDate"), "dd MMM yyyy")
                    txtpob.Text = dr("POB").ToString()
                    txtlga.Text = dr("LGA").ToString()

                    If dr("Picture").ToString() <> "" Then
                        Dim image As [Byte]() = DirectCast(dr("Picture"), [Byte]())
                        Dim src As String = "data:image/jpeg;base64," + Convert.ToBase64String(image)
                        Image1.ImageUrl = src
                    Else
                        Image1.ImageUrl = "~/images/nophoto.jpg"
                    End If




                    'This is to load the parent details
                    If dr("Parentfirstname") Is DBNull.Value Then
                        txtparentfirst.Text = "TBA"
                    Else
                        txtparentfirst.Text = dr("Parentfirstname")
                    End If

                    If dr("Parentlastname") Is DBNull.Value Then
                        txtparentlast.Text = "TBA"
                    Else
                        txtparentlast.Text = dr("Parentlastname")

                    End If
                    If dr("Relation") Is DBNull.Value Then
                        ddlrelation.SelectedValue = "TBA"
                    Else
                        ddlrelation.SelectedValue = dr("Relation")
                    End If
                    If dr("ParentEducation") Is DBNull.Value Then
                        Education.Text = "TBA"
                    Else
                        Education.Text = dr("ParentEducation")
                    End If
                    If dr("ParentOccupation") Is DBNull.Value Then
                        Occupation.Text = "TBA"
                    Else
                        Occupation.Text = dr("ParentOccupation")
                    End If
                    If dr("Parentemail") Is DBNull.Value Then
                        txtemail.Text = "TBA"
                    Else
                        txtemail.Text = dr("Parentemail")
                    End If
                    If dr("Parentaddress") Is DBNull.Value Then
                        txtparentaddress.Text = "TBA"
                    Else
                        txtparentaddress.Text = dr("Parentaddress")
                    End If

                    If dr("Mobilephone") Is DBNull.Value Then
                        Mobilephone.Text = "TBA"
                    Else
                        Mobilephone.Text = dr("Mobilephone")
                    End If

                    If dr("OfficePhone") Is DBNull.Value Then
                        txtofficephone.Text = "TBA"
                    Else
                        txtofficephone.Text = dr("OfficePhone")
                    End If
                    connect.Close()


                    'bind picture by query string
                    'Dim connect7 As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                    'connect7.Open()

                    'Dim com7 As New SqlCommand("getimage", connect7)
                    'com7.CommandType = CommandType.StoredProcedure

                    'Dim paramid As SqlParameter = New SqlParameter()
                    'paramid.ParameterName = "@id"
                    'paramid.Value = studentID

                    'com7.Parameters.Add(paramid)
                    'Dim bytes As Byte() = DirectCast(com7.ExecuteScalar(), Byte())

                    'If bytes Is Nothing Or bytes Is DBNull.Value Then
                    '    lbluploadphoto.Visible = True
                    '    lbluploadphoto.Text = "No Photo..Upload here"
                    'Else
                    '    Dim strBase64 As String = Convert.ToBase64String(bytes)
                    '    Image1.ImageUrl = Convert.ToString("data:Image/png;base64,") & strBase64

                    'End If

                    'connect7.Close()


                End If
                Me.txtmiddlename.Enabled = False
                Me.txtfirstname.Enabled = False
                Me.txtlastname.Enabled = False
                Me.txtstudentno.Enabled = False



                'get courses registered by query string
                Dim getdetailsconnect As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

                getdetailsconnect.Open()
                Dim _cmd2 As SqlCommand = getdetailsconnect.CreateCommand()
                _cmd2.CommandType = CommandType.Text
                _cmd2.CommandText = "select * from CourseReg where RegID=( '" + studentID + "')"
                _cmd2.ExecuteNonQuery()
                Dim dr2 As SqlDataReader

                dr2 = _cmd2.ExecuteReader()
                If dr2.Read Then

                    Dim connectreg As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                    connectreg.Open()

                    Dim comreg As SqlCommand = connectreg.CreateCommand
                    comreg.CommandType = CommandType.Text
                    comreg.CommandText = "select * from CourseReg where RegID=( '" + studentID + "')"
                    Dim dt3 As New DataTable()
                    Dim sda3 As New SqlDataAdapter(comreg)

                    sda3.Fill(dt3)
                    GridView1.DataSource = dt3
                    GridView1.DataBind()
                    comreg.ExecuteNonQuery()
                    connectreg.Close()

                    dr2.Close()
                Else
                    lblcoursenotregistered.Visible = True

                    btnaddcourse.Visible = True

                End If

                getdetailsconnect.Close()
                getdetailsconnect.Dispose()

                'getdocuments

                Dim getdocuments As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
                getdocuments.Open()

                Dim cmdgetdoc As SqlCommand = getdocuments.CreateCommand
                cmdgetdoc.CommandType = CommandType.Text
                cmdgetdoc.CommandText = "select BirthCertificate, ClearanceForm, Picture from StudentRegistration where IDNo=( '" + studentID + "')"
                Dim da As New SqlDataAdapter(cmdgetdoc)
                Dim ds As New DataSet()
                da.Fill(ds)
                gridviewdoc.DataSource = ds
                gridviewdoc.DataBind()
                getdocuments.Close()


            End If



       

        End If


     




    End Sub

    Protected Sub btnaddcourse_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/StudentReg/Course_reg.aspx?IDNo=" & txtstudentno.Text & "")
    End Sub

  

   

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/StudentReg/Students.aspx")
    End Sub


    Public Function GetPhotoAsByteArray() As Byte()
        Try
            If Request.Files.Count > 0 Then
                'on upload

                Dim SavePath As String = Server.MapPath("photos/" & Guid.NewGuid.ToString)
                Request.Files(0).SaveAs(SavePath)

                Dim ms As New IO.MemoryStream
                Dim img As Drawing.Image = Drawing.Image.FromFile(SavePath)
                img.Save(ms, Imaging.ImageFormat.Jpeg)
                img.Dispose()
                IO.File.Delete(SavePath)
                Return ms.ToArray
            Else
                Dim B(-1) As Byte
                Return B
            End If
        Catch
            Dim B(-1) As Byte
            Return B
        End Try
    End Function

    Protected Sub gridviewdoc_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim studentID As String = Request.QueryString("IDNo")
        Dim updatecon2 As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
        updatecon2.Open()

        Dim updatecom2 As SqlCommand = updatecon2.CreateCommand
        updatecom2.CommandType = CommandType.Text
        updatecom2.CommandText = "select IDNo, BirthCertificate, ClearanceForm, Picture from  StudentRegistration where IDNo=@id"
        updatecom2.Parameters.AddWithValue("@id", studentID)
        Dim dr As SqlDataReader = updatecom2.ExecuteReader

        If dr.Read Then
            Response.Clear()
            Response.Buffer = True
            'Response.ContentType = dr("type").ToString()
            Response.AddHeader("content-disposition", "attachment;filename=" + dr("BirthCertificate").ToString())
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.BinaryWrite(DirectCast(dr("BirthCertificate"), Byte()))
            Response.End()
        End If
    End Sub

    Protected Sub btnsavepersonal_ServerClick(sender As Object, e As EventArgs)
        'if the profile picture is upated
        Dim studentID As String = Request.QueryString("IDNo")
        Dim P As New Cls_StudentRegistration
        If File1.PostedFile.FileName <> "" Then
            Dim img2 As New Bitmap(File1.PostedFile.InputStream, False)
            Dim height As Integer = img2.Height
            ' get the height of image in pixel.
            Dim width As Integer = img2.Width

            If height <> width Then
                Msgbox1.ShowError("Image Resolution must be equal e.g 100x100px")

                Exit Sub
            End If

            Dim rec As Enrollment.StudentRegistration = P.SelectThisIDNo(studentID)
            rec.FirstName = txtfirstname.Text
            rec.MiddleName = txtmiddlename.Text
            rec.LastName = txtlastname.Text
            rec.Address = txtaddress.Text
            rec.Nationality = txtnationality.Text
            rec.DOB = dateEdit.Date
            rec.Gender = ddlgender.SelectedValue
            rec.LGA = txtlga.Text
            rec.AdmissionDate = txtadmissiondate.Date
            rec.State = ddlstate.SelectedValue
            rec.POB = txtpob.Text
            rec.Picture = GetPhotoAsByteArray()

            Dim res As ResponseInfo = P.Update(rec)

            If res.ErrorCode = 0 Then



                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Student_profile.aspx?IDNo=" & studentID & "';", True)


            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If


        Else
            'if the profile picture is not updated it shouldnt updae getphotobytearrayfunction
             Dim rec As Enrollment.StudentRegistration = P.SelectThisIDNo(studentID)
            rec.FirstName = txtfirstname.Text
            rec.MiddleName = txtmiddlename.Text
            rec.LastName = txtlastname.Text
            rec.Address = txtaddress.Text
            rec.Nationality = txtnationality.Text
            rec.DOB = dateEdit.Date
            rec.Gender = ddlgender.SelectedValue
            rec.LGA = txtlga.Text
            rec.AdmissionDate = txtadmissiondate.Date
            rec.State = ddlstate.SelectedValue
            rec.POB = txtpob.Text
          
            Dim res As ResponseInfo = P.Update(rec)

            If res.ErrorCode = 0 Then
                Msgbox1.Showsuccess("Data saved successfully for student '" & studentID & "' ")
                TabContainer1.ActiveTabIndex = 1


            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If

        End If

    End Sub

    Protected Sub btnsaveparent_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btnsavedocument_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btnsavecourse_ServerClick(sender As Object, e As EventArgs)

    End Sub
End Class
