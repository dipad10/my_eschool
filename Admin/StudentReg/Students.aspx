﻿<%@ Page Language="VB" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Students.aspx.vb" MasterPageFile="~/Admin/StudentReg/Student.master" Inherits="webroot_web_modules_StudentReg_StudentInfo" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>



   <asp:Content ID="content1" ContentPlaceHolderID="body" runat="server"> 
     
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
       
       <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" ConfirmText="Are you sure you want to delete this student(s)" TargetControlID="btndelete" runat="server" />
      <h4 class="title">Manage Students</h4>
      

      
  
       <div class="panel">
           <div class="content bg-white">
               

                <dx:ASPxGridView ID="ASPxGridView1" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-darkBlue" SettingsPager-PageSize="25" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource1" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" >
                   <Settings ShowFilterRow="True" ShowFilterBar="Auto" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                       
                       <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>
                    <dx:GridViewDataHyperLinkColumn FieldName="IDNo" CellStyle-CssClass="text-small fg-darkBlue text-bold" PropertiesHyperLinkEdit-DisplayFormatString="IDNo" PropertiesHyperLinkEdit-NavigateUrlFormatString="Students_edit.aspx?IDNo={0}" VisibleIndex="1" CellStyle-ForeColor="#006600">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="Students_edit.aspx?IDNo={0}" TextFormatString="IDNo">
                        </PropertiesHyperLinkEdit>

<CellStyle CssClass="text-small fg-darkBlue text-bold" ForeColor="#006600"></CellStyle>
                       </dx:GridViewDataHyperLinkColumn>
                      
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="FirstName" VisibleIndex="2">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                  
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="LastName" VisibleIndex="4">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                      
                       <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small fg-grayLight" FieldName="AdmissionDate" VisibleIndex="6">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataDateColumn>
                   
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="Gender" VisibleIndex="8">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                    
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="Enrollment_type" VisibleIndex="41">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                    
                   </Columns>
                   <Settings ShowFooter="True" />

                  <Styles>
            <AlternatingRow Enabled="true" />
                     
        </Styles>
               </dx:ASPxGridView>

               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Enrollment_Net10ConnectionString %>" SelectCommand="select * from studentregistration order by admissiondate desc"></asp:SqlDataSource>



               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                           <asp:Button ID="btnnew" Visible="true" Height="29px" OnClick="btnnew_Click" CssClass="button text-small bg-lightGreen fg-white mini-button" runat="server" Text="New Student" Font-Size="X-Small" Font-Bold="True" />             
                      </td>
                         
                           <td>

                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click" CssClass="button bg-red text-small fg-white mini-button" runat="server"  Font-Size="X-Small" Font-Bold="True" Text="Delete Student" />
                           </td>
                           
                           <td>
                           <asp:Button ID="cmdprint" Visible="true" Height="29px" OnClick="cmdprint_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Print Profile" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>

       <%--'highlightrow when gridview is checkeed--%>
   
 
       </asp:Content>