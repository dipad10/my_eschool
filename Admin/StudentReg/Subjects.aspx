﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="Subjects.aspx.vb" Inherits="Admin_StudentReg_Subjects" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
      <h4 class="title">Subjects</h4>
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
     <div class="panel">
           <div class="content">
               
<asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                         <asp:HyperLinkField HeaderText="SN" ItemStyle-CssClass="fg-red" DataNavigateUrlFields="SN" DataTextField="SN"
                    DataNavigateUrlFormatString="subjects_edit.aspx?sn={0}"/>

                     
                       <asp:BoundField DataField="Subjectname" HeaderText="Subjectname" SortExpression="Subjectname" />
                      
                       <asp:BoundField DataField="Teacher" HeaderText="H.O.D" SortExpression="Teacher" />
                      
                   
            
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-secondary" Height="30px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>

              <div class="align-center">
                   <table>
                       <tr>
                      <td>
                          <button runat="server" id="btnnew" onserverclick="btnnew_ServerClick" class="button bg-lightGreen fg-white mini-button"><span class="mif-plus"></span> New Subject</button>
                      </td>
                         
                           <td>
                               <asp:Button ID="btncancel" Visible="true" Height="25px" OnClick="btncancel_Click" CssClass="button bg-red text-small fg-white mini-button" runat="server" Text="Cancel" />
                           </td>
                            <td>
                               <asp:Button ID="btndelete" Visible="true" Height="25px" OnClick="btndelete_Click" CssClass="button bg-red text-small fg-white mini-button" runat="server" Text="Delete" />
                           </td>
                       </tr>
                       
                   </table>
               </div>
               </div>
         </div>
</asp:Content>

