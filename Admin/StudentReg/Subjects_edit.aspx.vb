﻿
Partial Class Admin_StudentReg_Subjects_edit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sn As String = Request.QueryString("sn")
            If sn <> "" Then

                Dim rec As Enrollment.Subject = (New Cls_subjects).SelectThissubject(sn)
                txtsubjectid.Text = rec.SN
                txtsubjectname.Text = rec.Subjectname
                staffs.SelectedText = rec.Teacher

                lblclass.InnerText = "Subject " & sn & " Edit "
            Else
                lblclass.InnerText = "New subject"
            End If
        End If
    End Sub

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        Dim A As New Cls_subjects
        Dim subjectid As String = Request.QueryString("sn")
        If subjectid <> "" Then
            'update record
            Dim rec As Enrollment.Subject = A.SelectThissubject(subjectid)
            rec.Subjectname = txtsubjectname.Text
            rec.Teacher = staffs.SelectedText

            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='subjects.aspx';", True)


            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If

        Else
            'create fresh record
            Dim rec As Enrollment.Subject = A.SelectThissubjectname(txtsubjectname.Text)
            If rec.Subjectname Is Nothing Then  'if d classname does not exist before in the system
                rec.Subjectname = txtsubjectname.Text
                rec.Teacher = staffs.SelectedText

                Dim res As ResponseInfo = A.Insert(rec)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='subjects.aspx';", True)


                Else
                    Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                    'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
                End If
            Else

                'if it exists
                Me.Msgbox1.ShowHelp("This subject already exists in the system! Pls use a different subject name")



            End If




        End If
    End Sub

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub
End Class
