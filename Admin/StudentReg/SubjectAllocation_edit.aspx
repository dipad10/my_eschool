﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/StudentReg/Student.master" AutoEventWireup="false" CodeFile="SubjectAllocation_edit.aspx.vb" Inherits="Admin_StudentReg_SubjectAllocation_edit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <h4 class="title">Subjects Allocation</h4>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>


    <div class="panel">
           <div class="content">
               <h5 id="lblallocate" runat="server" class="hint-text"></h5>
               <hr />
               


               
               <asp:Panel ID="Panelgridview" runat="server">
<asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                         <asp:HyperLinkField HeaderText="SN" ItemStyle-CssClass="fg-red" DataNavigateUrlFields="SN" DataTextField="SN"
                    DataNavigateUrlFormatString="subjectallocations.aspx"/>

                                            <asp:BoundField DataField="class" HeaderText="class" SortExpression="class" />

                       <asp:BoundField DataField="subjects" HeaderText="subjects" SortExpression="subjects" />
                      
                   
            
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-secondary" Height="30px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
       <span> <button runat="server" id="btnremove" onserverclick="btnremove_ServerClick" class="mini-button button fg-white bg-darkRed"><span class="mif-arrow-right text-bold"></span>Remove Course(s)</button></span>

               </asp:Panel>
               <br />

               <asp:Panel ID="panelallocate" runat="server">
                   <div class="grid">
                   <div class="row cells2">
                       <div class="cell">
                      <span class="text-small bg-lightGreen text-bold">Select every subject that the class offers and allocate</span>
                           <br />
                            <asp:ListBox ID="ListBox1" SelectionMode="Multiple" Height="200"
                                Width="300" runat="server">
                                
                            </asp:ListBox>
                       </div>
                       
                       

                        <div class="cell">
                            <span class="text-small bg-lightGreen text-bold">Subjects already allocated to specified class</span>
                           <br />
                              
                            <asp:ListBox ID="ListBox2" Height="200"
                                Width="300" runat="server">
                                
                            </asp:ListBox>
                       </div>

                   </div>
                   <br />
                              <span>

                                  <asp:Button ID="btnadd" OnClick="btnadd_Click" runat="server" Text="Add selected items" />
                              </span>
                                 <span> <button runat="server" id="btnsavecourse" onserverclick="btnsavecourse_ServerClick" class="mini-button button fg-white bg-darkBlue"><span class="mif-arrow-right text-bold"></span> Save Course(s)</button></span>


               </div>

               </asp:Panel>
               
               </div>
         </div>
</asp:Content>

