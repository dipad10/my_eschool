﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports DevExpress.Web.ASPxEdit
Imports DevExpress.Web
Imports DevExpress.XtraReports.Web

Partial Class Admin_Report_viewer2
    Inherits System.Web.UI.Page

    Protected Sub ASPxDocumentViewer1_CustomizeParameterEditors(sender As Object, e As CustomizeParameterEditorsEventArgs)
        If e.Parameter.Name = "studentsID" Then
            Dim customParameterEditor As New ASPxComboBox() With {.ID = "studentsid"}
            e.Editor = customParameterEditor
            AddHandler customParameterEditor.Init, AddressOf customParameterEditor_Init
        End If
    End Sub


    Private Sub customParameterEditor_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim customParameterEditor As ASPxComboBox = TryCast(sender, ASPxComboBox)
      

        
        'customParameterEditor.ValueSeparator = "|"c
        Dim Recs As List(Of Enrollment.StudentRegistration) = (New Cls_StudentRegistration).SelectAll
        customParameterEditor.DataSource = Recs
        customParameterEditor.Columns.Add("IDNo").Width = 120
        customParameterEditor.Columns.Add("Firstname").Width = 170
        customParameterEditor.Columns.Add("Lastname").Width = 200
        customParameterEditor.ValueField = "IDNo"
        customParameterEditor.TextField = "Firstname"
        customParameterEditor.TextField = "Lastname"
        customParameterEditor.TextFormatString = "{0}"
        customParameterEditor.IncrementalFilteringMode = IncrementalFilteringMode.Contains




        customParameterEditor.DataBindItems()
    End Sub
End Class
