﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Report/Reportmaster.master" AutoEventWireup="false" CodeFile="params.aspx.vb" Inherits="Admin_Report_params" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>

    <h4 class="title text-bold">
        <asp:Label ID="lblheader" runat="server" Text=""></asp:Label></h4>
    <div class="panel">
        <div class="content bg-white">
           
            <table runat="server" id="tbl" width="100%">
                <tbody>
                    <tr>
                        <td>
                            <asp:CheckBox ID="CheckBox1" OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" BackColor="#C6C6FF" Text="Export Options" CssClass="text-small text-bold" TextAlign="Right" runat="server" />
                        </td>
                        <td>
                            <asp:DropDownList ID="cdbExceloption" Width="200px" runat="server"></asp:DropDownList>
                        </td>
                        <td>
                            <div style="width:400px; float:right;" class="content align-right bg-grayLighter">
                                <p class="text-bold hint-text align-left"><span class="mif-notification"></span> Report Information</p>
                                <div >
                                        <p class="text-small align-center align-justify text-bold">Please select your parameters for your report</p>
                                <p class="text-small align-center align-justify text-bold">Need any assistance pls contact your administrator</p>
                                </div>
                            
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="align-left">
                <span class="">
              <asp:Button ID="cmdprint" Visible="true" Height="29px" OnClick="cmdprint_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Print Report" Font-Size="X-Small" Font-Bold="True" />

                  
                </span>

                

       <span class="">
           <asp:Button ID="CmdPreview" Visible="true" Height="29px" OnClick="CmdPreview_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Preview Report" Font-Size="X-Small" Font-Bold="True" />

                </span>
                  <span class="">
              <asp:Button ID="Button2" Visible="true" Height="29px" OnClick="Button2_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Export Report" Font-Size="X-Small" Font-Bold="True" />

                </span>
            </div>
            

        </div>
    </div>
</asp:Content>

