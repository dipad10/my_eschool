﻿
Option Compare Text
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports DevExpress.Web

Partial Class Admin_Report_params
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim oReport As New ReportDocument

        If Not Page.IsPostBack Then
            Dim rptFile As String = Server.MapPath(Request.QueryString("rpt"))
            If Not IO.File.Exists(rptFile) Then Exit Sub

            oReport.Load(rptFile, CrystalDecisions.[Shared].OpenReportMethod.OpenReportByTempCopy)
            rptFile = Mid(rptFile, rptFile.LastIndexOf("\") + 2) 'remove c:\...\...\
            rptFile = Replace(rptFile, ".rpt", "") 'remove extension
            rptFile = Replace(rptFile, "_", "") 'convert underscores
            lblheader.Text = rptFile
            Session("oReport") = oReport
        Else
            oReport = Session("oReport")
        End If

        Dim paramCount As Integer = 0

        'fetch all the params to create a form
        For Each V As ParameterFieldDefinition In _
                    oReport.DataDefinition.ParameterFields

            If V.ParameterType = ParameterType.StoreProcedureParameter AndAlso V.ReportName = "" Then

                paramCount += 1 'increase the param count

                Dim r1 As New HtmlTableRow : Me.tbl.Rows.Add(r1) 'add immediately

                Dim c1 As New HtmlTableCell : r1.Cells.Add(c1) 'add immediately
                c1.Attributes.Add("class", "editColA")

                If V.PromptText <> "" Then
                    c1.InnerHtml = V.PromptText
                ElseIf V.ParameterFieldName <> "" Then
                    c1.InnerHtml = V.ParameterFieldName
                Else
                    c1.InnerHtml = V.Name
                End If

                Dim defValue As String = Request.QueryString(V.Name)

                Dim c2 As New HtmlTableCell : r1.Cells.Add(c2) 'add immediately
                c2.Attributes.Add("class", "editColB") : c2.VAlign = "top"

                Select Case V.ValueType
                    Case FieldValueType.DateField, _
                        FieldValueType.DateTimeField, FieldValueType.TimeField

                        Dim objText As ASPxDateEdit = makedate(c2, V.Name, "pickDate")
                        'mod_ui_helpers.MakeDatePicker(Me, objText, Now)

                    Case Else 'string, number, ole etc
                        'use the name of the parameter
                        'to determine the type of the input field to create

                        Dim objInput As New Control

                        If V.Name Like "*StudentID*" Or V.Name Like "*StudentName*" Or V.Name Like "*IDNo*" Then
                            Dim objDdl As Control = makeusercontrol(c2, V.Name, defValue, "/controls/studentsID.ascx")
                            'mod_filldropdowns.FillSessions(objDdl)

                        ElseIf V.Name Like "*AcademicSession*" Then
                            Dim objDdl As DropDownList = makeDropdown2(c2, V.Name, defValue)
                            mod_filldropdowns.FillSessions(objDdl)

                        ElseIf V.Name Like "*Receiptno*" Then
                            Dim objDdl As Control = makeusercontrol(c2, V.Name, defValue, "/controls/Receivables.ascx")

                            '    'ElseIf V.Name Like "@risk*" Or V.Name Like "@class*" Then
                            '    '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    '    mod_combo_fill.FillRisks(objDdl)

                            '    'ElseIf V.Name Like "*subrisk*" Or V.Name Like "*subclass*" Then
                            '    '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    '    mod_combo_fill.FillSubRisks(objDdl)

                            'ElseIf V.Name Like "*covertype*" Or V.Name Like "*cover*" Then
                            '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    mod_combo_fill.FillCoverType(objDdl)

                            'ElseIf V.Name Like "*Paymonth*" Or V.Name Like "*PayDate*" Or V.Name Like "*Period*" Then
                            '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    mod_combo_fill.FillPayMonthYr(objDdl)

                            'ElseIf V.Name Like "*BirthdateMonth*" Or V.Name Like "*Birthdate*" Then
                            '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    mod_combo_fill.FillMonthDay(objDdl)

                            'ElseIf V.Name Like "*SheetID*" Or V.Name Like "*SheetID*" Then
                            '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    mod_combo_fill.FillPayGroup(objDdl)


                            'ElseIf V.Name Like "*BankGroup*" Or V.Name Like "*BankGroup*" Then
                            '    Dim objDdl As DropDownList = makeDropdown(c2, V.Name, defValue)
                            '    mod_combo_fill.FillBankPayGrp(objDdl)

                            'ElseIf V.Name Like "*party*" Then

                            '    Dim objCtl As Control = makeUserControl(c2, V.Name, defValue, "/webroot/web/controls/popups/party.ascx")

                            'ElseIf V.Name Like "*Account*" Then
                            '    Dim objCtl As Control = makeUserControl(c2, V.Name, defValue, "/webroot/web/controls/popups/AcctCode.ascx")

                            'ElseIf V.Name Like "*EmployeeID*" Then
                            '    Dim objCtl As Control = makeUserControl(c2, V.Name, defValue, "/webroot/web/controls/popups/EmployeeID.ascx")

                        Else
                            Dim objText As TextBox = maketextbox(c2, V.Name, defValue)
                        End If
                End Select

                'add blank row
                Dim r2 As New HtmlTableRow : Me.tbl.Rows.Add(r2)
                Dim c3 As New HtmlTableCell : r2.Cells.Add(c3)
                Dim c4 As New HtmlTableCell : r2.Cells.Add(c4)
                c3.Attributes.Add("class", "editColA")
                c4.Attributes.Add("class", "editColB")
            Else
                Dim a = V.ParameterFieldName
            End If
        Next

        Session("paramCount") = paramCount
    End Sub
    Private Function makedate(ByVal Parent As Control, ByVal paramName As String, ByVal defValue As String, Optional ByVal CssClass As String = "text-secondary") As ASPxDateEdit
        Dim c As New ASPxDateEdit
        Parent.Controls.Add(c)
        c.CssClass = CssClass
        c.Date = defValue
        c.ID = "cr_" & paramName : Return c
    End Function


    Private Function makeDropdown(ByVal Parent As Control, ByVal paramName As String, ByVal defValue As String, Optional ByVal CssClass As String = "text-secondary") As ASPxComboBox
        Dim c As New ASPxComboBox
        Parent.Controls.Add(c)
        c.CssClass = CssClass
        c.Text = defValue
        c.ID = "cr_" & paramName : Return c
    End Function
    Private Function makeDropdown2(ByVal Parent As Control, ByVal paramName As String, ByVal defValue As String, Optional ByVal CssClass As String = "text-secondary") As DropDownList
        Dim c As New DropDownList
        Parent.Controls.Add(c)
        c.CssClass = CssClass
        c.Text = defValue
        c.ID = "cr_" & paramName : Return c
    End Function

    Private Function maketextbox(ByVal Parent As Control, ByVal paramName As String, ByVal defValue As String, Optional ByVal CssClass As String = "text-secondary") As TextBox
        Dim c As New TextBox
        Parent.Controls.Add(c)
        c.CssClass = CssClass
        c.Text = defValue
        c.ID = "cr_" & paramName : Return c
    End Function

    Private Function makeusercontrol(ByVal Parent As Control, ByVal paramName As String, ByVal defValue As String, ByVal ascxpath As String) As Control
        Dim c As Object = LoadControl(ascxpath)
        Parent.Controls.Add(c)
        c.selectedvalue = defValue

        c.ID = "cr_" & paramName : Return c
    End Function

   
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.cdbExceloption.Visible = False
            FillExcelOption(Me.cdbExceloption)
        End If

        Dim rptFile As String = Server.MapPath(Request.QueryString("rpt"))
        If IO.File.Exists(rptFile) Then
            If Page.IsPostBack Then
                ' 2, 3, do nothing
            Else
                Dim paramCount As Integer = Session("paramCount")
                If paramCount > 0 And paramCount > (Request.QueryString.Count - 1) Then
                    'show param page first
                Else
                    Dim oReport As ReportDocument = Session("oReport")
                    cmdprint_Click(Nothing, Nothing)
                    'show report directly
                    'Select Case settings.COMPANY_ABREV
                    '    Case "CHI"
                    '        cmdSubmit_Click(Nothing, Nothing)
                    '        'cmdPrint_Click(Nothing, Nothing)
                    '    Case Else
                    '        cmdSubmit_Click(Nothing, Nothing)
                    'End Select

                End If
            End If
        End If
    End Sub

    Public Sub FillExcelOption(ByVal cb As DropDownList)
        cb.Items.Clear()
        cb.Items.Add(New ListItem("MS Word", "WordForWindows"))
        cb.Items.Add(New ListItem("MS Excel 97-2000", "Excel"))
        cb.Items.Add(New ListItem("MS Excel 97-2000 (Data Only)", "ExcelRecord"))
        cb.Items.Add(New ListItem("Rich Text Format", "RichText"))

        cb.Items.Insert(0, New ListItem("- select -", "NULL"))
    End Sub

    Protected Sub CmdPreview_Click(sender As Object, e As EventArgs) Handles CmdPreview.Click
        Dim oReport As ReportDocument = Session("oReport")

        Session("dbParams") = GetReportParams(oReport)
        Session("lbHeader") = Me.lblheader.Text
        Response.Redirect("viewer.aspx")
    End Sub

    Protected Sub cmdprint_Click(sender As Object, e As EventArgs) Handles cmdprint.Click
        Dim oReport As ReportDocument = Session("oReport")
        Dim dbParams() As DictionaryEntry = GetReportParams(oReport)

        'this code block fetches data from the stored_proceedure and 
        'pushes it into the report. If it is an old report and the sp
        'is not found in the database, and error occurs.

        setReportParams(oReport, dbParams, Me.lblheader.Text)
        Response.Clear()
        oReport.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, _
                                     Page.Response, False, Nothing)
        'Response.Flush()
    End Sub










    Private Function GetReportParams(ByVal oReport As ReportDocument) As DictionaryEntry()
        Dim dbParams(-1) As DictionaryEntry

        'fetch param values and assign to params
        For Each V As ParameterFieldDefinition In _
            oReport.DataDefinition.ParameterFields

            If V.ParameterType = ParameterType.StoreProcedureParameter AndAlso V.ReportName = "" Then

                ReDim Preserve dbParams(dbParams.Length)
                dbParams(dbParams.Length - 1) = New DictionaryEntry(V.Name, DBNull.Value)

                Select Case V.ValueType
                    Case FieldValueType.DateField, _
                        FieldValueType.DateTimeField, FieldValueType.TimeField

                        Dim objText As TextBox = Me.tbl.FindControl("cr_" & V.Name)
                        dbParams(dbParams.Length - 1).Value = CDate(objText.Text)

                    Case Else 'string, number, ole etc
                        Dim objText As Object = Me.tbl.FindControl("cr_" & V.Name)

                        'get the type of the object
                        'then fetch its current value
                        Select Case TypeName(objText)
                            Case "Textbox"
                                dbParams(dbParams.Length - 1).Value = objText.Text
                            Case "DropDownList"
                                dbParams(dbParams.Length - 1).Value = objText.SelectedValue
                            Case Else
                                'one of our custom popups
                                dbParams(dbParams.Length - 1).Value = objText.SelectedValue
                        End Select
                End Select

            End If
        Next

        Return dbParams
    End Function

    Public Function setReportParams(ByVal oReport As ReportDocument, ByVal dbParams() As DictionaryEntry, ByVal ReportTitle As String) As String
        Dim oTable As CrystalDecisions.CrystalReports.Engine.Table
        If oReport.Database.Tables.Count = 1 Then

            oTable = oReport.Database.Tables(0)
            Dim spName As String = oTable.LogOnInfo.TableName
            Dim Conn As New SqlConnection(m_strconnString)
            Dim cmd As New SqlCommand(spName, Conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 9999999

            For Each p In dbParams
                cmd.Parameters.Add(New SqlClient.SqlParameter(parameterName:=p.Key, value:=p.Value))
            Next

            Dim rdr As New SqlDataAdapter(cmd)
            Dim tbl As New DataTable(oTable.Name.ToString)
            Try
                rdr.Fill(tbl)
                oTable.SetDataSource(tbl)
            Catch ex As Exception
                Return ex.Message
            End Try
        End If

        'mod_report.setHeaderFooterInfo(oReport, ReportTitle)
        Return ""


    End Function

    Private Function pushReportData(ByVal oReport As ReportDocument, ByVal rptDataSet As DataSet) As Boolean
        '   Called when a report is generated
        '   Calls SetData for the main report object
        '   Also calls SetData for any subreport objects

        SetData(oReport, rptDataSet)

        For Each oSubReport As ReportDocument In oReport.Subreports
            SetData(oSubReport, rptDataSet)
        Next
    End Function

    Private Function SetData(ByVal oReport As ReportDocument, ByVal rptDataSet As DataSet) As Boolean
        ' receives a DataSet and a report object (could be the 
        ' main object, could be a subreport object)

        ' loops through the report object's tables collection, 
        ' matches up the table name with the corresponding table
        ' name in the dataset, and sets the datasource accordingly

        ' This function eliminates the need for a developer to 
        ' know the specific table order in the report

        For Each oTable In oReport.Database.Tables
            oTable.SetDataSource(rptDataSet.Tables(oTable.Name.ToString()))
        Next
    End Function

    Protected Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)
        If Me.CheckBox1.Checked = True Then
            Me.cdbExceloption.Visible = True
        Else
            Me.cdbExceloption.Visible = False

        End If
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs)
        Dim oReport As ReportDocument = Session("oReport")
        Dim dbParams() As DictionaryEntry = GetReportParams(oReport)

        'this code block fetches data from the stored_proceedure and 
        'pushes it into the report. If it is an old report and the sp
        'is not found in the database, and error occurs.

        Dim rptFile As String = Me.lblheader.Text
        setReportParams(oReport, dbParams, Me.lblheader.Text)
        Response.Clear()
        Dim ExportType As CrystalDecisions.Shared.ExportFormatType
        Select Case Me.cdbExceloption.SelectedItem.Value
            Case "Excel"
                ExportType = ExportFormatType.Excel
            Case "WordForWindows"
                ExportType = ExportFormatType.WordForWindows
            Case "RichText"
                ExportType = ExportFormatType.RichText
            Case "ExcelRecord"
                ExportType = ExportFormatType.ExcelRecord
            Case Else
                ExportType = ExportFormatType.ExcelRecord
        End Select
        oReport.ExportToHttpResponse(ExportType, Page.Response, True, rptFile)
        Response.Flush()
    End Sub
End Class
