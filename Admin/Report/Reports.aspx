﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Report/Reportmaster.master" AutoEventWireup="false" CodeFile="Reports.aspx.vb" Inherits="Admin_Report_Reports" %>
<%@ Register Assembly="DevExpress.XtraReports.v17.1.Web, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <h4 class="title">Reports</h4>
    <hr />
    
     <table>            <tr>                <td>                    <dx:ASPxComboBox ID="cbReportType" Width="300px" Theme="PlasticBlue" runat="server" ValueType="System.String">                        <Items>                            <dx:ListEditItem Text="(Empty)" Value="" />                            <dx:ListEditItem Text="All Students Report Sheet" Value="All_Students_Report_Sheet" />                            <dx:ListEditItem Text="Student list by class Report" Value="Get_student_list_by_class" />                           <dx:ListEditItem Text="Student(s) Profile Report" Value="Student_profile" />                            <dx:ListEditItem Text="All Receivables by period" Value="All_receivables_by_period" />                        </Items>                    </dx:ASPxComboBox>                </td>                <td>                   <asp:Button ID="btPreview" Visible="true" Height="29px" OnClick="btPreview_Click" CssClass="button text-small bg-darkBlue fg-white mini-button" runat="server" Text="Preview Report" Font-Size="X-Small" Font-Bold="True" />
                                  </td>            </tr>        </table><dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" OnCustomizeParameterEditors="ASPxDocumentViewer1_CustomizeParameterEditors" Theme="PlasticBlue" runat="server"></dx:ASPxDocumentViewer>
  <%--  <asp:Panel ID="pnlstudentreports" runat="server">
        <div class="treeview" data-role="treeview">
    <ul>
       <li class="node collapsed">
    <span class="leaf text-secondary text-bold">Students Reports</span>
    <span class="node-toggle"></span>
    <ul>
        <li>
            <a href="params.aspx?rpt=~/Admin/App_Reports/All Students Report Sheet.rpt"  class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
       
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
      
    </ul>
</li>
    </ul>

           
</div>
    </asp:Panel>
        
   <asp:Panel ID="pnlaccountreports" runat="server">
        <div class="treeview" data-role="treeview">
    <ul>
       <li class="node collapsed">
    <span class="leaf text-secondary text-bold">Account Reports</span>
    <span class="node-toggle"></span>
    <ul>
        <li>
            <a href="params.aspx?rpt=~/Admin/App_Reports/All Student Report Sheet.rpt"  class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
       
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
      
    </ul>
</li>
    </ul>

           
</div>
    </asp:Panel>

    <asp:Panel ID="pnlfixedassets" runat="server">
        <div class="treeview" data-role="treeview">
    <ul>
       <li class="node collapsed">
    <span class="leaf text-secondary text-bold">Fixed Assets Reports</span>
    <span class="node-toggle"></span>
    <ul>
        <li>
            <a href="params.aspx?rpt=~/Admin/App_Reports/All Student Report Sheet.rpt"  class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
       
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
      
    </ul>
</li>
    </ul>

           
</div>
    </asp:Panel>

    <asp:Panel ID="pnltransport" runat="server">
        <div class="treeview" data-role="treeview">
    <ul>
       <li class="node collapsed">
    <span class="leaf text-secondary text-bold">Transport Reports</span>
    <span class="node-toggle"></span>
    <ul>
        <li>
            <a href="params.aspx?rpt=~/Admin/App_Reports/All Student Report Sheet.rpt"  class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
       
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
         <li>
            <a href="..." class="leaf text-small link"><span class="icon mif-file-download"></span> All Students Report Sheet</a>
        </li>
      
    </ul>
</li>
    </ul>

           
</div>
    </asp:Panel>--%>
</asp:Content>

