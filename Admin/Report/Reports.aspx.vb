﻿Option Compare Text
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.XtraReports.UI
Imports DevExpress.Web
Imports System.IO
Imports DevExpress.XtraReports.Web


Partial Class Admin_Report_Reports
    Inherits System.Web.UI.Page
    Private Function GetReportByName(ByVal reportName As String) As XtraReport
        Dim report As XtraReport = Nothing
        Select Case reportName
            Case "All_Students_Report_Sheet"
                report = New All_Students_Report_Sheet()
            Case "Get_student_list_by_class"
                report = New Get_student_list_by_class()
            Case "Student_profile"
                report = New Student_profile()
            Case "All_receivables_by_period"
                report = New All_receivables_by_period()
                'Case "Orders"
                '    'report = New OrdersReport()
        End Select
        Return report
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim reportName As String = CStr(Session("ReportName"))
        If (Not String.IsNullOrEmpty(reportName)) Then
            ASPxDocumentViewer1.Report = GetReportByName(reportName)
        End If



        'Dim rec As Enrollment.SecUser = (New Cls_secusers).SelectThisID(Session("uname").ToString())
        'Select Case rec.permission
        '    Case "Accountant"
        '        pnlaccountreports.Visible = True
        '        pnlfixedassets.Visible = True
        '        pnlstudentreports.Visible = False
        '        pnltransport.Visible = False

        '    Case "Admin"
        '        pnlaccountreports.Visible = True
        '        pnlfixedassets.Visible = True
        '        pnlstudentreports.Visible = True
        '        pnltransport.Visible = True

        '    Case "Registrar"
        '        pnlaccountreports.Visible = False
        '        pnlfixedassets.Visible = False
        '        pnlstudentreports.Visible = True
        '        pnltransport.Visible = False
        'End Select
    End Sub

    Protected Sub btPreview_Click(sender As Object, e As EventArgs)
        Dim reportName As String = CStr(cbReportType.Value)

        ASPxDocumentViewer1.Report = GetReportByName(reportName)
        Session("ReportName") = reportName


    End Sub
    Sub ShowPreview()
        'Response.Redirect("ASPNETDocumentViewer.aspx")
        Response.Redirect("/Admin/Report/Viewer.aspx")
    End Sub
   

    Protected Sub ASPxDocumentViewer1_CustomizeParameterEditors(sender As Object, e As CustomizeParameterEditorsEventArgs)
        If e.Parameter.Name = "studentsID" Then
            Dim customParameterEditor As New ASPxComboBox() With {.ID = "studentsid"}
            e.Editor = customParameterEditor
            AddHandler customParameterEditor.Init, AddressOf customParameterEditor_Init
        End If
    End Sub

    Private Sub customParameterEditor_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim customParameterEditor As ASPxComboBox = TryCast(sender, ASPxComboBox)



        'customParameterEditor.ValueSeparator = "|"c
        Dim Recs As List(Of Enrollment.StudentRegistration) = (New Cls_StudentRegistration).SelectAll
        customParameterEditor.DataSource = Recs
        customParameterEditor.Columns.Add("IDNo").Width = 120
        customParameterEditor.Columns.Add("Firstname").Width = 170
        customParameterEditor.Columns.Add("Lastname").Width = 200
        customParameterEditor.ValueField = "IDNo"
        customParameterEditor.TextField = "Firstname"

        customParameterEditor.TextFormatString = "{0},{1}"
        customParameterEditor.IncrementalFilteringMode = IncrementalFilteringMode.Contains




        customParameterEditor.DataBindItems()
    End Sub
End Class
