﻿Option Compare Text
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.XtraReports.UI
Imports DevExpress.Web
Imports System.IO
Imports DevExpress.XtraReports.Web

Partial Class Admin_Report_Viewer
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
      
        If (Session("Report") IsNot Nothing) Then
            Dim ms As New MemoryStream(CType(Session("Report"), Byte()))
            Try
                ms.Seek(0, SeekOrigin.Begin)
                Dim rep As New XtraReport
                rep.PrintingSystem.LoadDocument(ms)
                ASPxDocumentViewer1.Report = rep
            Finally
                ms.Close()
            End Try

        End If
    End Sub





End Class
