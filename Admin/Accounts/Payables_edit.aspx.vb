﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web
Partial Class Admin_Accounts_Payables_edit
    Inherits System.Web.UI.Page
    Dim getdetailsconnect As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtfirstname.Enabled = False
        txtmiddlename.Enabled = False
        txtlastname.Enabled = False
        txtreceipt.Enabled = False
        txttotal.Enabled = False
        txtoutstanding.Enabled = False


        If Not Page.IsPostBack Then
            BindDt(Me.DataGrid1, "tableopt1")
            Dim idno As String = Request.QueryString("IDNo")
            Dim receiptno As String = Request.QueryString("ReceiptNo")

            If idno <> "" Then

                Dim rec As Enrollment.Payable = (New Cls_Payables).SelectThisIDandRcp(idno, receiptno)
                combo1.Text = rec.IDNo
                txtfirstname.Text = rec.FirstName
                txtlastname.Text = rec.LastName
                txtmiddlename.Text = rec.MiddleName
                ddlbustype.SelectedValue = rec.BusinessType
                ddltranstype.SelectedValue = rec.TransType
                billdate.Date = Format(rec.TDate, "dd MMM yyyy")
                txtreceipt.Text = rec.ReceiptNo
                txtnarration.Text = rec.Narration
                comboacctcode.Text = rec.AccountCode
                txtacctname.Text = rec.AccountName
                txtbankcode.Text = rec.BankAcctCode
                TxtBankName.Text = rec.BankAcctName
                txtbill.Text = FormatNumber(rec.Bill, 2)
                txtamountpaid.Text = FormatNumber(rec.Amountpaid, 2)
                txtothers.Text = FormatNumber(rec.Others, 2)
                txtoutstanding.Text = FormatNumber(rec.Outstandng, 2)
                txttotal.Text = FormatNumber(rec.Total, 2)
                TxtAmountGross.Text = FormatNumber(rec.GrossApaid, 2)
                TxtOutstandingGross.Text = FormatNumber(rec.GrossOutstanding, 2)
                combo1.Enabled = False
                lblreceivable.InnerText = "Payable '" & receiptno & "' Edit"
                Call CreateGrid(idno)

                If rec.Status = "APPROVED" Then

                    Msgbox1.ShowHelp("This Transaction has been approved therefore you won't be able to save!")
                    cmdsave.Visible = False
                End If


            Else
                lblreceivable.InnerText = "New Payable"
            End If
            Me.bindcombo()

            Me.bindaccountcodes()
            Me.bindbankcodes()

        End If

        'BindDt(Me.GridView1, "table1")
    End Sub

    Private Sub bindcombo()
        'This drop contents from the devexpress search control into the required text boxes
        Dim constr As String = ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT IDNo, FirstName, LastName, Gender FROM StudentRegistration"
                cmd.Connection = con

                Dim dt As New DataTable()
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dt)
                    combo1.DataSource = dt
                    combo1.Columns.Clear()
                    combo1.Columns.Add("IDNo").Width = 120
                    combo1.Columns.Add("FirstName").Width = 170
                    combo1.Columns.Add("LastName").Width = 200

                    combo1.ValueField = "IDNo"
                    combo1.TextField = "FirstName"
                    combo1.TextField = "LastName"

                    combo1.TextFormatString = "{0}"
                    combo1.DataBind()

                End Using
            End Using
        End Using


    End Sub

    Protected Sub combo1_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)



        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource1.SelectCommand = "SELECT [IDNo], [FirstName], [LastName] FROM (select [IDNo], [FirstName], [LastName], row_number()over(order by t.[LastName]) as [rn] from [StudentRegistration] as t where (([FirstName] + ' ' + [LastName] + ' ' + [IDNo]) LIKE @filter)) as st where st.[rn] between @startIndex and @endIndex"
        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("filter", TypeCode.String, String.Format("%{0}%", e.Filter))
        SqlDataSource1.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString())
        SqlDataSource1.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString())
        comboBox.DataSource = SqlDataSource1
        comboBox.DataBind()

    End Sub

    Protected Sub combo1_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        Dim value As Long = 0
        If e.Value Is Nothing OrElse (Not Int64.TryParse(e.Value.ToString(), value)) Then
            Return
        End If
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource1.SelectCommand = "SELECT IDNo, LastName, FirstName FROM StudentRegistration WHERE (IDNo = @ID) ORDER BY FirstName"

        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("ID", TypeCode.String, e.Value.ToString())
        comboBox.DataSource = SqlDataSource1
        comboBox.DataBind()

    End Sub

    Protected Sub combo1_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim studentcon As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
        studentcon.Open()

        Dim com8 As SqlCommand = studentcon.CreateCommand
        com8.CommandType = CommandType.Text
        com8.CommandText = "select * from StudentRegistration where IDNo=( '" + combo1.Text + "')"
        com8.ExecuteNonQuery()
        Dim dr8 As SqlDataReader

        dr8 = com8.ExecuteReader()
        If dr8.Read Then
            txtfirstname.Text = dr8("FirstName").ToString()
            txtmiddlename.Text = dr8("MiddleName").ToString()
            txtlastname.Text = dr8("LastName").ToString()
        End If

        studentcon.Close()


    End Sub


    Private Sub CreateGrid(ByVal TransID As String)

        Dim Recs As List(Of Enrollment.Receivable) = (New Cls_Receivable).SelectThisIdNoAll(TransID)
        'Dim Recs As Enrollment.Receivable = (New Cls_Receivable).SelectThis(TransID)
        Dim da As DataTable = getDt("table1")
        'A loop is needed here to list all existing records
        For Each r In Recs
            Dim dr As DataRow = da.NewRow()
            dr(0) = da.Rows.Count + 1
            dr(1) = r.ReceiptNo
            dr(2) = r.BusinessType
            dr(3) = r.TransType
            dr(4) = CDbl(r.Bill & vbNullString).ToString
            dr(5) = CDbl(r.Total & vbNullString).ToString
            dr(6) = CDbl(r.Outstandng & vbNullString).ToString
            dr(7) = r.AccountName
            dr(8) = r.BankAcctName
            da.Rows.Add(dr)

        Next

        saveDt("table1", da)
        BindDt(DataGrid1, "table1")
    End Sub
    Private Sub saveDt(ByVal tablename As String, ByVal newDt As DataTable)
        ViewState(tablename) = NewDt
    End Sub
    Private Sub BindDt(ByVal DtGrid As DataGrid, ByVal tablename As String)
        Dim Dt As DataTable = GetDt(tablename)
        Dim Dv As New DataView(Dt)
        ViewState(tablename) = Dt
        DtGrid.DataSource = Dv
        DtGrid.DataBind()

    End Sub

    Private Function GetDt(ByVal tablename As String) As DataTable
        If TypeOf ViewState(tablename) Is DataTable Then
            Return CType(ViewState(tablename), DataTable)
        Else
            Dim Dt As New DataTable
            Dt.Columns.Add(New DataColumn("SN", GetType(Int32)))
            Dt.Columns.Add(New DataColumn("Receipt No", GetType(String)))
            Dt.Columns.Add(New DataColumn("Bus.Type", GetType(String)))
            Dt.Columns.Add(New DataColumn("Trans.Type", GetType(String)))
            Dt.Columns.Add(New DataColumn("Bill", GetType(Decimal)))
            Dt.Columns.Add(New DataColumn("Amount Paid", GetType(Decimal)))
            Dt.Columns.Add(New DataColumn("Outstanding", GetType(Decimal)))
            Dt.Columns.Add(New DataColumn("Account Name", GetType(String)))
            Dt.Columns.Add(New DataColumn("Bank Name", GetType(String)))
            Return Dt
        End If
    End Function


    Protected Sub ddltranstype_SelectedIndexChanged(sender As Object, e As EventArgs)
        If ddltranstype.SelectedValue = "CHEQUE" Then
            lbltellerno.Visible = True
            txttellerno.Visible = True
        Else
            lbltellerno.Visible = False
            txttellerno.Visible = False
        End If

    End Sub

    Protected Sub btncompute_Click(sender As Object, e As EventArgs)

        Dim Test As Integer = CDbl(Me.txtbill.Text) * 1
        Me.txtbill.Text = Test
        Me.txttotal.Text = CDbl(txtamountpaid.Text) + CDbl(txtothers.Text)  'Then '+ TxtBILL.Text

        'If CDbl(Me.TxtBill.Text) * 1 Then
        Test = CDbl(Me.txtbill.Text) * 1
        Me.txtbill.Text = Test
        Me.txtoutstanding.Text = Test - CDbl(txttotal.Text)
    End Sub




    Private Sub GetReceiptDetails()

    End Sub


    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/Accounts/Payables.aspx")
    End Sub



    Protected Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdadd.Click

        Dim dt As DataTable = GetDt("table1")
        Dim dr As DataRow = dt.NewRow
        dr(0) = dt.Rows.Count + 1

        Dim Conn As New SqlConnection(m_strconnString)

        Conn.Open()
        Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'PayablesRcp' "
        Dim adap As SqlDataAdapter
        Dim ds As New DataSet
        adap = New SqlDataAdapter(SqlNo, Conn)
        adap.Fill(ds, "describe")
        Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)
        Dim Datepart As Long = (DateString.Length)
        Conn.Close()

        Dim RECEIPT As String = "CN" & "/" & Format(Datepart, "2000") & "/" & Format(NValue, "0000")
        'Me.txtreceipt.Text = RECEIPT


        Conn.Open()
        Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='PayablesRcp' "
        Dim comm1 As New SqlCommand(sqlNoUpD, Conn)
        comm1.CommandType = CommandType.Text
        comm1.ExecuteNonQuery()
        dr(1) = RECEIPT
        dr(2) = Me.ddlbustype.SelectedValue
        dr(3) = Me.ddltranstype.SelectedValue
        dr(4) = CDbl(Me.txtbill.Text)
        dr(5) = CDbl(Me.txttotal.Text)
        dr(6) = CDbl(Me.txtoutstanding.Text)
        dr(7) = Me.txtacctname.Text
        dr(8) = Me.TxtBankName.Text
        dt.Rows.Add(dr)
        BindDt(DataGrid1, "table1")

        ''Dim X As DataRow, Y As Decimal
        ''For Each X In dt.Rows
        ''    Y += X(6)
        ''Next

        Me.txtacctname.Text = dr(7)
        Me.TxtBankName.Text = dr(8)
        Me.txtbill.Text = dr(4)
        Me.txttotal.Text = dr(5)
        Me.txtoutstanding.Text = dr(6)

    End Sub
    Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
        Dim dt As DataTable = GetDt("table1")
        Dim resd As ResponseInfo = (New Cls_Payables).Deleteone(e.Item.Cells(2).Text)
        dt.Rows.RemoveAt(e.Item.ItemIndex)
        BindDt(DataGrid1, "table1")
    End Sub

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs) Handles cmdsave.Click
        Dim dt As DataTable = GetDt("table1")
        Dim r As DataRow, Q As Int16 = 1


        Dim idno As String = Request.QueryString("IDNo")
       
        Dim P As New Cls_Payables
        If idno <> "" Then

            Dim Recs As List(Of Enrollment.Payable) = (New Cls_Payables).SelectThisIdNoAll(idno)
            'Dim rec As Enrollment.Receivable = P.SelectThis(receiptno)
            'system should first delete old record so it can insert the new records with the existing records
            For Each n In Recs
                P.Deletefinal(idno)

            Next


            'create fresh new record with existing one in datagrid along side


            'Conn.Open()
            'Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'ReceivablesRcp' "
            'Dim adap As SqlDataAdapter
            'Dim ds As New DataSet
            'adap = New SqlDataAdapter(SqlNo, Conn)
            'adap.Fill(ds, "describe")
            'Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)
            'Dim Datepart As Long = (DateString.Length)
            'Conn.Close()

            'Dim RECEIPT As String = "RCP" & "/" & Format(Datepart, "2000") & "/" & Format(NValue, "0000")
            'Me.txtreceipt.Text = RECEIPT


            'Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='ReceivablesRcp' "
            'Dim comm1 As New SqlCommand(sqlNoUpD, Conn)
            'comm1.CommandType = CommandType.Text
            'comm1.ExecuteNonQuery()
            Dim Conn As New SqlConnection(m_strconnString)
            Conn.Open()
            For Each r In dt.Rows
                r(0) = Q : Q += 1

                Dim Comm As SqlCommand = Conn.CreateCommand()
                Comm.CommandType = CommandType.Text
                Comm.CommandText = "Insert Into Payables([IDNo], [LastName], [FirstName], [MiddleName], [BusinessType], [TDate], [TransType], [Teller], [ReceiptNo], [Narration], [AccountCode], [AccountName], [BankAcctCode], [BankAcctName], [Bill], [Amountpaid], [Others], [Outstandng], [Total], [GrossApaid], [GrossOutstanding], [Notetype], [TransGUID], [Deleted], [Active], [Status], [SubmittedBy], [SubmittedOn], [ModifiedBy], [ModifiedOn], [RefRcp], [Tag]) Values (@idno, @lname, @fname, @mname, @bustype, @bdate, @transtype, @teller, @receipt, @naration, @actcode, @actname, @bankcode, @bankname, @bill, @apaid, @others, @outstanding, @total, @Agross, @Ogross, @notetype, @TGuid, @deleted, @active, @status, @SubBy, @SubOn, @ModBy, @ModOn, @Ref, @Tag)"
                Comm.Parameters.AddWithValue("@idno", combo1.Text)
                Comm.Parameters.AddWithValue("@lname", txtlastname.Text)
                Comm.Parameters.AddWithValue("@fname", txtfirstname.Text)
                Comm.Parameters.AddWithValue("@mname", txtmiddlename.Text)
                Comm.Parameters.AddWithValue("@bustype", r(2))
                Comm.Parameters.AddWithValue("@bdate", CDate(billdate.Text))
                Comm.Parameters.AddWithValue("@transtype", r(3))
                Comm.Parameters.AddWithValue("@teller", txttellerno.Text)
                Comm.Parameters.AddWithValue("@receipt", r(1))
                Comm.Parameters.AddWithValue("@naration", txtnarration.Text)
                Comm.Parameters.AddWithValue("@actcode", comboacctcode.Text)
                Comm.Parameters.AddWithValue("@actname", r(7))
                Comm.Parameters.AddWithValue("@bankcode", txtbankcode.Text)
                Comm.Parameters.AddWithValue("@bankname", r(8))
                Comm.Parameters.AddWithValue("@bill", r(4))
                Comm.Parameters.AddWithValue("@apaid", txtamountpaid.Text)
                Comm.Parameters.AddWithValue("@others", txtothers.Text)
                Comm.Parameters.AddWithValue("@outstanding", r(6))
                Comm.Parameters.AddWithValue("@total", r(5))
                Comm.Parameters.AddWithValue("@Agross", TxtAmountGross.Text)
                Comm.Parameters.AddWithValue("@Ogross", TxtOutstandingGross.Text)
                Comm.Parameters.AddWithValue("@notetype", "RCP")
                Comm.Parameters.AddWithValue("@TGuid", System.Guid.NewGuid.ToString)
                Comm.Parameters.AddWithValue("@deleted", 0)
                Comm.Parameters.AddWithValue("@active", 1)
                Comm.Parameters.AddWithValue("@status", "PENDING")
                Comm.Parameters.AddWithValue("@SubBy", Session("uname").ToString())
                Comm.Parameters.AddWithValue("@SubOn", Date.Now.ToString("dd MMMM yyyy"))
                Comm.Parameters.AddWithValue("@ModBy", "")
                Comm.Parameters.AddWithValue("@ModOn", "")
                Comm.Parameters.AddWithValue("@Ref", txtreceipt.Text)
                Comm.Parameters.AddWithValue("@Tag", "AUTO_RCP")

                Comm.ExecuteNonQuery()
            Next
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='/Admin/Accounts/Payables.aspx';", True)

        Else

            'insert new record
            Dim Conn As New SqlConnection(m_strconnString)

            Conn.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'PayablesRcp' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, Conn)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)
            Dim Datepart As Long = (DateString.Length)
            Conn.Close()

            Dim RECEIPT As String = "CN" & "/" & Format(Datepart, "2000") & "/" & Format(NValue, "0000")
            Me.txtreceipt.Text = RECEIPT


            Conn.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='PayablesRcp' "
            Dim comm1 As New SqlCommand(sqlNoUpD, Conn)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()

            For Each r In dt.Rows
                r(0) = Q : Q += 1

                Dim Comm As SqlCommand = Conn.CreateCommand()
                Comm.CommandType = CommandType.Text
                Comm.CommandText = "Insert Into Payables([IDNo], [LastName], [FirstName], [MiddleName], [BusinessType], [TDate], [TransType], [Teller], [ReceiptNo], [Narration], [AccountCode], [AccountName], [BankAcctCode], [BankAcctName], [Bill], [Amountpaid], [Others], [Outstandng], [Total], [GrossApaid], [GrossOutstanding], [Notetype], [TransGUID], [Deleted], [Active], [Status], [SubmittedBy], [SubmittedOn], [ModifiedBy], [ModifiedOn], [RefRcp], [Tag]) Values (@idno, @lname, @fname, @mname, @bustype, @bdate, @transtype, @teller, @receipt, @naration, @actcode, @actname, @bankcode, @bankname, @bill, @apaid, @others, @outstanding, @total, @Agross, @Ogross, @notetype, @TGuid, @deleted, @active, @status, @SubBy, @SubOn, @ModBy, @ModOn, @Ref, @Tag)"
                Comm.Parameters.AddWithValue("@idno", combo1.Text)
                Comm.Parameters.AddWithValue("@lname", txtlastname.Text)
                Comm.Parameters.AddWithValue("@fname", txtfirstname.Text)
                Comm.Parameters.AddWithValue("@mname", txtmiddlename.Text)
                Comm.Parameters.AddWithValue("@bustype", r(2))
                Comm.Parameters.AddWithValue("@bdate", CDate(billdate.Text))
                Comm.Parameters.AddWithValue("@transtype", r(3))
                Comm.Parameters.AddWithValue("@teller", txttellerno.Text)
                Comm.Parameters.AddWithValue("@receipt", r(1))
                Comm.Parameters.AddWithValue("@naration", txtnarration.Text)
                Comm.Parameters.AddWithValue("@actcode", comboacctcode.Text)
                Comm.Parameters.AddWithValue("@actname", r(7))
                Comm.Parameters.AddWithValue("@bankcode", txtbankcode.Text)
                Comm.Parameters.AddWithValue("@bankname", r(8))
                Comm.Parameters.AddWithValue("@bill", r(4))
                Comm.Parameters.AddWithValue("@apaid", txtamountpaid.Text)
                Comm.Parameters.AddWithValue("@others", txtothers.Text)
                Comm.Parameters.AddWithValue("@outstanding", r(6))
                Comm.Parameters.AddWithValue("@total", r(5))
                Comm.Parameters.AddWithValue("@Agross", TxtAmountGross.Text)
                Comm.Parameters.AddWithValue("@Ogross", TxtOutstandingGross.Text)
                Comm.Parameters.AddWithValue("@notetype", "RCP")
                Comm.Parameters.AddWithValue("@TGuid", System.Guid.NewGuid.ToString)
                Comm.Parameters.AddWithValue("@deleted", 0)
                Comm.Parameters.AddWithValue("@active", 1)
                Comm.Parameters.AddWithValue("@status", "PENDING")
                Comm.Parameters.AddWithValue("@SubBy", Session("uname").ToString())
                Comm.Parameters.AddWithValue("@SubOn", Date.Now.ToString("dd MMMM yyyy"))
                Comm.Parameters.AddWithValue("@ModBy", "")
                Comm.Parameters.AddWithValue("@ModOn", "")
                Comm.Parameters.AddWithValue("@Ref", txtreceipt.Text)
                Comm.Parameters.AddWithValue("@Tag", "AUTO_RCP")

                Comm.ExecuteNonQuery()
            Next
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='/Admin/Accounts/Payables.aspx';", True)





        End If



    End Sub
    ''' <summary>
    ''' binding of accountcodes into devexpress comboacctcode
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub bindaccountcodes()
        'This drop contents from the devexpress search control into the required text boxes

        Using con As New SqlConnection(m_strconnString)
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT AccountID, AccountName FROM Accounts where Type='A' "
                cmd.Connection = con

                Dim dt As New DataTable()
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dt)
                    comboacctcode.DataSource = dt

                    comboacctcode.Columns.Add("AccountID").Width = 120
                    comboacctcode.Columns.Add("AccountName").Width = 170


                    comboacctcode.ValueField = "AccountID"
                    comboacctcode.TextField = "AccountName"

                    comboacctcode.TextFormatString = "{0}"
                    comboacctcode.DataBind()

                End Using
            End Using
        End Using


    End Sub

    Protected Sub comboacctcode_SelectedIndexChanged(sender As Object, e As EventArgs)
        Using con As New SqlConnection(m_strconnString)
            con.Open()

            Dim com8 As SqlCommand = con.CreateCommand
            com8.CommandType = CommandType.Text
            com8.CommandText = "select * from Accounts where AccountID=( '" + comboacctcode.Text + "')"
            com8.ExecuteNonQuery()
            Dim dr8 As SqlDataReader

            dr8 = com8.ExecuteReader()
            If dr8.Read Then
                txtacctname.Text = dr8("AccountName").ToString()
            End If

            con.Close()
        End Using

    End Sub

    Protected Sub comboacctcode_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource2.SelectCommand = "SELECT [AccountID], [AccountName] FROM (select [AccountID], [AccountName], row_number()over(order by t.[AccountID]) as [rn] from [Accounts] as t where (([AccountName] + ' ' + [AccountID]) LIKE @filter)) as st where st.[rn] between @startIndex and @endIndex"
        SqlDataSource2.SelectParameters.Clear()
        SqlDataSource2.SelectParameters.Add("filter", TypeCode.String, String.Format("%{0}%", e.Filter))
        SqlDataSource2.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString())
        SqlDataSource2.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString())
        comboBox.DataSource = SqlDataSource2
        comboBox.DataBind()
    End Sub

    Protected Sub comboacctcode_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        Dim value As Long = 0
        If e.Value Is Nothing OrElse (Not Int64.TryParse(e.Value.ToString(), value)) Then
            Return
        End If
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource2.SelectCommand = "SELECT AccountID, AccountName FROM Accounts WHERE (AccountID = @ID) ORDER BY AccountID DESC"

        SqlDataSource2.SelectParameters.Clear()
        SqlDataSource2.SelectParameters.Add("ID", TypeCode.String, e.Value.ToString())
        comboBox.DataSource = SqlDataSource2
        comboBox.DataBind()
    End Sub

    Protected Sub txtbankcode_SelectedIndexChanged(sender As Object, e As EventArgs)
        Using con As New SqlConnection(m_strconnString)
            con.Open()

            Dim com8 As SqlCommand = con.CreateCommand
            com8.CommandType = CommandType.Text
            com8.CommandText = "select * from Accounts where AccountID=( '" + txtbankcode.Text + "')"
            com8.ExecuteNonQuery()
            Dim dr8 As SqlDataReader

            dr8 = com8.ExecuteReader()
            If dr8.Read Then
                TxtBankName.Text = dr8("AccountName").ToString()
            End If

            con.Close()
        End Using

    End Sub

    Protected Sub txtbankcode_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource2.SelectCommand = "SELECT [AccountID], [AccountName] FROM (select [AccountID], [AccountName], row_number()over(order by t.[AccountID]) as [rn] from [Accounts] as t where (([AccountName] + ' ' + [AccountID]) LIKE @filter)) as st where st.[rn] between @startIndex and @endIndex"
        SqlDataSource2.SelectParameters.Clear()
        SqlDataSource2.SelectParameters.Add("filter", TypeCode.String, String.Format("%{0}%", e.Filter))
        SqlDataSource2.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString())
        SqlDataSource2.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString())
        comboBox.DataSource = SqlDataSource2
        comboBox.DataBind()
    End Sub

    Protected Sub txtbankcode_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        Dim value As Long = 0
        If e.Value Is Nothing OrElse (Not Int64.TryParse(e.Value.ToString(), value)) Then
            Return
        End If
        Dim comboBox As ASPxComboBox = CType(source, ASPxComboBox)
        SqlDataSource2.SelectCommand = "SELECT AccountID, AccountName FROM Accounts WHERE (AccountID = @ID) ORDER BY AccountID DESC"

        SqlDataSource2.SelectParameters.Clear()
        SqlDataSource2.SelectParameters.Add("ID", TypeCode.String, e.Value.ToString())
        comboBox.DataSource = SqlDataSource2
        comboBox.DataBind()
    End Sub

    Private Sub bindbankcodes()
        'This drop contents from the devexpress search control into the required text boxes

        Using con As New SqlConnection(m_strconnString)
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT AccountID, AccountName FROM Accounts where Type='B' "
                cmd.Connection = con

                Dim dt As New DataTable()
                Using sda As New SqlDataAdapter(cmd)
                    sda.Fill(dt)
                    txtbankcode.DataSource = dt

                    txtbankcode.Columns.Add("AccountID").Width = 120
                    txtbankcode.Columns.Add("AccountName").Width = 170


                    txtbankcode.ValueField = "AccountID"
                    txtbankcode.TextField = "AccountName"

                    txtbankcode.TextFormatString = "{0}"
                    txtbankcode.DataBind()

                End Using
            End Using
        End Using


    End Sub
End Class
