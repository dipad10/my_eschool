﻿
Partial Class Admin_Accounts_Assets
    Inherits System.Web.UI.Page



    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/Accounts/Asset_purchase_edit.aspx")
    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim assetno As String = ASPxGridView1.GetSelectedFieldValues("AssetCode")(i).ToString
            If ASPxGridView1.Selection.Count = 0 Then
                Me.Msgbox1.ShowError("Please select an item to Dispose")
            Else

                Dim res As ResponseInfo = (New Cls_Assetpurchase).Delete(assetno)

                'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)

                Msgbox1.Showsuccess("" & count & " Asset(s) deleted successfully!")
            End If



        Next

    End Sub

    Protected Sub btnTransfer_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim assetno As String = ASPxGridView1.GetSelectedFieldValues("AssetCode")(i).ToString
            If ASPxGridView1.Selection.Count = 0 Then
                Me.Msgbox1.ShowError("Please select an Asset to Transfer")
            Else

                Response.Redirect("/Admin/Accounts/Asset_Transfers_edit.aspx?Trans-id=" & assetno & "")
            End If



        Next
    End Sub


    Protected Sub btndispose_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim assetno As String = ASPxGridView1.GetSelectedFieldValues("AssetCode")(i).ToString
            If ASPxGridView1.Selection.Count = 0 Then
                Me.Msgbox1.ShowError("Please select an Asset to Dispose")
            Else

                Response.Redirect("/Admin/Accounts/Asset_disposals_edit.aspx?Trans-id=" & assetno & "")
            End If



        Next
    End Sub

    Protected Sub cmdprint_Click(sender As Object, e As EventArgs)

    End Sub
End Class
