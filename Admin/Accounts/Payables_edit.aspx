﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Accounts/Account.master" AutoEventWireup="false" CodeFile="Payables_edit.aspx.vb" Inherits="Admin_Accounts_Payables_edit" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
     <h4 id="lblreceivable" runat="server" class="title"></h4>

    <div data-role="panel" class="panel">
    
   
        <div style="z-index:999;" class="content">
              <h5 class="hint-text">Transaction Details</h5>
            <hr />
            <div>
                <table width="100%">
                <tr height="50px">
                     <td>
                         <span class="text-secondary text-bold">StudentID/Name: </span>
                    </td>
                    <td>
                          <span>  <dx:ASPxComboBox ID="combo1" IncrementalFilteringMode="Contains" CssClass="text-secondary" Width="170px" OnSelectedIndexChanged="combo1_SelectedIndexChanged" AutoPostBack="true" AutoResizeWithContainer="True" AnimationType="Slide" Border-BorderColor="DarkBlue" OnItemsRequestedByFilterCondition="combo1_ItemsRequestedByFilterCondition" CallbackPageSize="10" EnableCallbackMode="True" OnItemRequestedByValue="combo1_ItemRequestedByValue" runat="server" EnableTheming="True" Theme="SoftOrange">
                           </dx:ASPxComboBox></span>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                   
                    <td>
                         <span class="text-secondary text-bold">Firstname</span>
                    </td>
                    <td>
                           <span>  <asp:TextBox ID="txtfirstname" CssClass="text-secondary" ReadOnly="true" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                    </td>
                      <td>
                        
                       <span class="text-secondary text-bold">Lastname</span>
                    </td>
                    <td>
                         <span>
                            <asp:TextBox ID="txtlastname" CssClass="text-secondary" ReadOnly="true" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox>
                     </span>
                    </td>
                </tr>
                <tr height="50px">
                  
                    <td>
                          <span class="text-secondary text-bold">Othername</span>
                    </td>
                    <td>
                          <span>   <asp:TextBox CssClass="text-secondary" ID="txtmiddlename" ReadOnly="true" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                    </td>
                      <td>
                        <span class=" text-secondary text-bold">Business type:</span>
                    </td>
                     <td>
                         <span>
                              <asp:DropDownList ID="ddlbustype"  CssClass="text-secondary" BorderColor="Orange" Width="170px" runat="server">
                             <asp:ListItem Enabled="true" Value="TBA">--select--</asp:ListItem>
                                   <asp:ListItem>SCHOOL FEES</asp:ListItem>
                              <asp:ListItem>ACCEPTANCE</asp:ListItem>
                              <asp:ListItem>P.T.A</asp:ListItem>
                              <asp:ListItem>EXAMINATION</asp:ListItem>
                               <asp:ListItem>CLEARANCE</asp:ListItem>
                         </asp:DropDownList>
                         </span>
                        
                     </td>
                </tr>
                <tr>
                  
                     <td>
                            <span class=" text-secondary text-bold">Transaction type:</span>
                     </td>
                     <td>
                         <span>
                               <asp:DropDownList ID="ddltranstype" CssClass="text-secondary" OnSelectedIndexChanged="ddltranstype_SelectedIndexChanged" BorderColor="Orange" Width="170px" runat="server">
                             <asp:ListItem Enabled="true" Value="NULL">--select--</asp:ListItem>
                                    <asp:ListItem Value="CASH">CASH</asp:ListItem>
                              <asp:ListItem Value="CHEQUE">CHEQUE</asp:ListItem>
                              <asp:ListItem Value="TRANSFER">DIRECT TRANSFER</asp:ListItem>
                             
                         </asp:DropDownList>
                         </span> 
                        
                     </td>
                     
                      <td>
                          <span class="text-secondary text-bold">Billing Date:</span>
                    </td>
                     <td>
                         <span>  <dx:ASPxDateEdit ID="billdate" CssClass="text-secondary" EditFormatString="dd MMM yyyy" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="170px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit></span>
                     </td>
                </tr>
                    <tr>
                         <td>
                          <span class="text-secondary text-bold">
                              <asp:Label ID="lbltellerno" Visible="false" runat="server" Text="Teller no."></asp:Label> </span>
                    </td>
                        <td>
                            <asp:TextBox CssClass="text-secondary" ID="txttellerno" Visible="false" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                <tr height="50px">
                  <td>
                       <span class="text-secondary text-bold">Receipt No:</span>
                  </td>
                       <td>
 <span>   <asp:TextBox ID="txtreceipt" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server">AUTO</asp:TextBox></span>
                       </td>
                       <td>
                             <span class="text-secondary text-bold">Narration:</span>
                       </td>
                       <td>
                           <span>   <asp:TextBox CssClass="text-secondary" Width="170px" ID="txtnarration" TextMode="MultiLine" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                       </td>
                </tr>
            </table>
            </div>
            
        </div>
        <div class="content">
            <h5 class="hint-text">Account Details</h5>
            <hr />
            <div>
                <table width="100%">
                    <tr height="50px">
                        <td>
                             <span class="text-secondary text-bold">Account code:</span>
                           
                        </td>
                      
                         <td>
                              <span>  <dx:ASPxComboBox ID="comboacctcode" CssClass="text-secondary" Width="170px" OnSelectedIndexChanged="comboacctcode_SelectedIndexChanged" AutoPostBack="true" AutoResizeWithContainer="True" AnimationType="Slide" Border-BorderColor="DarkBlue" OnItemsRequestedByFilterCondition="comboacctcode_ItemsRequestedByFilterCondition" CallbackPageSize="10" EnableCallbackMode="True" OnItemRequestedByValue="comboacctcode_ItemRequestedByValue" runat="server" EnableTheming="True" Theme="SoftOrange">
                           </dx:ASPxComboBox></span>
                         </td>
                        <td>
                    <span class="text-secondary text-bold">Account Name:</span>
                </td>
                          <td>
                         <span>  <asp:TextBox CssClass="text-secondary" ID="txtacctname" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                          </td>
               

                    </tr>

                    <tr>
                         <td>
                    <span class="text-secondary text-bold">Bank code:</span>
                </td>
                          <td>
                         <span> <dx:ASPxComboBox CssClass="text-secondary" ID="txtbankcode" Width="170px" OnSelectedIndexChanged="txtbankcode_SelectedIndexChanged" AutoPostBack="true" AutoResizeWithContainer="True" AnimationType="Slide" Border-BorderColor="DarkBlue" OnItemsRequestedByFilterCondition="txtbankcode_ItemsRequestedByFilterCondition" CallbackPageSize="10" EnableCallbackMode="True" OnItemRequestedByValue="txtbankcode_ItemRequestedByValue" runat="server" EnableTheming="True" Theme="SoftOrange">
                           </dx:ASPxComboBox></span>

                          </td>

                          <td>
                    <span class="text-secondary text-bold">Bank Name:</span>
                </td>
                          <td>
                         <span>  <asp:TextBox CssClass="text-secondary" ID="TxtBankName" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                          </td>

                        
                        
                    </tr>

                    <tr height="50px">
                        <td><span class="text-secondary text-bold">Bill: </span></td>
                         <td>  <span>  <asp:TextBox ID="txtbill" BorderColor="Orange" CssClass="text-secondary text-bold" onkeyup = "javascript:this.value=Comma(this.value);" BorderWidth="1" runat="server">0.00</asp:TextBox></span></td>
                         <td>
                             <span class="text-secondary text-bold">Amount paid: </span>
                         </td>
                         <td>
                              <span>  <asp:TextBox CssClass="text-secondary text-bold" ID="txtamountpaid" onkeyup = "javascript:this.value=Comma(this.value);" BorderColor="Orange" BorderWidth="1" runat="server">0.00</asp:TextBox></span>
                         </td>

                       
                      
                        
                    </tr>
                    <tr>
                         <td><span class="text-secondary text-bold">Others: </span></td>
                         <td>
                             <span>  <asp:TextBox CssClass="text-secondary text-bold" ID="txtothers" onkeyup = "javascript:this.value=Comma(this.value);" Text="0.00" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                         </td>
                         <td>
                             <span class="text-secondary text-bold">Outstanding:</span>
                         </td>
                         <td>
                              <span>  <asp:TextBox CssClass="text-secondary text-bold" ID="txtoutstanding" onkeyup = "javascript:this.value=Comma(this.value);" Text="0.00" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                         </td>
                      
                          

                         
                    </tr>
                    <tr height="50px">
                        <td>
                            <span class="text-secondary text-bold">Total: </span>
                        </td>
                         <td>
                               <span>  <asp:TextBox CssClass="text-secondary text-bold" ID="txttotal" onkeyup = "javascript:this.value=Comma(this.value);" Text="0.00" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                         </td>

                         <td>
                       <asp:Button ID="btncompute" Visible="true" Height="29px" OnClick="btncompute_Click" CssClass="button text-small bg-darkViolet fg-white mini-button" runat="server" Text="Compute" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                        </td>
                         <td>
                        <asp:Button ID="cmdadd" Visible="true" Height="29px" CssClass="button text-small bg-darkViolet fg-white mini-button" runat="server" Text="Add" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                         </td>
                         
                    </tr>
                    <tr height="50px">
                      <td>
                            <span class="text-secondary text-bold">Gross Paid: </span>
                        </td>
                         <td>
                               <span>  <asp:TextBox CssClass="text-secondary text-bold" ID="TxtAmountGross" onkeyup = "javascript:this.value=Comma(this.value);" Text="0.00" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                         </td>

                         <td>
                            <span class="text-secondary text-boldOutstanding Gross:" </span>
                        </td>
                         <td>
                               <span>  <asp:TextBox CssClass="text-secondary text-bold" ID="TxtOutstandingGross" onkeyup = "javascript:this.value=Comma(this.value);" Text="0.00" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                         </td>
                    </tr>
                  
                </table>

                <div>
                    <asp:DataGrid ID="DataGrid1" runat="server" CssClass="Grid2 text-small table striped hovered" CellPadding="3" Width="100%">
                                 <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
                                <Columns>
                                    <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                                </Columns>
                            </asp:DataGrid>

                              
                </div>

               
            </div>
        </div>
         <div class="align-center">
                   <span>  &nbsp;
              <asp:Button ID="cmdsave" Visible="true" Height="29px" CssClass="button text-small bg-darkViolet fg-white mini-button" runat="server" Text="Save" ToolTip="Save" Font-Size="X-Small" Font-Bold="True" /></span>

                                 <span> 
       <asp:Button ID="cmdcancel" Visible="true" OnClick="btncancel_ServerClick" Height="29px" CssClass="button text-small bg-darkViolet fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" /></span>
 

                               

                </div>
</div>
      <asp:SqlDataSource ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:Enrollment_Net10ConnectionString %>" runat="server"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:Enrollment_Net10ConnectionString %>" runat="server"></asp:SqlDataSource>

    
     <script type ="text/javascript">

         function Comma(Num) {
             Num += '';
             Num = Num.replace(/,/g, '');

             x = Num.split('.');
             x1 = x[0];

             x2 = x.length > 1 ? '.' + x[1] : '';


             var rgx = /(\d)((\d{3}?)+)$/;

             while (rgx.test(x1))

                 x1 = x1.replace(rgx, '$1' + ',' + '$2');

             return x1 + x2;

         }
    </script>

</asp:Content>

