﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Accounts/Account.master" AutoEventWireup="false" CodeFile="Asset_Disposals.aspx.vb" Inherits="Admin_Accounts_Asset_Disposals" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
    <h4 class="title">Asset Disposals</h4>
 
    <div>
        <div class="panel">
           <div class="content bg-white">
               

                <dx:ASPxGridView ID="ASPxGridView1" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-darkBlue" SettingsPager-PageSize="25" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource1" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" >
                   <Settings ShowFilterRow="True" ShowFilterBar="Auto" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                       
                       <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>
                    <dx:GridViewDataHyperLinkColumn FieldName="AssetCode" CellStyle-CssClass="text-small fg-darkBlue text-bold" PropertiesHyperLinkEdit-DisplayFormatString="AssetCode" PropertiesHyperLinkEdit-NavigateUrlFormatString="Asset_purchases_edit.aspx?editid={0}" VisibleIndex="1" CellStyle-ForeColor="#006600">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="Asset_purchases_edit.aspx?editid={0}" TextFormatString="AssetCode">
                        </PropertiesHyperLinkEdit>

<CellStyle CssClass="text-small fg-darkBlue text-bold" ForeColor="#006600"></CellStyle>
                       </dx:GridViewDataHyperLinkColumn>
                      
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight"  Caption="Asset name" FieldName="AssetName" VisibleIndex="2">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                  
                      
                       <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" Caption="Purchase Date" CellStyle-CssClass="text-small fg-grayLight" FieldName="TransactionDate" VisibleIndex="6">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataDateColumn>
                   
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" Caption="Category" FieldName="Categories" VisibleIndex="8">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                    
                       <dx:GridViewDataTextColumn PropertiesTextEdit-DisplayFormatString="₦{0:#,##0}" CellStyle-CssClass="text-small fg-grayLight" Caption="Purchase Amount" FieldName="PaidAmount" VisibleIndex="41">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>

                          <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" Caption="Submitted on" CellStyle-CssClass="text-small fg-grayLight" FieldName="SubmittedOn" VisibleIndex="6">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataDateColumn>                      
                   </Columns>
                   <Settings ShowFooter="True" />

                  <Styles>
            <AlternatingRow Enabled="true" />
                     
        </Styles>
               </dx:ASPxGridView>

               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Enrollment_Net10ConnectionString %>" SelectCommand="select * from AssetPurchase where Trans_Type='DISPOSAL' order by TransactionDate desc"></asp:SqlDataSource>



               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                           <asp:Button ID="btnnew" Visible="true" Height="29px" OnClick="btnnew_Click" CssClass="button text-small bg-lightGreen fg-white mini-button" runat="server" Text="New Purchase" Font-Size="X-Small" Font-Bold="True" />             
                      </td>
                         
                           <td>

                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click1" CssClass="button bg-red text-small fg-white mini-button" runat="server"  Font-Size="X-Small" Font-Bold="True" Text="Delete" />
                           </td>
                            <td>
                           <asp:Button ID="btnTransfer" Visible="true" Height="29px" OnClick="btnTransfer_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Transfer Asset" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                            <td>
                           <asp:Button ID="btndispose" Visible="true" Height="29px" OnClick="btndispose_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Dispose Asset" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                           
                           <td>
                           <asp:Button ID="cmdprint" Visible="true" Height="29px" OnClick="cmdprint_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Print Voucher" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>
    </div>
</asp:Content>

