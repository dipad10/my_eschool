﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Admin_Administrator_chart_of_accounts
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If _Connection.State = ConnectionState.Open Then
            _Connection.Close()

        End If
        If Not Page.IsPostBack Then

            loadGrid()


            datebetween.Date = Date.Now.ToString("dd-MMM-yyyy")
            dateand.Date = Date.Now.ToString("dd-MMM-yyyy")
        End If
    End Sub

    Private Sub loadGrid()

        'Dim rec As Enrollment.Account = (New Cls_Accounts).SelectAllaccounts(0)
        Dim Recs As List(Of Enrollment.Account) = (New Cls_Accounts).SelectAllaccounts

      
        GridView1.DataSource = Recs
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            Msgbox1.ShowHelp("No record found")
        End If
    End Sub
    Protected Sub btnnew_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("chart_of_accounts_edit.aspx")
    End Sub

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btnapply_ServerClick(sender As Object, e As EventArgs)

    End Sub
End Class
