﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports DevExpress.XtraReports.Parameters
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraPrinting.Preview
Imports DevExpress.XtraPrinting
Partial Class Admin_Accounts_Receivables
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    'Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
    '    If _Connection.State = ConnectionState.Open Then
    '        _Connection.Close()

    '    End If
    '    If Not Page.IsPostBack Then

    '        loadGrid()
    '    End If
    'End Sub

    'Private Sub loadGrid()
    '    _Connection.Open()
    '    Dim _cmd As SqlCommand = _Connection.CreateCommand()
    '    _cmd.CommandType = CommandType.Text
    '    _cmd.CommandText = "select * from Receivables where status='PENDING' ORDER BY ReceiptNo DESC"
    '    _cmd.ExecuteNonQuery()


    '    Dim dt As New DataTable()
    '    Dim sda As New SqlDataAdapter(_cmd)

    '    sda.Fill(dt)
    '    GridView1.DataSource = dt
    '    GridView1.DataBind()
    '    If GridView1.Rows.Count = 0 Then
    '        Msgbox1.ShowHelp("No record found")
    '    End If
    'End Sub
   

    'Protected Sub btnapply_Click(sender As Object, e As EventArgs)

    'End Sub

    'Protected Sub btnexport_ServerClick(sender As Object, e As EventArgs)
    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.AddHeader("content-disposition", "attachment;filename=StudentRecords.xls")
    '    Response.Charset = ""
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Using sw As New StringWriter()
    '        Dim hw As New HtmlTextWriter(sw)

    '        'To Export all pages
    '        GridView1.AllowPaging = False
    '        Me.loadGrid()

    '        GridView1.HeaderRow.BackColor = Color.White
    '        For Each cell As TableCell In GridView1.HeaderRow.Cells
    '            cell.BackColor = GridView1.HeaderStyle.BackColor
    '        Next
    '        For Each row As GridViewRow In GridView1.Rows
    '            row.BackColor = Color.White
    '            For Each cell As TableCell In row.Cells
    '                If row.RowIndex Mod 2 = 0 Then
    '                    cell.BackColor = GridView1.AlternatingRowStyle.BackColor
    '                Else
    '                    cell.BackColor = GridView1.RowStyle.BackColor
    '                End If
    '                cell.CssClass = "textmode"
    '            Next
    '        Next

    '        GridView1.RenderControl(hw)
    '        'style to format numbers to string
    '        Dim style As String = "<style> .textmode { } </style>"
    '        Response.Write(style)
    '        Response.Output.Write(sw.ToString())
    '        Response.Flush()
    '        Response.[End]()
    '    End Using
    'End Sub
    'Public Overrides Sub VerifyRenderingInServerForm(control As Control)
    '    ' Verifies that the control is rendered
    'End Sub

    Protected Sub btnprint_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.Msgbox1.ShowError("Please select an item to Print")
        Else
            For i As Integer = 0 To count - 1

                Dim studentno As String = ASPxGridView1.GetSelectedFieldValues("IDNo")(i).ToString
                Dim rcpno As String = ASPxGridView1.GetSelectedFieldValues("ReceiptNo")(i).ToString

                Dim rptfile As String = "/Admin/Report/params.aspx?rpt=~/Admin/App_Reports/Payment Receipt.rpt&@receiptno=" & rcpno & ""

                Response.Redirect(rptfile)


            Next

        End If
     


        'Dim count As Integer = 0
        'Dim SelectedIDs As New ArrayList

        'For Each r As GridViewRow In GridView1.Rows

        '    If CType(r.Cells(0).Controls(0).FindControl("chkItem"), CheckBox).Checked Then
        '        SelectedIDs.Add(r.Cells(1).Text)
        '    End If

        '    If SelectedIDs.Count = 0 Then
        '        'nothing was selected
        '        Me.Msgbox1.ShowError("Please select an item to Print")
        '    Else
        '        Dim rcpno As [String] = r.Cells(2).Text
        '        Dim rptfile As String = "/Admin/Report/params.aspx?rpt=~/Admin/App_Reports/Payment Receipt.rpt&@receiptno=" & rcpno & ""

        '        Response.Redirect(rptfile)

        'Dim report As New Payment_receipt


        '' Obtain a parameter, and set its value.
        'report.Parameters("parameter1").Value = rcpno

        '' Hide the Parameters UI from end-users.
        'report.Parameters("parameter1").Visible = False

        '' Show the report's print preview.
        'report.CreateDocument()
        'Dim ms As New MemoryStream()

        'Try
        '    report.PrintingSystem.SaveDocument(ms)
        '    ms.Seek(0, SeekOrigin.Begin)
        '    Dim array As Byte() = ms.ToArray()
        '    Session("Report") = array
        'Finally
        '    ms.Close()
        'End Try
        'ShowPreview()


        '    End If
        'Next

    End Sub

    Protected Sub btnapprove_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.Msgbox1.ShowError("Please select an item to Print")
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1

                Dim studentno As String = ASPxGridView1.GetSelectedFieldValues("IDNo")(i).ToString
                Dim rcpno As String = ASPxGridView1.GetSelectedFieldValues("ReceiptNo")(i).ToString
                If ASPxGridView1.Selection.Count = 0 Then
                    Me.Msgbox1.ShowError("Please select an item to Print")
                Else
                    _Connection.Open()
                    Dim _cmd As SqlCommand = _Connection.CreateCommand()
                    _cmd.CommandType = CommandType.Text
                    _cmd.CommandText = "Update Receivables Set status='APPROVED' where ReceiptNo=('" & rcpno & "')"
                    _cmd.ExecuteNonQuery()
                    _Connection.Close()


                    Msgbox1.Showsuccess("" & count & " Transaction(s) Approved successfully!")
                End If



            Next
        End If


       

        'For i As Integer = 0 To GridView1.Rows.Count - 1
        '    Dim cb As CheckBox = DirectCast(GridView1.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
        '    If cb.Checked = True Then
        '        count += 1
        '        If cb IsNot Nothing AndAlso cb.Checked Then
        '            Dim receiptno As String = GridView1.Rows(i).Cells(2).Text
        '            _Connection.Open()
        '            Dim _cmd As SqlCommand = _Connection.CreateCommand()
        '            _cmd.CommandType = CommandType.Text
        '            _cmd.CommandText = "Update Receivables Set status='APPROVED' where ReceiptNo=('" & receiptno & "')"
        '            _cmd.ExecuteNonQuery()
        '            _Connection.Close()

        '            'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)

        '            Msgbox1.Showsuccess("" & count & " Transaction(s) Approved successfully!")

        '        End If

        '    Else

        '    End If

        'Next
    End Sub

    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/Admin/Accounts/Receivables_edit.aspx")
    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)

    End Sub

    

    Sub ShowPreview()
        'Response.Redirect("ASPNETDocumentViewer.aspx")
        Response.Redirect("/Admin/Report/Viewer.aspx")
    End Sub

    Protected Function CreateURLQueryString(ByVal IDNo As Object, ByVal ReceiptNo As Object) As String
        Dim url As String = String.Format("Receivables_edit.aspx?IDNo={0}&ReceiptNo={1}", IDNo.ToString(), ReceiptNo.ToString())
        Return url
    End Function
End Class
