﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web
Partial Class Admin_Administrator_chart_of_accounts_edit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim acctID As String = Request.QueryString("AccountID")
            If acctID <> "" Then
                'fetch records
                Dim rec As Enrollment.Account = (New Cls_Accounts).SelectThisID(acctID)
                txtacctid.Text = rec.AccountID
                txtacctname.Text = rec.AccountName
                ddltype.SelectedValue = rec.Type
                lblreceivable.InnerText = "Account '" & acctID & "' Edit"

            Else
                lblreceivable.InnerText = "New Account"


            End If
        End If
    End Sub
    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        Dim A As New Cls_Accounts
        Dim acctid As String = Request.QueryString("AccountID")
        If acctid <> "" Then
            'update record
            Dim rec As Enrollment.Account = A.SelectThisID(acctid)
            rec.AccountID = txtacctid.Text
            rec.AccountName = txtacctname.Text
            rec.Type = ddltype.SelectedValue
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='chart_of_accounts.aspx';", True)


            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
            End If

        Else
            'create fresh record
            Dim rec As Enrollment.Account = A.SelectThisID(txtacctid.Text)
            If rec.AccountID Is Nothing Then  'if d acct id does not exist before in the system
                rec.AccountID = txtacctid.Text
                rec.AccountName = txtacctname.Text
                rec.Type = ddltype.SelectedValue
                Dim res As ResponseInfo = A.Insert(rec)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='chart_of_accounts.aspx';", True)


                Else
                    Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                    'LogError(Request.UserHostAddress, "Update Users", res.ErrorMessage, True)
                End If
            Else

                'if it exists
                Me.Msgbox1.ShowHelp("This accountId already exists in the system! Pls use another accountId")



            End If




        End If

    End Sub

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub

   
End Class
