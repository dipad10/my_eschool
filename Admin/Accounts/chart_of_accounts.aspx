﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Administrator/Administrator.master" AutoEventWireup="false" CodeFile="chart_of_accounts.aspx.vb" Inherits="Admin_Administrator_chart_of_accounts" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
   
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
     <h4 class="title">Chart of Accounts</h4>
    <div class="accordion" data-role="accordion" data-close-any="true">
      <div class="accordion">
    <div class="frame">
        <div class="heading">Search filter</div>
        <div class="content">
            <table width="100%">
         <tbody>
             <tr>
                 <td><span class=" text-small">Where</span></td> <td><span><asp:DropDownList BorderColor="Orange" Width="110px" CssClass="text-small" Height="20px" BorderWidth="1" ID="ddlfilter" runat="server">
                     <asp:ListItem Value="IDNo">Account ID</asp:ListItem>
                     <asp:ListItem Value="FirstName">Account name</asp:ListItem>
                    
                            </asp:DropDownList></span> </td>
                 <td><span class="text-small">Contains</span></td> <td><asp:TextBox ID="txtcontains" Width="110px" Height="20px" BorderColor="Orange" BorderWidth="1" CssClass="text-small" runat="server"></asp:TextBox></td>
                 <td><span class="text-small">Between</span> </td>
                      <td>

                     
                         <dx:ASPxDateEdit ID="datebetween" runat="server" EditFormatString="dd-MMM-yyyy" CssClass="text-small" Height="20px" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="110px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                  </td>
                  
                 <td><span class="text-small">And</span>  </td> 
                         <td>
                              <dx:ASPxDateEdit ID="dateand" CssClass="text-small" EditFormatString="dd-MMM-yyyy" Height="20px" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="110px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                         </td>
                        
<td>                                                         <button runat="server" id="btnapply" height="20px" onserverclick="btnapply_ServerClick" class="button bg-green fg-white text-small mini-button"><span class=" mif-search"></span> Search</button>

</td>
             </tr>
             
           
         </tbody>

     </table></div>
    </div>
          </div>
        </div>
    <div>
        <div class="panel">
           <div class="content bg-white">
               <asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <asp:CheckBox ID="chkall" runat="server" onclick="checkAll(this);" />
                             </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" onclick = "Check_Click(this)" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                       
                       
                       <asp:HyperLinkField HeaderText="AccountID" DataNavigateUrlFields="AccountID" DataTextField="AccountID"
                    DataNavigateUrlFormatString="chart_of_accounts_edit.aspx?AccountID={0}"/>
                        
                       <asp:BoundField DataField="AccountName" HeaderText="AccountName" SortExpression="AccountName" />
                        <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                   
         
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                          <button runat="server" id="btnnew" onserverclick="btnnew_ServerClick" class="button bg-lightGreen fg-white small-button"><span class="mif-plus"></span> New Account</button>
                      </td>
                          <td>
                              <button runat="server" id="btncancel" onserverclick="btncancel_ServerClick" class="button bg-green fg-white small-button"><span class="mif-backward"></span> Cancel</button>

                           </td>
                           <td>
                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click" CssClass="button bg-red fg-white small-button" runat="server" Text="Delete" />
                           </td>
                           
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>
    </div>
</asp:Content>


