﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Administrator/Administrator.master" AutoEventWireup="false" CodeFile="chart_of_accounts_edit.aspx.vb" Inherits="Admin_Administrator_chart_of_accounts_edit" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
     <h4 id="lblreceivable" runat="server" class="title"></h4>
    <div data-role="panel" class="panel">
    
   
        <div style="z-index:999;" class="content">
              <h5 class="hint-text">Account Details</h5>
            <hr />
            <div>
                <table width="100%">
                <tr height="50px">
                     <td>
                         <span class="text-secondary text-bold">Account ID: </span>
                    </td>
                    <td>
                      <span>  <asp:TextBox ID="txtacctid" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                    <td> <span class="text-secondary text-bold">Account Name: </span></td>
                    <td>
                       <span>  <asp:TextBox ID="txtacctname" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                    <td>
                         <span class="text-secondary text-bold">Type: </span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddltype" Width="110px" CssClass="text-secondary" runat="server">
                            <asp:ListItem Value="A">School Account</asp:ListItem>
                             <asp:ListItem Value="B">Bank Account</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
          </table>
               
            </div>
        </div>
         <div class="align-center">
                   <span>  &nbsp;<asp:Button ID="cmdsave" OnClick="cmdsave_Click" runat="server" CssClass="button bg-green fg-white small-button" Text="Save" /></span>

                                 <span>  <button runat="server" id="btncancel" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>

                </div>
</div>
</asp:Content>

