﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Accounts/Account.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="Receivables.aspx.vb" Inherits="Admin_Accounts_Receivables" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
    <h4 class="title">Account Receivables</h4>
 
    <div>
        <div class="panel">
           <div class="content bg-white">
               <%--<asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <asp:CheckBox ID="chkall" runat="server" onclick="checkAll(this);" />
                             </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" onclick = "Check_Click(this)" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
             <asp:BoundField DataField="TransGUID" Visible="false" HeaderText="Student No." SortExpression="TransGUID" />

                       <asp:HyperLinkField HeaderText="Student No." DataNavigateUrlFields="IDNo,ReceiptNo" DataTextField="IDNo"
                    DataNavigateUrlFormatString="Receivables_edit.aspx?IDNo={0}&amp;ReceiptNo={1}"/>
                          <asp:BoundField DataField="ReceiptNo" HeaderText="ReceiptNo" SortExpression="ReceiptNo" />

                       <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />

                        <asp:BoundField DataField="LastName" HeaderText="Surname" SortExpression="LastName" />
                       <asp:BoundField DataField="BusinessType" HeaderText="Bus.Type" SortExpression="BusinessType" />    
             <asp:BoundField DataField="TDate" DataFormatString="{0:dd MMM yyyy}" HeaderText="Trans.Date" SortExpression="TDate" />
              
                    
                       <asp:BoundField DataField="Outstandng" DataFormatString="₦{0:#,##0}" HeaderText="Balance" SortExpression="Outstandng" />
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>--%>

                        <dx:ASPxGridView ID="ASPxGridView1" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-darkBlue" SettingsPager-PageSize="25" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource1" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" >
                   <Settings ShowFilterRow="True" ShowFilterBar="Auto" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                       
                       <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>

                  <%--  <dx:GridViewDataHyperLinkColumn FieldName="IDNo" CellStyle-CssClass="text-small fg-darkBlue text-bold" PropertiesHyperLinkEdit-DisplayFormatString="IDNo" PropertiesHyperLinkEdit-NavigateUrlFormatString="Receivables_edit.aspx?IDNo={0}&amp;ReceiptNo={1}" VisibleIndex="1" CellStyle-ForeColor="#006600">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="Receivables_edit.aspx?IDNo={0}" TextFormatString="IDNo">
                        </PropertiesHyperLinkEdit>

<CellStyle CssClass="text-small fg-darkBlue text-bold" ForeColor="#006600"></CellStyle>
                       </dx:GridViewDataHyperLinkColumn>--%>
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" Caption="StudentNo." VisibleIndex="2">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                           <DataItemTemplate>
                               <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" 
							NavigateUrl='<%#CreateURLQueryString(Eval("IDNo"), Eval("ReceiptNo"))%>' 
							Text='<% #Eval("IDNo")%>' />
                           </DataItemTemplate>
                       </dx:GridViewDataTextColumn>

                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="ReceiptNo" VisibleIndex="2">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                  
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="FirstName" VisibleIndex="4">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="LastName" VisibleIndex="4">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="BusinessType" VisibleIndex="4">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                      
                       <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small fg-grayLight" FieldName="TDate" VisibleIndex="6">
<PropertiesDateEdit DisplayFormatString="dd-MMM-yyyy"></PropertiesDateEdit>

<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataDateColumn>
                   
                       <dx:GridViewDataTextColumn PropertiesTextEdit-DisplayFormatString="₦{0:#,##0}" CellStyle-CssClass="text-small fg-grayLight" FieldName="Outstandng" VisibleIndex="8">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                    
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small fg-grayLight" FieldName="Status" VisibleIndex="41">
<CellStyle CssClass="text-small fg-grayLight"></CellStyle>
                       </dx:GridViewDataTextColumn>
                    
                   </Columns>
                   <Settings ShowFooter="True" />
                  <Styles>
            <AlternatingRow Enabled="true" />
                       
        </Styles>
               </dx:ASPxGridView>

               <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Enrollment_Net10ConnectionString %>" SelectCommand="select * from Receivables ORDER BY ReceiptNo DESC"></asp:SqlDataSource>
               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                      <asp:Button ID="btnnew" Visible="true" Height="29px" OnClick="btnnew_Click" CssClass="button text-small bg-lightGreen fg-white mini-button" runat="server" Text="New Receivable" Font-Size="X-Small" Font-Bold="True" />

                      </td>
                           <td>
                     <asp:Button ID="btnprint" Visible="true" Height="29px" OnClick="btnprint_Click" CssClass="button text-small bg-darkGreen fg-white mini-button" runat="server" Text="Print Receipt" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                           <td>
                    <asp:Button ID="btnapprove" Visible="true" Height="29px" OnClick="btnapprove_Click" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Approve" ToolTip="Approve Transaction" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                          <td>
                          <asp:Button ID="btncancel" Visible="true" Height="29px" OnClick="btncancel_Click" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                           <td>
                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click" CssClass="button bg-lightRed fg-white mini-button" runat="server" Text="Delete" Font-Size="X-Small" Font-Bold="True" />
                           </td>
                          
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>
    </div>
</asp:Content>

