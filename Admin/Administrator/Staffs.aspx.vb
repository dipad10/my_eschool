﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Admin_Administrator_Staffs
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If _Connection.State = ConnectionState.Open Then
            _Connection.Close()

        End If
        If Not Page.IsPostBack Then

            loadGrid()


            datebetween.Date = Date.Now.ToString("dd-MMM-yyyy")
            dateand.Date = Date.Now.ToString("dd-MMM-yyyy")
        End If

       
    End Sub

    Private Sub loadGrid()

        'Dim rec As Enrollment.School_setting = (New cls_school_settings).Selectschool(0)


        _Connection.Open()
        Dim _cmd As SqlCommand = _Connection.CreateCommand()
        _cmd.CommandType = CommandType.Text
        _cmd.CommandText = "select * from Staffs ORDER BY StaffID DESC"
        _cmd.ExecuteNonQuery()


        Dim dt As New DataTable()
        Dim sda As New SqlDataAdapter(_cmd)

        sda.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        If GridView1.Rows.Count = 0 Then
            Msgbox1.ShowHelp("No record found")
        End If
    End Sub
    Protected Sub btnnewstaff_ServerClick(sender As Object, e As EventArgs)
        Response.Redirect("staffs_edit.aspx")
    End Sub

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btnapply_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Protected Sub ddlsort_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        GridView1.PageIndex = e.NewPageIndex
        Me.loadGrid()


    End Sub
End Class
