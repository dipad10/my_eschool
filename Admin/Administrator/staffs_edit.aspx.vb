﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web
Partial Class Admin_Administrator_staffs_edit
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim staffid As String = Request.QueryString("staffID")

            If staffid <> "" Then

                Dim rec As Enrollment.Staff = (New Cls_staffs).SelectThisID(staffid)
                txtstaffid.Text = rec.StaffID
                ddltitle.SelectedValue = rec.Field5
                txtfirstname.Text = rec.Field6
                txtsurname.Text = rec.Field7
                txtothernames.Text = rec.Field8
                ddlreligion.SelectedValue = rec.Religion
                ddlmarital.SelectedValue = rec.Marital_status
                ddltitle.SelectedValue = rec.Field9
                ddlstate.SelectedValue = rec.State_of_origin
                txtlga.Text = rec.LGA
                txtcurrentaddress.Text = rec.Current_address
                txtemail.Text = rec.Email
                txtphone1.Text = rec.Phone1
                txtphone2.Text = rec.Phone2
                txtpermanentaddress.Text = rec.Permanent_address
                txtnokname.Text = rec.Nok_name
                txtnokaddress.Text = rec.Nok_address
                txtnokrelate.Text = rec.Nok_Relationship
                ddltype.SelectedValue = rec.Type
                txtdob.Date = Format(rec.DOB, "dd-MMM-yyyy")

                If rec.Type = "TEACHING" Then
                    pnlteaching.Visible = True
                    pnlnonteaching.Visible = False

                    ddldepartments.SelectedValue = rec.Department
                    ddlsubjects.SelectedValue = rec.Subject

                ElseIf rec.Type = "NON-TEACHING" Then
                    pnlnonteaching.Visible = True
                    pnlteaching.Visible = False
                    ddlsection.SelectedValue = rec.Department


                    txtqualification.Text = rec.Qualification
                    txtobtained.Text = rec.Institution_obtained
                    txtstartdate.Date = rec.start_date
                    txtenddate.Date = rec.end_date



                End If

            End If





            Call bindddldepartments()




            'BindDt(Me.DataGrid1, "Table1")
            CreateGrid(staffid)

        End If
    End Sub
    Protected Sub ddltype_SelectedIndexChanged(sender As Object, e As EventArgs)
        Select Case ddltype.SelectedValue
            Case "TEACHING"
                pnlteaching.Visible = True
                pnlnonteaching.Visible = False

            Case "NON-TEACHING"
                pnlnonteaching.Visible = True
                pnlteaching.Visible = False

        End Select
    End Sub

    

    Protected Sub btncancel_ServerClick(sender As Object, e As EventArgs)

    End Sub

    Protected Sub btnadd_Click(sender As Object, e As EventArgs)

        Dim dt As DataTable = GetDt("table1")
        Dim dr As DataRow = dt.NewRow
        dr(0) = dt.Rows.Count + 1
        dr(1) = Me.txtqualification.Text
        dr(2) = Me.txtobtained.Text
        dr(3) = Format(Me.txtstartdate.Date, "dd-MMM-yyyy")
        dr(4) = Format(Me.txtenddate.Date, "dd-MMM-yyyy")

        dt.Rows.Add(dr)
        BindDt(DataGrid1, "table1")

        'Dim X As DataRow, Y As Integer
        'For Each X In dt.Rows
        '    Y += X(4)
        'Next

        'Call clear()



    End Sub

    Private Sub bindddldepartments()
        Using myconnection As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)


            myconnection.Open()
            Using mycommand As SqlCommand = myconnection.CreateCommand
                mycommand.CommandType = CommandType.Text
                mycommand.CommandText = "select DepName from CodDepartment"

                Dim _dt As DataTable = New DataTable()
                Dim _da As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da.Fill(_dt)
                If _dt.Rows.Count > 0 Then
                    ddldepartments.Items.Insert(0, New ListItem(String.Empty, String.Empty))
                    ddldepartments.DataSource = _dt
                    ddldepartments.DataTextField = "DepName"
                    ddldepartments.DataValueField = "DepName"
                    ddldepartments.DataBind()
                    ddldepartments.Items.Insert(0, New ListItem(String.Empty, String.Empty))
                End If
                myconnection.Close()

            End Using
        End Using
    End Sub

    Protected Sub ddldepartments_SelectedIndexChanged(sender As Object, e As EventArgs)
        Using myconnection2 As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)

            myconnection2.Open()
            Using mycommand As SqlCommand = myconnection2.CreateCommand
                mycommand.CommandType = CommandType.Text
                mycommand.CommandText = "select Courses from Departmentcourses where Department=('" & ddldepartments.SelectedValue & "')"

                Dim _dt1 As DataTable = New DataTable()
                Dim _da1 As SqlDataAdapter = New SqlDataAdapter(mycommand)
                _da1.Fill(_dt1)
                If _dt1.Rows.Count > 0 Then
                    ddlsubjects.DataSource = _dt1
                    ddlsubjects.DataTextField = "Courses"
                    ddlsubjects.DataValueField = "Courses"
                    ddlsubjects.DataBind()
                    ddlsubjects.Items.Insert(0, New ListItem(String.Empty, String.Empty))

                End If
            End Using
        End Using
    End Sub

    Private Function GetDt(ByVal tablename As String) As DataTable
        If TypeOf ViewState(tablename) Is DataTable Then
            Return CType(ViewState(tablename), DataTable)
        Else
            Dim Dt As New DataTable
            Dt.Columns.Add(New DataColumn("SN", GetType(Int32)))
            Dt.Columns.Add(New DataColumn("Qualification", GetType(String)))
            Dt.Columns.Add(New DataColumn("Institution", GetType(String)))
            Dt.Columns.Add(New DataColumn("Start Date", GetType(Date)))
            Dt.Columns.Add(New DataColumn("End Date", GetType(Date)))
        

            Return Dt
        End If
    End Function

    Private Sub BindDt(ByVal DtGrid As DataGrid, ByVal tablename As String)
        Dim Dt As DataTable = GetDt(tablename)
        Dim Dv As New DataView(Dt)
        ViewState(tablename) = Dt
        DtGrid.DataSource = Dv
        DtGrid.DataBind()

    End Sub

    'Private Sub CreateGrid(ByVal TransID As String)

    '    Dim Recs As List(Of Enrollment.Receivable) = (New Cls_Receivable).SelectThisByRefRcp(TransID)
    '    Dim da As DataTable = getDt("table1")
    '    'A loop is needed here to list all existing records
    '    For Each r In Recs
    '        Dim dr As DataRow = da.NewRow()
    '        dr(0) = da.Rows.Count + 1
    '        dr(1) = r.ReceiptNo
    '        dr(2) = r.AccountName
    '        dr(3) = r.BankAcctName
    '        dr(4) = CDbl(r.Bill & vbNullString).ToString
    '        dr(5) = CDbl(r.Total & vbNullString).ToString
    '        dr(6) = CDbl(r.Outstandng & vbNullString).ToString

    '        da.Rows.Add(dr)
    '    Next

    '    SaveDt("table1", da)
    '    BindDt(DataGrid1, "table1")
    'End Sub


    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        Dim dt As DataTable = GetDt("table1")
        Dim r As DataRow, Q As Int16 = 1
        Dim staffid As String = Request.QueryString("staffID")
        Dim P As New Cls_staffs

        Dim D As New Cls_GradeCompute
        'For Each r In dt.Rows
        '    r(0) = Q : Q += 1

        If staffid <> "" Then
            'update records
            Dim rec As Enrollment.Staff = P.SelectThisID(staffid)

            For Each r In dt.Rows
                r(0) = Q : Q += 1

                rec.Name = ddltitle.SelectedValue & " " & txtfirstname.Text & " " & txtsurname.Text
                rec.Sex = ddltitle.SelectedValue
                rec.Marital_status = ddlmarital.SelectedValue
                rec.Religion = ddlreligion.SelectedValue
                rec.LGA = txtlga.Text
                rec.State_of_origin = ddlstate.SelectedValue
                rec.Current_address = txtcurrentaddress.Text
                rec.Permanent_address = txtpermanentaddress.Text
                rec.Phone1 = txtphone1.Text
                rec.Phone2 = txtphone2.Text
                rec.Email = txtemail.Text
                rec.Nok_name = txtnokname.Text
                rec.Nok_address = txtnokaddress.Text
                rec.Nok_Relationship = txtnokrelate.Text
                rec.Qualification = r(1)
                rec.Institution_obtained = r(2)
                rec.start_date = r(3)
                rec.end_date = r(4)
                rec.Field5 = Session("uname").ToString()
                rec.DOB = txtdob.Date
                rec.Field5 = ddltitle.SelectedValue
                rec.Field6 = txtfirstname.Text
                rec.Field7 = txtsurname.Text
                rec.Field8 = txtothernames.Text


                Select Case ddltype.SelectedValue
                    Case "TEACHING"
                        rec.Remarks = "Teacher"
                        rec.Type = ddltype.SelectedValue
                        rec.Department = ddldepartments.SelectedValue
                        rec.Subject = ddlsubjects.SelectedValue
                        rec.Class_assigned = ddldepartments.SelectedValue
                    Case "NON-TEACHING"
                        rec.Remarks = "Worker"
                        rec.Type = ddltype.SelectedValue
                        rec.Department = ddlsection.SelectedValue


                End Select
                Dim res As ResponseInfo = P.Update(rec)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='staffs.aspx';", True)
                Else
                    Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                End If
            Next


        

        Else 'create fresh record to insert into the database
            Dim Conn As New SqlConnection(m_strconnString)

            Conn.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'Staffs' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, Conn)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)
            Dim Datepart As Long = (DateString.Length)
            Conn.Close()

            Dim ID As String = "STF" & "/" & Date.Now.Year.ToString() & "/" & Format(NValue, "0000")
            Me.txtstaffid.Text = ID


            Conn.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='Staffs' "
            Dim comm1 As New SqlCommand(sqlNoUpD, Conn)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()

            For Each r In dt.Rows
                r(0) = Q : Q += 1


                Dim rec As New Enrollment.Staff
                rec.StaffID = txtstaffid.Text
                rec.Name = ddltitle.SelectedValue & " " & txtfirstname.Text & " " & txtsurname.Text
                rec.Sex = ddltitle.SelectedValue
                rec.Marital_status = ddlmarital.SelectedValue
                rec.Religion = ddlreligion.SelectedValue
                rec.LGA = txtlga.Text
                rec.State_of_origin = ddlstate.SelectedValue
                rec.Current_address = txtcurrentaddress.Text
                rec.Permanent_address = txtpermanentaddress.Text
                rec.Phone1 = txtphone1.Text
                rec.Phone2 = txtphone2.Text
                rec.Email = txtemail.Text
                rec.Nok_name = txtnokname.Text
                rec.Nok_address = txtnokaddress.Text
                rec.Nok_Relationship = txtnokrelate.Text
                rec.Qualification = r(1)
                rec.Institution_obtained = r(2)
                rec.start_date = r(3)
                rec.end_date = r(4)
                rec.Field5 = Session("uname").ToString()
                rec.DOB = txtdob.Date
                rec.Field5 = ddltitle.SelectedValue
                rec.Field6 = txtfirstname.Text
                rec.Field7 = txtsurname.Text
                rec.Field8 = txtothernames.Text
                rec.Field9 = ddltitle.SelectedValue

                Select Case ddltype.SelectedValue
                    Case "TEACHING"
                        rec.Remarks = "Teacher"
                        rec.Type = ddltype.SelectedValue
                        rec.Department = ddldepartments.SelectedValue
                        rec.Subject = ddlsubjects.SelectedValue
                        rec.Class_assigned = ddldepartments.SelectedValue
                    Case "NON-TEACHING"
                        rec.Remarks = "Worker"
                        rec.Type = ddltype.SelectedValue
                        rec.Department = ddlsection.SelectedValue


                End Select
                Dim res As ResponseInfo = P.Insert(rec)
                If res.ErrorCode = 0 Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='staffs.aspx';", True)
                Else
                    Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
                End If
            Next


            'create fresh records 




        End If

        'Next


    End Sub

    Private Sub CreateGrid(ByVal staffid As String)

        Dim Recs As List(Of Enrollment.Staff) = (New Cls_staffs).SelectThisByRefstaffid(staffid)
        Dim da As DataTable = GetDt("table1")
        'A loop is needed here to list all existing records
        For Each r In Recs
            Dim dr As DataRow = da.NewRow()
            dr(0) = da.Rows.Count + 1
            dr(1) = r.Qualification
            dr(2) = r.Institution_obtained
            dr(3) = Format(r.start_date, "dd-MMM-yyyy")
            dr(4) = Format(r.end_date, "dd-MMM-yyyy")

            da.Rows.Add(dr)
        Next

        saveDt("table1", da)
        BindDt(DataGrid1, "table1")
    End Sub
    Private Sub saveDt(ByVal tablename As String, ByVal newDt As DataTable)
        ViewState(tablename) = NewDt
    End Sub
End Class
