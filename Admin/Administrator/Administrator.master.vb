﻿
Partial Class Admin_Administrator_Administrator
    Inherits System.Web.UI.MasterPage

     
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim rec As Enrollment.SecUser = (New Cls_secusers).SelectThisID(Session("uname"))
            Select Case rec.permission
                Case "Registrar", "Accountant"
                    Response.Redirect("/Accessdenied.aspx")
                Case Else
                    Exit Sub


            End Select
        End If

    End Sub
End Class

