﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Admin_Administrator_Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Using con As New SqlConnection(m_strconnString)
            con.Open()

            Dim cmd As New SqlCommand("Getpiechartdetails", con)

            cmd.CommandType = CommandType.StoredProcedure


            Dim ds As New DataSet()
            Using sda As New SqlDataAdapter(cmd)
                sda.Fill(ds)
                For Each row As DataRow In ds.Tables(0).Rows
                    PieChart1.PieChartValues.Add(New AjaxControlToolkit.PieChartValue() With { _
                    .Category = row("Field2").ToString(), _
                     .Data = Convert.ToDecimal(row("Class")) _
                    })
                Next
            End Using

            Using cmd2 As New SqlCommand()
                cmd2.CommandText = "select count(*) from StudentRegistration"
                cmd2.Connection = con
                Dim count As Int32 = DirectCast(cmd2.ExecuteScalar(), Int32)
                If Not String.IsNullOrEmpty(count) Then
                    Label1.Text = count
                Else
                    Label1.Text = "0"
                End If


            End Using

            con.Close()


        End Using


       
    End Sub
End Class
