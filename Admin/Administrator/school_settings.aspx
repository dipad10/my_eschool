﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Administrator/Administrator.master" AutoEventWireup="false" CodeFile="school_settings.aspx.vb" Inherits="Admin_Administrator_school_settings" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
     <h4 class="title">School settings</h4>
    <div>
        <div class="panel">
           <div class="content bg-white">
               <asp:GridView ID="GridView1" CssClass="Grid2 text-small table striped hovered"  Width="100%" runat="server" AutoGenerateColumns="False">
                   <Columns>
                         <asp:TemplateField>
                             <HeaderTemplate>
                                 <asp:CheckBox ID="chkall" runat="server" onclick="checkAll(this);" />
                             </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkItem" onclick = "Check_Click(this)" runat="server" />
                    </ItemTemplate>
                             </asp:TemplateField>
                       
                       
                       <asp:HyperLinkField HeaderText="ID" DataNavigateUrlFields="ID" DataTextField="ID"
                    DataNavigateUrlFormatString="school_settings.aspx?ID={0}"/>
                        
                       <asp:BoundField DataField="logo" HeaderText="SchoolLogo" SortExpression="logo" />
                        <asp:BoundField DataField="Name" HeaderText="SchoolName" SortExpression="LastName" />
                   
        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />   
 <asp:HyperLinkField HeaderText="Website" DataNavigateUrlFields="Website" DataTextField="Website"/>
                        <asp:BoundField DataField="ABVR" HeaderText="ABBV." SortExpression="ABVR" />   
                          </Columns>
                   <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
               </asp:GridView>
               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                          <button runat="server" id="btnnewschool" onserverclick="btnnewschool_ServerClick" class="button bg-lightGreen fg-white small-button"><span class="mif-plus"></span> New Receivable</button>
                      </td>
                          <td>
                              <button runat="server" id="btncancel" onserverclick="btncancel_ServerClick" class="button bg-green fg-white small-button"><span class="mif-backward"></span> Cancel</button>

                           </td>
                           <td>
                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click" CssClass="button bg-red fg-white small-button" runat="server" Text="Delete" />
                           </td>
                           
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>
    </div>
</asp:Content>

