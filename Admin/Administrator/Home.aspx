﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Administrator/Administrator.master" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Admin_Administrator_Home" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <div class="row cells2">
         <div class="cell">
             <div class="panel">
    <div class="heading fg-white bg-darkRed">
        <span class="title">Class Distribution</span>
    </div>
    <div style="height:340px;" class="content">
           <ajaxToolkit:PieChart ID="PieChart1" Width="350px" Height="250px" BorderStyle="None" BorderWidth="0" runat="server"></ajaxToolkit:PieChart>
    </div>
</div>
         </div>

          <div class="cell">
              <div class="panel">
    <div class="heading fg-white bg-darkRed">
        <span class="title">Total number of Students</span>
    </div>
    <div style="height:340px;" class="content">
        <div style="margin-left:75px;">
            <asp:Label ID="Label1" runat="server" CssClass="text-bold" Font-Size="200px"></asp:Label>
        </div>
    </div>
</div>
         </div>
     </div>

    

       

      


</asp:Content>

