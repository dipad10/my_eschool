﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/Administrator/Administrator.master" AutoEventWireup="false" CodeFile="staffs_edit.aspx.vb" Inherits="Admin_Administrator_staffs_edit" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>

      <h4 id="lblstaff" runat="server" class="title"></h4>
    <div data-role="panel" class="panel">
    
   
        <div style="z-index:999;" class="content">
              <h5 class="hint-text">Personal Details</h5>
            <hr />
            <div>
                <table width="100%">
                <tr height="50px">
                     <td>
                         <p class="text-secondary text-bold">Staff ID: </p>
                    </td>
                    <td>
                      <span>  <asp:TextBox ID="txtstaffid" Text="AUTO" Enabled="false" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                    <td> <p class="text-secondary text-bold">Title: </p></td>
                    <td>
                       <span>  
                           <asp:DropDownList ID="ddltitle" CssClass="text-secondary" Width="150px" runat="server">
                               <asp:ListItem>Mr.</asp:ListItem>
                                <asp:ListItem>Mrs.</asp:ListItem>
                           </asp:DropDownList></span>

                    </td>
                 
                </tr>
                    <tr>
                        <td><p class="text-secondary text-bold">Firstname: </p></td>
                         <td>
                          <span>  <asp:TextBox ID="txtfirstname" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                         </td>
                         <td><p class="text-secondary text-bold">Surname: </p></td>
                         <td>
                          <span>  <asp:TextBox ID="txtsurname" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                         </td>
                    </tr>
                    <tr>
                        <td><p class="text-secondary text-bold">Othernames: </p></td>
                         <td>
                          <span>  <asp:TextBox ID="txtothernames" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                         </td>
                         <td>
                             <p class="text-secondary text-bold">Religion: </p>
                         </td>
                         <td>
                                 <span>  
                           <asp:DropDownList ID="ddlreligion" CssClass="text-secondary" Width="150px" runat="server">
                               <asp:ListItem>Christainity</asp:ListItem>
                                <asp:ListItem>Muslim</asp:ListItem>
                           </asp:DropDownList></span>
                         </td>
                    </tr>
                    <tr>
                        <td><p class="text-secondary text-bold">Date of Birth: </p></td>
                        <td>
                              <dx:ASPxDateEdit ID="txtdob" CssClass="text-secondary" EditFormatString="dd-MMM-yyyy" Height="20px" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="150px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                        </td>
                        <td><p class="text-secondary text-bold">Marital Status: </p></td>
                        <td>
                              <span>  
                           <asp:DropDownList ID="ddlmarital" CssClass="text-secondary" Width="150px" runat="server">
                               <asp:ListItem>Married</asp:ListItem>
                                <asp:ListItem>Single</asp:ListItem>
                           </asp:DropDownList></span>
                        </td>
                    </tr>



                    <tr>
                        <td><p class="text-secondary text-bold">State of Origin: </p></td>
                        <td>
                             <span><asp:DropDownList ID="ddlstate" BorderColor="Orange" CssClass="text-secondary" Width="150px" runat="server">
                            <asp:ListItem Selected="True" Value="null">--select state--</asp:ListItem>
                            <asp:ListItem> Abia State</asp:ListItem>
 <asp:ListItem> Adamawa State</asp:ListItem>
<asp:ListItem>Akwa Ibom State</asp:ListItem>
<asp:ListItem>Anambra State</asp:ListItem>
  <asp:ListItem>Bauchi State</asp:ListItem>
 <asp:ListItem>Bayelsa State</asp:ListItem>
<asp:ListItem>Benue State</asp:ListItem>
<asp:ListItem>Borno State</asp:ListItem>
  <asp:ListItem>Cross River State</asp:ListItem>
 <asp:ListItem>Delta State</asp:ListItem>
<asp:ListItem>Ebonyi State</asp:ListItem>
 <asp:ListItem>Edo State</asp:ListItem>
 <asp:ListItem>Ekiti State</asp:ListItem>
<asp:ListItem>Enugu State</asp:ListItem>
<asp:ListItem>Gombe State</asp:ListItem>
  <asp:ListItem>Imo State</asp:ListItem>
 <asp:ListItem>Jigawa State</asp:ListItem>
<asp:ListItem>Kaduna State</asp:ListItem>
<asp:ListItem>Kano State</asp:ListItem>
  <asp:ListItem>Katsina State</asp:ListItem>
 <asp:ListItem>Kebbi State</asp:ListItem>
<asp:ListItem>Kogi State</asp:ListItem>
 <asp:ListItem>Kwara State</asp:ListItem>
<asp:ListItem>Lagos State</asp:ListItem>
<asp:ListItem>Nasarawa State</asp:ListItem>
  <asp:ListItem>Niger State</asp:ListItem>
 <asp:ListItem>Ogun State</asp:ListItem>
<asp:ListItem>Ondo State</asp:ListItem>
<asp:ListItem>Osun State</asp:ListItem>
  <asp:ListItem>Oyo State</asp:ListItem>
 <asp:ListItem>Plateau State</asp:ListItem>
<asp:ListItem>Rivers State</asp:ListItem>
 <asp:ListItem>Sokoto State</asp:ListItem>
 <asp:ListItem>Taraba State</asp:ListItem>
<asp:ListItem>Yobe State</asp:ListItem>
<asp:ListItem>Zamfara State</asp:ListItem>
  <asp:ListItem>Abuja</asp:ListItem>
                        </asp:DropDownList></span>

                        </td>
                        <td><p class="text-secondary text-bold">Origin Local Government: </p></td>
                        <td>
                        <span>  <asp:TextBox ID="txtlga" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                        </td>
                    </tr>
                    </table>
                     </div>
            </div>
                    <div class="content">
                          <h5 class="hint-text">Contact Details</h5>
            <hr />
            <div>
                <table width="100%">

                         <tr>
                        <td><p class="text-secondary text-bold">Current address: </p></td>
                        <td>
                        <span>  <asp:TextBox ID="txtcurrentaddress" TextMode="MultiLine" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                        </td>
                        <td><p class="text-secondary text-bold">Permanent address: </p></td>
                        <td>
                        <span>  <asp:TextBox ID="txtpermanentaddress" TextMode="MultiLine" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                        </td>
                    </tr>

                        
                    <tr>
                        <td><p class="text-secondary text-bold">Email: </p></td>
                         <td>    <span>  <asp:TextBox ID="txtemail" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span></td>
                         <td>
                             <p class="text-secondary text-bold">Phone1: </p>
                         </td>
                         <td>
                              <span>  <asp:TextBox ID="txtphone1" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>
                         </td>
                    </tr>

                     <tr>
                        <td>
                            <p class="text-secondary text-bold">Phone2: </p>
                        </td>
                         <td>
                   <span>  <asp:TextBox ID="txtphone2" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                         </td>
                         <td></td>
                         <td></td>
                    </tr>
                </table>
                
            
            </div>

                    </div>

                   
        <div class="content">
                          <h5 class="hint-text">Next of Kin Details</h5>
            <hr />
            <div>
                <table width="100%">

                         <tr>
                        <td><p class="text-secondary text-bold">Next of Kin: </p></td>
                        <td>
                        <span>  <asp:TextBox ID="txtnokname" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                        </td>
                        <td><p class="text-secondary text-bold">Nok address: </p></td>
                        <td>
                        <span>  <asp:TextBox ID="txtnokaddress" TextMode="MultiLine" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                        </td>
                    </tr>

                        
                    <tr>
                        <td><p class="text-secondary text-bold">Nok Relationship: </p></td>
                         <td>    <span>  <asp:TextBox ID="txtnokrelate" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span></td>
                       
                    </tr>

                     
                </table>
                
            
            </div>

                    </div>
                 
         <div class="content">
                          <h5 class="hint-text">Official Details</h5>
            <hr />
          
                <table width="100%">

                         <tr>
                        <td><p class="text-secondary text-bold">Select Official Type: </p></td>
                        <td>
                        <span>
                            <asp:DropDownList ID="ddltype" AutoPostBack="true" CssClass="text-secondary" Width="150px" OnSelectedIndexChanged="ddltype_SelectedIndexChanged" runat="server">
                                <asp:ListItem Enabled="true" Value="null">-----Select------</asp:ListItem>
                                 <asp:ListItem Value="TEACHING">TEACHING</asp:ListItem>
                                 <asp:ListItem Value="NON-TEACHING">NON-TEACHING</asp:ListItem>
                            </asp:DropDownList>
                        </span>

                        </td>
                     
                    </tr>


                     
                </table>
                
                <asp:Panel ID="pnlteaching" Visible="false" runat="server">
                    <table width="100%">
                        <tr>
                            <td><p class="text-secondary text-bold">Select Department: </p></td>
                            <td>
                                <asp:DropDownList ID="ddldepartments" AutoPostBack="true" OnSelectedIndexChanged="ddldepartments_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </td>
                            <td><p class="text-secondary text-bold">Select Subject(s): </p></td>
                            <td> <asp:DropDownList ID="ddlsubjects" runat="server"></asp:DropDownList></td>
                        </tr>
                      
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlnonteaching" Visible="false" runat="server">
                    <table width="100%">
                        <tr>
                            <td><p class="text-secondary text-bold">Select Section: </p></td>
                            <td>
                                <asp:DropDownList ID="ddlsection" runat="server">
                                    <asp:ListItem>DRIVER</asp:ListItem>
                                     <asp:ListItem>CLEANER</asp:ListItem>
                                     <asp:ListItem>GATEMAN</asp:ListItem>
                                     <asp:ListItem>DELIVERYMAN</asp:ListItem>
                                    <asp:ListItem>HOSTEL KEEPER</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <td></td>
                      
                        </tr>
                    
                    </table>
                </asp:Panel>

                               </div>
         <div class="content">
                          <h5 class="hint-text">Qualification Details</h5>
            <hr />
  <Table width="100%">
                               <tr>
                            <td><p class="text-secondary text-bold">Qualification: </p></td>
                              <td> <span>  <asp:TextBox ID="txtqualification" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span></td>
                              <td><span class="text-secondary text-bold">Instititution Obtained: </span></td>
                              <td><span>  <asp:TextBox ID="txtobtained" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span></td>
                        </tr>
                        <tr>
                            <td><p class="text-secondary text-bold">Start Date: </p></td>
                             <td>
                                  <dx:ASPxDateEdit ID="txtstartdate" CssClass="text-small" EditFormatString="dd-MMM-yyyy" Height="20px" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="110px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                             </td>
                             <td><p class="text-secondary text-bold">End Date: </p></td>
                             <td>
                                  <dx:ASPxDateEdit ID="txtenddate" CssClass="text-small" EditFormatString="dd-MMM-yyyy" Height="20px" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="110px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit>
                             </td>
                            <td><span> <asp:Button ID="btnadd"  OnClick="btnadd_Click" CssClass="button mini-button bg-darkViolet fg-white" Width="50px" runat="server" Text="Add" /></span></td>
                        </tr>
                         </Table>
                    <asp:DataGrid ID="DataGrid1" Visible="true" runat="server" CssClass="Grid2 text-small table striped hovered" CellPadding="3" Width="100%">
                                 <HeaderStyle CssClass="bg-darkBlue fg-white text-small" Height="15px"></HeaderStyle>
                 <FooterStyle CssClass="bg-grayLighter" />
                   
                                <Columns>
                                    <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                                </Columns>
                            </asp:DataGrid>
             </div>
                        
                       

                     
                        
       

                    
            
       
         <div class="align-center">
                   <span>  &nbsp;<asp:Button ID="cmdsave" OnClick="cmdsave_Click" runat="server" CssClass="button bg-green fg-white small-button" Text="Save" /></span>

                                 <span>  <button runat="server" id="btncancel" onserverclick="btncancel_ServerClick" class="button bg-red fg-white small-button"><span class=" mif-cancel"></span> Cancel</button></span>

                </div>
</div>

</asp:Content>

