﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Accessdenied.aspx.vb" Inherits="Accessdenied" %>

    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

    <head runat="server">
        <title>Access Denied</title>

        <link href="/css/metro.css" rel="stylesheet" />
        <link href="/css/metro-icons.css" rel="stylesheet" />
        <link href="/css/metro-responsive.css" rel="stylesheet" />
        <link href="/css/metro-rtl.css" rel="Stylesheet" />
        <link href="/css/docs.css" rel="stylesheet" />
        <link href="/css/grid.css" rel="stylesheet" />
        <link href="/css/metro-schemes.css" rel="Stylesheet" />
        <script src="/Controls/popups.js" type="text/javascript"></script>
        <script src="/js/Gridviewcheck.js" type="text/javascript"></script>
        <script src="/js/docs.js" type="text/javascript"></script>
        <script src="/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="/js/metro.js" type="text/javascript"></script>
        <script src="/js/ga.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://google-code-prettify.googlecode.com/svn/loader/prettify.css" />
        <%--    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" type="text/javascript"></script>--%>
            <meta charset="UTF-8" content="IE=edge" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    </head>

    <body style=" background: url(/images/back.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

        <form runat="server">

            <header class="app-bar fixed-top bg-darkCobalt" data-role="appbar">
                <%--<div class="container">--%>
                    <a href="/" class="app-bar-element branding"><img src="/images/wn8.png" style="height: 28px; display: inline-block; margin-right: 10px;"> E-school</a>



                    <div class="app-bar-element place-right active-container">
                        <span id="welcome" runat="server" class="dropdown-toggle text-secondary text-bold">Logout Here <span class="mif-user"></span> </span>
                        <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" data-no-close="true" style="width: 220px; display: block;">

                            <ul class="unstyled-list fg-dark">
                                <li><a href="#" class="fg-white1 text-secondary text-bold fg-hover-yellow">Profile</a></li>
                                <li><a href="#" class="fg-white2 text-secondary text-bold fg-hover-yellow">Security</a></li>
                                <li><a href="/Login.aspx?msg=logoff" class="fg-white3 text-secondary text-bold fg-hover-yellow">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <span class="app-bar-pull"></span>

                    <div class="app-bar-pullbutton automatic" style="display: none;"></div>
                    <div class="clearfix" style="width: 0;"></div>
                    <nav class="app-bar-pullmenu hidden flexstyle-app-bar-menu" style="display: none;">
                        <ul class="app-bar-pullmenubar hidden app-bar-menu"></ul>
                    </nav>
                    <%--</div>--%>

            </header>
            <div style="width:1100px;" class="container clear-float page-content bg-white">



                <div style="background-color:#00CCFF;">
                    <img src="images/access-denied.png" style="margin-left:130px;" width="70%" height="70%" />
                </div>




            </div>

            <footer style="width:1100px;" class="container">
                <div class="bottom-menu-wrapper bg-darkBlue">
                    <ul class="horizontal-menu compact fg-white">
                        <li><a>&copy; 2014 E-School</a></li>

                    </ul>
                </div>
            </footer>


        </form>
        <script>
            $(function() {
                $("#panel").panel();
            });
        </script>
        <script>
            $(function() {
                $("#accordion").accordion();
            });
        </script>


        <script>
            $(function() {
                $("#datepicker").datepicker();
            });
        </script>



    </body>

    </html>