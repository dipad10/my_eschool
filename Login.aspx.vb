﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Login
    Inherits System.Web.UI.Page
    Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Enrollment_Net10ConnectionString").ConnectionString)
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If _Connection.State = ConnectionState.Open Then
            _Connection.Close()
        End If

        If Not Page.IsPostBack Then

            Dim message As String = Request.QueryString("msg")
            If message <> "" Then
                Response.Write("<script>alert('You have logged out successfully');</script>")
                Session("uname") = ""


            End If
        End If
    End Sub

    Protected Sub Btnsignin_Click(sender As Object, e As EventArgs) Handles Btnsignin.Click
        Try
           
            _Connection.Open()
            Dim _cmd As SqlCommand = _Connection.CreateCommand()
            _cmd.CommandType = CommandType.Text
            _cmd.CommandText = "select * from SecUsers where Username=( '" + Txtusername.Text + "') and Password=( '" + txtpassword.Text + "')"
            _cmd.ExecuteNonQuery()
            Dim dr As SqlDataReader
            dr = _cmd.ExecuteReader()
            If dr.Read() Then
                Session("uname") = Txtusername.Text
                Session("password") = txtpassword.Text

                If dr("permission").ToString() = "Admin" Then
                    Response.Redirect("/Admin/Dashboard.aspx")
                ElseIf dr("permission").ToString() = "Accountant" Then
                    Response.Redirect("/Admin/Accounts/Receivables.aspx")
                ElseIf dr("permission").ToString() = "Registrar" Then
                    Response.Redirect("/Admin/StudentReg/Students.aspx")
                End If
            Else
                Response.Write("<script>alert('Invalid username or Password');</script>")
            End If

            _Connection.Close()
            _Connection.Dispose()
            dr.Close()

        Catch ex As Exception
            Response.Write("<script>alert('An error Occured: " + ex.Message + "');</script>")
        End Try
       
    End Sub
End Class
