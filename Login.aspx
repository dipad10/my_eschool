﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login to Eazi-school</title>
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500" />
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/css/form-elements.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
     <link rel="shortcut icon" href="assets/ico/favicon.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png" />
       <script src="/assets/js/jquery-1.11.1.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/assets/js/jquery.backstretch.min.js"></script>
        <script src="/assets/js/scripts.js"></script>
</head>
<body>
    <form id="form1" style="display:inline;" runat="server">

    
  
    
     <div class="top-content">
        
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Eazi-School</strong> Login</h1>
                            <div class="description">
                            	
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			
                            		<h4 class="text-info">Enter your username and password to log on:</h4>
<asp:Label ID="Label1" runat="server" ForeColor="red"></asp:Label>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <div class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username"></label>
<asp:TextBox placeholder="Username" ID="Txtusername" CssClass="form-username form-control" runat="server" Text=""></asp:TextBox>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password"></label>
<asp:TextBox ID="txtpassword" placeholder="password" CssClass="form-password form-control" runat="server" TextMode="Password" Text=""></asp:TextBox>
			                        </div>
			                       <asp:Button ID="Btnsignin" CssClass="btn" runat="server" Text="Log In" />
			                    </div>
		                    </div>
                        </div>
                    </div>
                   
                </div>
            </div>
            
        </div>


   
        </form>
</body>
</html>
