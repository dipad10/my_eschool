﻿Public Class Student_profile
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents TableStyle As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Private WithEvents LavenderStyle As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents formattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Private WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents LightBlue As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents White As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents TableHeaderStyle As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents studentsID As DevExpress.XtraReports.Parameters.Parameter
    Private WithEvents sqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Private WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents sqlDataSource2 As DevExpress.DataAccess.Sql.SqlDataSource
    Private WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Private WithEvents xrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel32 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLine1 As DevExpress.XtraReports.UI.XRLine
    Private WithEvents xrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "Student_profile.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.Student_profile.ResourceManager
        Me.components = New System.ComponentModel.Container()
        Dim dynamicListLookUpSettings1 As DevExpress.XtraReports.Parameters.DynamicListLookUpSettings = New DevExpress.XtraReports.Parameters.DynamicListLookUpSettings()
        Dim storedProcQuery1 As DevExpress.DataAccess.Sql.StoredProcQuery = New DevExpress.DataAccess.Sql.StoredProcQuery()
        Dim queryParameter1 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim tableQuery1 As DevExpress.DataAccess.Sql.TableQuery = New DevExpress.DataAccess.Sql.TableQuery()
        Dim tableInfo1 As DevExpress.DataAccess.Sql.TableInfo = New DevExpress.DataAccess.Sql.TableInfo()
        Dim columnInfo1 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim columnInfo2 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim columnInfo3 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim columnInfo4 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim columnInfo5 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim xrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim xrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim xrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.TableStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.LavenderStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.formattingRule1 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.LightBlue = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.White = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.TableHeaderStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.studentsID = New DevExpress.XtraReports.Parameters.Parameter()
        Me.sqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.sqlDataSource2 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.xrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'TableStyle
        '
        Me.TableStyle.BackColor = System.Drawing.Color.White
        Me.TableStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TableStyle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
        Me.TableStyle.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.TableStyle.Font = New System.Drawing.Font("Calibri", 36.0!)
        Me.TableStyle.ForeColor = System.Drawing.Color.DarkRed
        Me.TableStyle.Name = "TableStyle"
        Me.TableStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'PageFooter
        '
        Me.PageFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageFooter.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.PageFooter.HeightF = 100.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'PageHeader
        '
        Me.PageHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageHeader.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrPictureBox1, Me.xrLabel23, Me.xrLabel22, Me.xrLabel18, Me.xrLabel17, Me.xrLabel16, Me.xrLabel15, Me.xrLabel14, Me.xrLabel12, Me.xrLabel11, Me.xrLabel10, Me.xrLabel9, Me.xrLabel8, Me.xrLabel3, Me.xrLabel2, Me.xrLabel13, Me.xrLabel19, Me.xrLabel20, Me.xrLabel21, Me.xrLabel7, Me.xrLabel5, Me.xrLabel4, Me.xrLabel6})
        Me.PageHeader.HeightF = 403.625!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.StylePriority.UseBackColor = False
        Me.PageHeader.StylePriority.UseBorderColor = False
        '
        'LavenderStyle
        '
        Me.LavenderStyle.BackColor = System.Drawing.Color.RosyBrown
        Me.LavenderStyle.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.LavenderStyle.ForeColor = System.Drawing.Color.Black
        Me.LavenderStyle.Name = "LavenderStyle"
        Me.LavenderStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'formattingRule1
        '
        Me.formattingRule1.Name = "formattingRule1"
        '
        'Detail
        '
        Me.Detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Detail.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel30, Me.xrLabel29, Me.xrLabel28, Me.xrLabel27, Me.xrLabel26, Me.xrLabel25, Me.xrLabel24})
        Me.Detail.HeightF = 23.95833!
        Me.Detail.Name = "Detail"
        Me.Detail.OddStyleName = "LightBlue"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupHeader1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("ProductName", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 25.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'LightBlue
        '
        Me.LightBlue.BackColor = System.Drawing.Color.FromArgb(CType(CType(119, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.LightBlue.Name = "LightBlue"
        '
        'White
        '
        Me.White.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(228, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.White.BorderWidth = 0.0!
        Me.White.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.White.ForeColor = System.Drawing.Color.Black
        Me.White.Name = "White"
        Me.White.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'TableHeaderStyle
        '
        Me.TableHeaderStyle.BackColor = System.Drawing.Color.Maroon
        Me.TableHeaderStyle.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold)
        Me.TableHeaderStyle.ForeColor = System.Drawing.Color.White
        Me.TableHeaderStyle.Name = "TableHeaderStyle"
        Me.TableHeaderStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'BottomMargin
        '
        Me.BottomMargin.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BottomMargin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BottomMargin.HeightF = 273.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TopMargin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TopMargin.HeightF = 73.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.StylePriority.UseBackColor = False
        Me.TopMargin.StylePriority.UseBorderColor = False
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'studentsID
        '
        dynamicListLookUpSettings1.DataAdapter = Nothing
        dynamicListLookUpSettings1.DataMember = "StudentRegistration"
        dynamicListLookUpSettings1.DataSource = Me.sqlDataSource2
        dynamicListLookUpSettings1.DisplayMember = "IDNo"
        dynamicListLookUpSettings1.FilterString = "[IDNo] = [IDNo]"
        dynamicListLookUpSettings1.ValueMember = "IDNo"
        Me.studentsID.LookUpSettings = dynamicListLookUpSettings1
        Me.studentsID.Name = "studentsID"
        Me.studentsID.ValueInfo = "BES/2016/0094"
        '
        'sqlDataSource1
        '
        Me.sqlDataSource1.ConnectionName = "Enrollment_Net10ConnectionString"
        Me.sqlDataSource1.Name = "sqlDataSource1"
        storedProcQuery1.Name = "GetStudentProfile"
        queryParameter1.Name = "@IDNo"
        queryParameter1.Type = GetType(DevExpress.DataAccess.Expression)
        queryParameter1.Value = New DevExpress.DataAccess.Expression("[Parameters.studentsID]", GetType(String))
        storedProcQuery1.Parameters.Add(queryParameter1)
        storedProcQuery1.StoredProcName = "GetStudentProfile"
        Me.sqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {storedProcQuery1})
        Me.sqlDataSource1.ResultSchemaSerializable = resources.GetString("sqlDataSource1.ResultSchemaSerializable")
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel1})
        Me.ReportHeader.HeightF = 48.95834!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrLabel1
        '
        Me.xrLabel1.BorderWidth = 0.0!
        Me.xrLabel1.Font = New System.Drawing.Font("Calibri", 21.0!)
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(650.0!, 46.66667!)
        Me.xrLabel1.StyleName = "TableStyle"
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.StylePriority.UseTextAlignment = False
        Me.xrLabel1.Text = "Student Profile"
        Me.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrLabel2
        '
        Me.xrLabel2.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 10.00001!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(133.3333!, 23.0!)
        Me.xrLabel2.StylePriority.UseFont = False
        Me.xrLabel2.Text = "STUDENT REG NO:"
        '
        'xrLabel13
        '
        Me.xrLabel13.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 89.125!)
        Me.xrLabel13.Name = "xrLabel13"
        Me.xrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel13.SizeF = New System.Drawing.SizeF(114.5833!, 23.0!)
        Me.xrLabel13.StylePriority.UseFont = False
        Me.xrLabel13.Text = "ADDRESS:"
        '
        'xrLabel19
        '
        Me.xrLabel19.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 232.2917!)
        Me.xrLabel19.Name = "xrLabel19"
        Me.xrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel19.SizeF = New System.Drawing.SizeF(114.5834!, 23.0!)
        Me.xrLabel19.StylePriority.UseFont = False
        Me.xrLabel19.Text = "NATIONALITY:"
        '
        'xrLabel20
        '
        Me.xrLabel20.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 157.2916!)
        Me.xrLabel20.Name = "xrLabel20"
        Me.xrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel20.SizeF = New System.Drawing.SizeF(114.5833!, 23.0!)
        Me.xrLabel20.StylePriority.UseFont = False
        Me.xrLabel20.Text = "MOBILE NO:"
        '
        'xrLabel21
        '
        Me.xrLabel21.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 52.16665!)
        Me.xrLabel21.Name = "xrLabel21"
        Me.xrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel21.SizeF = New System.Drawing.SizeF(133.3333!, 23.0!)
        Me.xrLabel21.StylePriority.UseFont = False
        Me.xrLabel21.Text = "STUDENT NAME:"
        '
        'xrLabel7
        '
        Me.xrLabel7.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 122.5!)
        Me.xrLabel7.Name = "xrLabel7"
        Me.xrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel7.SizeF = New System.Drawing.SizeF(114.5833!, 23.0!)
        Me.xrLabel7.StylePriority.UseFont = False
        Me.xrLabel7.Text = "PARENT NAME:"
        '
        'xrLabel5
        '
        Me.xrLabel5.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 195.8333!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(114.5834!, 23.0!)
        Me.xrLabel5.StylePriority.UseFont = False
        Me.xrLabel5.Text = "CLASS:"
        '
        'xrLabel4
        '
        Me.xrLabel4.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 370.6251!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(62.49942!, 23.0!)
        Me.xrLabel4.StylePriority.UseFont = False
        Me.xrLabel4.Text = "STATE:"
        '
        'xrLabel6
        '
        Me.xrLabel6.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 333.9583!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(128.1255!, 23.0!)
        Me.xrLabel6.StylePriority.UseFont = False
        Me.xrLabel6.Text = "ADMISSION DATE:"
        '
        'xrLabel3
        '
        Me.xrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.IDNo")})
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(143.3333!, 10.00001!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(180.2083!, 23.0!)
        Me.xrLabel3.Text = "xrLabel3"
        '
        'xrLabel8
        '
        Me.xrLabel8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.StudentName")})
        Me.xrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(143.3333!, 52.16665!)
        Me.xrLabel8.Name = "xrLabel8"
        Me.xrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel8.SizeF = New System.Drawing.SizeF(180.2083!, 23.0!)
        Me.xrLabel8.Text = "xrLabel8"
        '
        'xrLabel9
        '
        Me.xrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Address")})
        Me.xrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(124.5833!, 89.125!)
        Me.xrLabel9.Name = "xrLabel9"
        Me.xrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel9.SizeF = New System.Drawing.SizeF(525.4167!, 23.0!)
        Me.xrLabel9.Text = "xrLabel9"
        '
        'xrLabel10
        '
        Me.xrLabel10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Parentname")})
        Me.xrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(124.5833!, 122.5!)
        Me.xrLabel10.Name = "xrLabel10"
        Me.xrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel10.SizeF = New System.Drawing.SizeF(328.125!, 22.99999!)
        Me.xrLabel10.Text = "xrLabel10"
        '
        'xrLabel11
        '
        Me.xrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Mobilephone")})
        Me.xrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(124.5833!, 157.2916!)
        Me.xrLabel11.Name = "xrLabel11"
        Me.xrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel11.SizeF = New System.Drawing.SizeF(198.9583!, 23.0!)
        Me.xrLabel11.Text = "xrLabel11"
        '
        'xrLabel12
        '
        Me.xrLabel12.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Field2")})
        Me.xrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(124.5835!, 195.8333!)
        Me.xrLabel12.Name = "xrLabel12"
        Me.xrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel12.SizeF = New System.Drawing.SizeF(147.9167!, 23.0!)
        Me.xrLabel12.Text = "xrLabel12"
        '
        'xrLabel14
        '
        Me.xrLabel14.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Nationality")})
        Me.xrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(124.5835!, 232.2917!)
        Me.xrLabel14.Name = "xrLabel14"
        Me.xrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel14.SizeF = New System.Drawing.SizeF(171.875!, 23.0!)
        Me.xrLabel14.Text = "xrLabel14"
        '
        'xrLabel15
        '
        Me.xrLabel15.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 266.75!)
        Me.xrLabel15.Name = "xrLabel15"
        Me.xrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel15.SizeF = New System.Drawing.SizeF(114.5834!, 23.0!)
        Me.xrLabel15.StylePriority.UseFont = False
        Me.xrLabel15.Text = "DOB:"
        '
        'xrLabel16
        '
        Me.xrLabel16.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.DOB", "{0:dd-MMM-yyyy}")})
        Me.xrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(124.5835!, 266.75!)
        Me.xrLabel16.Name = "xrLabel16"
        Me.xrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel16.SizeF = New System.Drawing.SizeF(147.9166!, 23.0!)
        Me.xrLabel16.Text = "xrLabel16"
        '
        'xrLabel17
        '
        Me.xrLabel17.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(10.00007!, 299.7917!)
        Me.xrLabel17.Name = "xrLabel17"
        Me.xrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel17.SizeF = New System.Drawing.SizeF(114.5834!, 23.0!)
        Me.xrLabel17.StylePriority.UseFont = False
        Me.xrLabel17.Text = "TYPE:"
        '
        'xrLabel18
        '
        Me.xrLabel18.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Enrollment_type")})
        Me.xrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(124.5833!, 299.7917!)
        Me.xrLabel18.Name = "xrLabel18"
        Me.xrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel18.SizeF = New System.Drawing.SizeF(182.2917!, 23.0!)
        Me.xrLabel18.Text = "xrLabel18"
        '
        'xrLabel22
        '
        Me.xrLabel22.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.AdmissionDate", "{0:dd-MMM-yyyy}")})
        Me.xrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(138.1256!, 333.9583!)
        Me.xrLabel22.Name = "xrLabel22"
        Me.xrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel22.SizeF = New System.Drawing.SizeF(134.3745!, 23.0!)
        Me.xrLabel22.Text = "xrLabel22"
        '
        'xrLabel23
        '
        Me.xrLabel23.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.State")})
        Me.xrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(72.4995!, 370.6251!)
        Me.xrLabel23.Name = "xrLabel23"
        Me.xrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel23.SizeF = New System.Drawing.SizeF(200.0006!, 23.0!)
        Me.xrLabel23.Text = "xrLabel23"
        '
        'xrTable2
        '
        Me.xrTable2.BackColor = System.Drawing.Color.White
        Me.xrTable2.ForeColor = System.Drawing.Color.Black
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow2})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(700.0!, 25.0!)
        Me.xrTable2.StyleName = "TableHeaderStyle"
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell4, Me.xrTableCell3, Me.xrTableCell7, Me.xrTableCell1, Me.xrTableCell2, Me.xrTableCell5, Me.xrTableCell6})
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 1.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell4.StylePriority.UseFont = False
        Me.xrTableCell4.StylePriority.UsePadding = False
        Me.xrTableCell4.Text = "Transaction Date"
        Me.xrTableCell4.Weight = 1.1908791504637541R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell3.StylePriority.UseFont = False
        Me.xrTableCell3.StylePriority.UsePadding = False
        Me.xrTableCell3.Text = "Buss. Type"
        Me.xrTableCell3.Weight = 1.2677107838681489R
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell7.StyleName = "TableHeaderStyle"
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.StylePriority.UsePadding = False
        Me.xrTableCell7.Text = "Trans. Type"
        Me.xrTableCell7.Weight = 1.22929494870663R
        '
        'xrLabel24
        '
        Me.xrLabel24.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.TDate", "{0:dd-MMM-yyyy}")})
        Me.xrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel24.Name = "xrLabel24"
        Me.xrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel24.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel24.Text = "xrLabel24"
        '
        'xrLabel25
        '
        Me.xrLabel25.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.BusinessType")})
        Me.xrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
        Me.xrLabel25.Name = "xrLabel25"
        Me.xrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel25.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel25.Text = "xrLabel25"
        '
        'xrLabel26
        '
        Me.xrLabel26.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.TransType")})
        Me.xrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
        Me.xrLabel26.Name = "xrLabel26"
        Me.xrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel26.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel26.Text = "xrLabel26"
        '
        'xrTableCell1
        '
        Me.xrTableCell1.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell1.Name = "xrTableCell1"
        Me.xrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell1.StylePriority.UseFont = False
        Me.xrTableCell1.StylePriority.UsePadding = False
        Me.xrTableCell1.Text = "Receipt No."
        Me.xrTableCell1.Weight = 1.22929494870663R
        '
        'xrLabel27
        '
        Me.xrLabel27.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.ReceiptNo")})
        Me.xrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(300.0!, 0.0!)
        Me.xrLabel27.Name = "xrLabel27"
        Me.xrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel27.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel27.Text = "xrLabel27"
        '
        'xrTableCell2
        '
        Me.xrTableCell2.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell2.Name = "xrTableCell2"
        Me.xrTableCell2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell2.StylePriority.UseFont = False
        Me.xrTableCell2.StylePriority.UsePadding = False
        Me.xrTableCell2.Text = "Bill"
        Me.xrTableCell2.Weight = 1.22929494870663R
        '
        'xrLabel28
        '
        Me.xrLabel28.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Bill", "{0:n}")})
        Me.xrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
        Me.xrLabel28.Name = "xrLabel28"
        Me.xrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel28.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel28.Text = "xrLabel28"
        '
        'xrTableCell5
        '
        Me.xrTableCell5.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell5.Name = "xrTableCell5"
        Me.xrTableCell5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell5.StylePriority.UseFont = False
        Me.xrTableCell5.StylePriority.UsePadding = False
        Me.xrTableCell5.Text = "Amt. Paid"
        Me.xrTableCell5.Weight = 1.22929494870663R
        '
        'xrLabel29
        '
        Me.xrLabel29.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Amountpaid", "{0:n}")})
        Me.xrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(500.0001!, 0.0!)
        Me.xrLabel29.Name = "xrLabel29"
        Me.xrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel29.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel29.Text = "xrLabel29"
        '
        'xrTableCell6
        '
        Me.xrTableCell6.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell6.Name = "xrTableCell6"
        Me.xrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell6.StylePriority.UseFont = False
        Me.xrTableCell6.StylePriority.UsePadding = False
        Me.xrTableCell6.Text = "Outstanding"
        Me.xrTableCell6.Weight = 1.22929494870663R
        '
        'xrLabel30
        '
        Me.xrLabel30.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Outstandng", "{0:n}")})
        Me.xrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
        Me.xrLabel30.Name = "xrLabel30"
        Me.xrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.xrLabel30.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel30.Text = "xrLabel30"
        '
        'sqlDataSource2
        '
        Me.sqlDataSource2.ConnectionName = "Enrollment_Net10ConnectionString"
        Me.sqlDataSource2.Name = "sqlDataSource2"
        tableQuery1.Name = "StudentRegistration"
        tableInfo1.Name = "StudentRegistration"
        columnInfo1.Name = "IDNo"
        columnInfo2.Name = "FirstName"
        columnInfo3.Name = "MiddleName"
        columnInfo4.Name = "LastName"
        columnInfo5.Name = "SN"
        tableInfo1.SelectedColumns.AddRange(New DevExpress.DataAccess.Sql.ColumnInfo() {columnInfo1, columnInfo2, columnInfo3, columnInfo4, columnInfo5})
        tableQuery1.Tables.AddRange(New DevExpress.DataAccess.Sql.TableInfo() {tableInfo1})
        Me.sqlDataSource2.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {tableQuery1})
        Me.sqlDataSource2.ResultSchemaSerializable = resources.GetString("sqlDataSource2.ResultSchemaSerializable")
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLine1, Me.xrLabel34, Me.xrLabel33, Me.xrLabel32, Me.xrLabel31})
        Me.GroupFooter1.HeightF = 46.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'xrLabel31
        '
        Me.xrLabel31.Font = New System.Drawing.Font("Garamond", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 4.500008!)
        Me.xrLabel31.Name = "xrLabel31"
        Me.xrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel31.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel31.StylePriority.UseFont = False
        Me.xrLabel31.Text = "TOTAL:"
        '
        'xrLabel32
        '
        Me.xrLabel32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Bill")})
        Me.xrLabel32.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
        Me.xrLabel32.Name = "xrLabel32"
        Me.xrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel32.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel32.StylePriority.UseFont = False
        xrSummary3.FormatString = "{0:n}"
        xrSummary3.IgnoreNullValues = True
        xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.xrLabel32.Summary = xrSummary3
        '
        'xrLabel33
        '
        Me.xrLabel33.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Amountpaid")})
        Me.xrLabel33.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(500.0001!, 0.0!)
        Me.xrLabel33.Name = "xrLabel33"
        Me.xrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel33.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel33.StylePriority.UseFont = False
        xrSummary2.FormatString = "{0:n}"
        xrSummary2.IgnoreNullValues = True
        xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.xrLabel33.Summary = xrSummary2
        '
        'xrLabel34
        '
        Me.xrLabel34.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "GetStudentProfile.Outstandng")})
        Me.xrLabel34.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold)
        Me.xrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
        Me.xrLabel34.Name = "xrLabel34"
        Me.xrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel34.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel34.StylePriority.UseFont = False
        xrSummary1.FormatString = "{0:n}"
        xrSummary1.IgnoreNullValues = True
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.xrLabel34.Summary = xrSummary1
        '
        'xrLine1
        '
        Me.xrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
        Me.xrLine1.Name = "xrLine1"
        Me.xrLine1.SizeF = New System.Drawing.SizeF(678.9999!, 23.0!)
        '
        'xrPictureBox1
        '
        Me.xrPictureBox1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Image", Nothing, "GetStudentProfile.Picture")})
        Me.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.MiddleCenter
        Me.xrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(453.125!, 10.00001!)
        Me.xrPictureBox1.Name = "xrPictureBox1"
        Me.xrPictureBox1.SizeF = New System.Drawing.SizeF(108.3334!, 79.12499!)
        Me.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze
        '
        'Student_profile
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.GroupHeader1, Me.PageHeader, Me.PageFooter, Me.ReportHeader, Me.GroupFooter1})
        Me.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.sqlDataSource1, Me.sqlDataSource2})
        Me.DataMember = "GetStudentProfile"
        Me.DataSource = Me.sqlDataSource1
        Me.Font = New System.Drawing.Font("Arial Narrow", 9.75!)
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.formattingRule1})
        Me.Margins = New System.Drawing.Printing.Margins(71, 77, 73, 273)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.studentsID})
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.LightBlue, Me.TableHeaderStyle, Me.TableStyle, Me.White, Me.LavenderStyle})
        Me.Version = "15.2"
        Me.Watermark.Image = CType(resources.GetObject("Student_profile.Watermark.Image"), System.Drawing.Image)
        Me.Watermark.ImageTransparency = 200
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

End Class