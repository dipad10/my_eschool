﻿Public Class Get_student_list_by_class
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Private WithEvents xrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrTableCell12 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Private WithEvents xrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Private WithEvents xrTable2 As DevExpress.XtraReports.UI.XRTable
    Private WithEvents White As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents LightBlue As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents LavenderStyle As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents formattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Private WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Private WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private WithEvents TableHeaderStyle As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents TableStyle As DevExpress.XtraReports.UI.XRControlStyle
    Private WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Private WithEvents sqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Private WithEvents xrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents xrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents sqlDataSource2 As DevExpress.DataAccess.Sql.SqlDataSource
    Private WithEvents Classes As DevExpress.XtraReports.Parameters.Parameter

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resourceFileName As String = "Get_student_list_by_class.resx"
        Dim resources As System.Resources.ResourceManager = Global.Resources.Get_student_list_by_class.ResourceManager
        Me.components = New System.ComponentModel.Container()
        Dim tableQuery1 As DevExpress.DataAccess.Sql.TableQuery = New DevExpress.DataAccess.Sql.TableQuery()
        Dim tableInfo1 As DevExpress.DataAccess.Sql.TableInfo = New DevExpress.DataAccess.Sql.TableInfo()
        Dim columnInfo1 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim columnInfo2 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim columnInfo3 As DevExpress.DataAccess.Sql.ColumnInfo = New DevExpress.DataAccess.Sql.ColumnInfo()
        Dim storedProcQuery1 As DevExpress.DataAccess.Sql.StoredProcQuery = New DevExpress.DataAccess.Sql.StoredProcQuery()
        Dim queryParameter1 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim dynamicListLookUpSettings1 As DevExpress.XtraReports.Parameters.DynamicListLookUpSettings = New DevExpress.XtraReports.Parameters.DynamicListLookUpSettings()
        Me.sqlDataSource2 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.xrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.White = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.LightBlue = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.LavenderStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.formattingRule1 = New DevExpress.XtraReports.UI.FormattingRule()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.xrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.TableHeaderStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.TableStyle = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.sqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.Classes = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'sqlDataSource2
        '
        Me.sqlDataSource2.ConnectionName = "Enrollment_Net10ConnectionString"
        Me.sqlDataSource2.Name = "sqlDataSource2"
        tableQuery1.Name = "Classes"
        tableInfo1.Name = "Classes"
        columnInfo1.Name = "SN"
        columnInfo2.Name = "Class"
        columnInfo3.Name = "Teacher"
        tableInfo1.SelectedColumns.AddRange(New DevExpress.DataAccess.Sql.ColumnInfo() {columnInfo1, columnInfo2, columnInfo3})
        tableQuery1.Tables.AddRange(New DevExpress.DataAccess.Sql.TableInfo() {tableInfo1})
        Me.sqlDataSource2.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {tableQuery1})
        Me.sqlDataSource2.ResultSchemaSerializable = resources.GetString("sqlDataSource2.ResultSchemaSerializable")
        '
        'PageHeader
        '
        Me.PageHeader.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageHeader.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel1})
        Me.PageHeader.HeightF = 80.20835!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.StylePriority.UseBackColor = False
        Me.PageHeader.StylePriority.UseBorderColor = False
        '
        'xrLabel1
        '
        Me.xrLabel1.BorderWidth = 0.0!
        Me.xrLabel1.Font = New System.Drawing.Font("Calibri", 20.0!)
        Me.xrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 38.75001!)
        Me.xrLabel1.Name = "xrLabel1"
        Me.xrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.xrLabel1.SizeF = New System.Drawing.SizeF(650.0!, 41.45834!)
        Me.xrLabel1.StyleName = "TableStyle"
        Me.xrLabel1.StylePriority.UseFont = False
        Me.xrLabel1.Text = "Student list by class"
        '
        'xrTableCell12
        '
        Me.xrTableCell12.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell12.Name = "xrTableCell12"
        Me.xrTableCell12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell12.StylePriority.UseFont = False
        Me.xrTableCell12.StylePriority.UsePadding = False
        Me.xrTableCell12.Text = "Enrollment Type"
        Me.xrTableCell12.Weight = 1.4757684712560988R
        '
        'xrTableCell8
        '
        Me.xrTableCell8.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell8.Name = "xrTableCell8"
        Me.xrTableCell8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell8.StylePriority.UseFont = False
        Me.xrTableCell8.StylePriority.UsePadding = False
        Me.xrTableCell8.Text = "Admission Date"
        Me.xrTableCell8.Weight = 1.6943374196927992R
        '
        'xrTableRow2
        '
        Me.xrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.xrTableCell4, Me.xrTableCell3, Me.xrTableCell7, Me.xrTableCell8, Me.xrTableCell12})
        Me.xrTableRow2.Name = "xrTableRow2"
        Me.xrTableRow2.Weight = 1.0R
        '
        'xrTableCell4
        '
        Me.xrTableCell4.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell4.Name = "xrTableCell4"
        Me.xrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell4.StylePriority.UseFont = False
        Me.xrTableCell4.StylePriority.UsePadding = False
        Me.xrTableCell4.Text = "Reg NO."
        Me.xrTableCell4.Weight = 1.4853570466572541R
        '
        'xrTableCell3
        '
        Me.xrTableCell3.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell3.Name = "xrTableCell3"
        Me.xrTableCell3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell3.StylePriority.UseFont = False
        Me.xrTableCell3.StylePriority.UsePadding = False
        Me.xrTableCell3.Text = "Firstname"
        Me.xrTableCell3.Weight = 1.6065655694919694R
        '
        'xrTableCell7
        '
        Me.xrTableCell7.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrTableCell7.Name = "xrTableCell7"
        Me.xrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 4, 0, 100.0!)
        Me.xrTableCell7.StyleName = "TableHeaderStyle"
        Me.xrTableCell7.StylePriority.UseFont = False
        Me.xrTableCell7.StylePriority.UsePadding = False
        Me.xrTableCell7.Text = "Lastname"
        Me.xrTableCell7.Weight = 1.7283884774599834R
        '
        'xrTable2
        '
        Me.xrTable2.BackColor = System.Drawing.Color.White
        Me.xrTable2.ForeColor = System.Drawing.Color.Black
        Me.xrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrTable2.Name = "xrTable2"
        Me.xrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.xrTableRow2})
        Me.xrTable2.SizeF = New System.Drawing.SizeF(650.0!, 25.0!)
        Me.xrTable2.StyleName = "TableHeaderStyle"
        '
        'White
        '
        Me.White.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(228, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.White.BorderWidth = 0.0!
        Me.White.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.White.ForeColor = System.Drawing.Color.Black
        Me.White.Name = "White"
        Me.White.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupHeader1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrTable2})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("ProductName", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 25.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'LightBlue
        '
        Me.LightBlue.BackColor = System.Drawing.Color.FromArgb(CType(CType(119, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.LightBlue.Name = "LightBlue"
        '
        'LavenderStyle
        '
        Me.LavenderStyle.BackColor = System.Drawing.Color.RosyBrown
        Me.LavenderStyle.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.LavenderStyle.ForeColor = System.Drawing.Color.Black
        Me.LavenderStyle.Name = "LavenderStyle"
        Me.LavenderStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'formattingRule1
        '
        Me.formattingRule1.Name = "formattingRule1"
        '
        'TopMargin
        '
        Me.TopMargin.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TopMargin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TopMargin.HeightF = 72.91666!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.StylePriority.UseBackColor = False
        Me.TopMargin.StylePriority.UseBorderColor = False
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Detail
        '
        Me.Detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Detail.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLabel6, Me.xrLabel5, Me.xrLabel4, Me.xrLabel3, Me.xrLabel2})
        Me.Detail.HeightF = 25.0!
        Me.Detail.Name = "Detail"
        Me.Detail.OddStyleName = "LightBlue"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLabel6
        '
        Me.xrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Getallstudentsbyclass.Enrollment_type")})
        Me.xrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(529.95!, 0.0!)
        Me.xrLabel6.Name = "xrLabel6"
        Me.xrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel6.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel6.Text = "xrLabel6"
        '
        'xrLabel5
        '
        Me.xrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Getallstudentsbyclass.LastName")})
        Me.xrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(251.52!, 0.0!)
        Me.xrLabel5.Name = "xrLabel5"
        Me.xrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel5.Text = "xrLabel5"
        '
        'xrLabel4
        '
        Me.xrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Getallstudentsbyclass.FirstName")})
        Me.xrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(120.83!, 0.0!)
        Me.xrLabel4.Name = "xrLabel4"
        Me.xrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel4.Text = "xrLabel4"
        '
        'xrLabel3
        '
        Me.xrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Getallstudentsbyclass.IDNo")})
        Me.xrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.xrLabel3.Name = "xrLabel3"
        Me.xrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel3.Text = "xrLabel3"
        '
        'xrLabel2
        '
        Me.xrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Getallstudentsbyclass.AdmissionDate", "{0:dd-MMM-yyyy}")})
        Me.xrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(392.12!, 0.0!)
        Me.xrLabel2.Name = "xrLabel2"
        Me.xrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.xrLabel2.Text = "xrLabel2"
        '
        'PageFooter
        '
        Me.PageFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PageFooter.Borders = CType((DevExpress.XtraPrinting.BorderSide.Top Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.PageFooter.HeightF = 100.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'TableHeaderStyle
        '
        Me.TableHeaderStyle.BackColor = System.Drawing.Color.Maroon
        Me.TableHeaderStyle.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold)
        Me.TableHeaderStyle.ForeColor = System.Drawing.Color.White
        Me.TableHeaderStyle.Name = "TableHeaderStyle"
        Me.TableHeaderStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'TableStyle
        '
        Me.TableStyle.BackColor = System.Drawing.Color.White
        Me.TableStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.TableStyle.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid
        Me.TableStyle.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.TableStyle.Font = New System.Drawing.Font("Calibri", 36.0!)
        Me.TableStyle.ForeColor = System.Drawing.Color.DarkRed
        Me.TableStyle.Name = "TableStyle"
        Me.TableStyle.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100.0!)
        '
        'BottomMargin
        '
        Me.BottomMargin.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BottomMargin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BottomMargin.HeightF = 272.9167!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'sqlDataSource1
        '
        Me.sqlDataSource1.ConnectionName = "Enrollment_Net10ConnectionString"
        Me.sqlDataSource1.Name = "sqlDataSource1"
        storedProcQuery1.Name = "Getallstudentsbyclass"
        queryParameter1.Name = "@class"
        queryParameter1.Type = GetType(DevExpress.DataAccess.Expression)
        queryParameter1.Value = New DevExpress.DataAccess.Expression("[Parameters.Classes]", GetType(String))
        storedProcQuery1.Parameters.Add(queryParameter1)
        storedProcQuery1.StoredProcName = "Getallstudentsbyclass"
        Me.sqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {storedProcQuery1})
        Me.sqlDataSource1.ResultSchemaSerializable = resources.GetString("sqlDataSource1.ResultSchemaSerializable")
        '
        'Classes
        '
        dynamicListLookUpSettings1.DataAdapter = Nothing
        dynamicListLookUpSettings1.DataMember = "Classes"
        dynamicListLookUpSettings1.DataSource = Me.sqlDataSource2
        dynamicListLookUpSettings1.DisplayMember = "Class"
        dynamicListLookUpSettings1.FilterString = "[Class] = [Class]"
        dynamicListLookUpSettings1.ValueMember = "Class"
        Me.Classes.LookUpSettings = dynamicListLookUpSettings1
        Me.Classes.Name = "Classes"
        '
        'Get_student_list_by_class
        '
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.GroupHeader1, Me.PageHeader, Me.PageFooter})
        Me.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.sqlDataSource1, Me.sqlDataSource2})
        Me.DataMember = "Getallstudentsbyclass"
        Me.DataSource = Me.sqlDataSource1
        Me.Font = New System.Drawing.Font("Arial Narrow", 9.75!)
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.formattingRule1})
        Me.Margins = New System.Drawing.Printing.Margins(100, 100, 73, 273)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Classes})
        Me.RequestParameters = False
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.LightBlue, Me.TableHeaderStyle, Me.TableStyle, Me.White, Me.LavenderStyle})
        Me.Version = "15.2"
        Me.Watermark.Image = CType(resources.GetObject("Get_student_list_by_class.Watermark.Image"), System.Drawing.Image)
        Me.Watermark.ImageTransparency = 175
        CType(Me.xrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

End Class