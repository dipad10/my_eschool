﻿Imports System.Net
Imports System.Text
Imports System.Web.Mail
Imports System.Configuration
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Threading

Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports System.IO.Ports

Imports CrystalDecisions.Shared
Public Module mod_main

    'reference this m_strconnString anywhere u want to call ur connection into d DB cos its now made public
    Public m_strconnString As String = (New Enrollment.DataClassesDataContext).Connection.ConnectionString
    Public settings As schoolsettings

    Public Enum ErrCodeEnum
        NO_ERROR = 0
        INVALID_GUID = 200
        INVALID_PASSWORD = 200
        FORBIDDEN = 407
        INVALID_RANGE = 408
        ACCOUNT_LOCKED = 400
        ACCOUNT_EXPIRED = 400
        GENERIC_ERROR = 100
    End Enum

    Public Class ResponseInfo
        Public ErrorCode As Integer
        Public ErrorMessage As String
        Public ExtraMessage As String
        Public TotalSuccess As Integer
        Public TotalFailure As Integer
        Public TotalCharged As Integer
        Public CurrentBalance As Integer
    End Class

#Region "GET FUNCTIONS"

    Public Function getQueryString(Optional ByVal q As NameValueCollection = Nothing, Optional ByVal SkipParam As String = "") As String
        If q Is Nothing Then q = My.Request.QueryString
        Dim query As String = "", skips() As String = Split(SkipParam, ",")

        For p As Integer = 0 To q.Count - 1
            If Not skips.Contains(q.GetKey(p)) Then
                query &= "&" & q.GetKey(p) & "=" & String.Join("", q.GetValues(p))
            End If
        Next

        If query.StartsWith("&") Then query = query.Remove(0, 1)
        Return query
    End Function
    

    Public Function DecodeFromBase64String(ByVal inputStr As String) As String
        Try
            If Len(inputStr) > 0 And (Len(inputStr) Mod 4) > 0 Then
                inputStr = inputStr & Left("====", (4 - (Len(inputStr) Mod 4)))
            End If
            ' Convert the binary input into Base64 UUEncoded output.
            Dim binaryData() As Byte = System.Convert.FromBase64String(inputStr)
            Return (New System.Text.UnicodeEncoding).GetString(binaryData)
        Catch exp As System.ArgumentNullException
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Structure schoolsettings
        Dim id As Integer
        Dim name As String
        Dim logo As String
        Dim address As String
        Dim website As String

    End Structure

    Public Sub Messagebox(ByVal Page As UI.Page, ByVal message As String)
        Page.Response.Cookies.Add(New HttpCookie("msgbox", message))
    End Sub
    Public Function getAdhocTable(ByVal strSQL As String, ByVal con As Data.Common.DbConnection) As Data.DataTable
        Dim cmd As New SqlCommand(strSQL, con)
        With cmd
            .CommandType = CommandType.Text

            Dim rdr As New SqlDataAdapter(cmd)
            Dim tbl As New DataTable("Table1")
            rdr.Fill(tbl) : Return tbl
        End With
    End Function
#End Region

#Region "CORE SYSTEM FUNCTIONS"
    Public Function _GetResponseStruct(ByVal ErrCode As ErrCodeEnum, Optional ByVal TotalSuccess As Integer = 0, _
     Optional ByVal TotalFailure As Integer = 0, Optional ByVal ErrorMsg As String = "", Optional ByVal ExtraMsg As String = "", Optional ByVal currentBalance As Integer = 0) As ResponseInfo
        Dim res As New ResponseInfo
        With res
            .ErrorCode = ErrCode
            .ErrorMessage = ErrorMsg
            .ExtraMessage = ExtraMsg
            .TotalSuccess = TotalSuccess
            .TotalFailure = TotalFailure
            .CurrentBalance = currentBalance
        End With
        Return res
    End Function
#End Region
End Module
