﻿Imports Microsoft.VisualBasic

Public Module mod_filldropdowns

    Public Sub FillSessions(ByVal cb As DropDownList)
        cb.Items.Clear()
        cb.Items.Add(New ListItem("2015/2016", "2015/2016"))
        cb.Items.Add(New ListItem("2016/2017", "2016/2017"))
        cb.Items.Add(New ListItem("2017/2018", "2017/2018"))
        cb.Items.Add(New ListItem("2018/2019", "2018/2019"))
        cb.Items.Add(New ListItem("2019/2020", "2019/2020"))
        cb.Items.Insert(0, New ListItem("- select -", "NULL"))
    End Sub

End Module
