﻿Imports Microsoft.VisualBasic

Public Class Cls_classes
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.Class) As ResponseInfo
        Try
            DB.Classes.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.Class) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisClass(ByVal SN As String) As Enrollment.Class
        Try
            Return (From R In DB.Classes Where R.SN = SN).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Class

        End Try
    End Function
    Public Function SelectThisClassname(ByVal name As String) As Enrollment.Class
        Try
            Return (From R In DB.Classes Where R.Class = name).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Class

        End Try
    End Function
    Public Function SelectAllclasses() As List(Of Enrollment.Class)
        Try
            Return (From U In DB.Classes).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Class)
        End Try
    End Function
End Class
