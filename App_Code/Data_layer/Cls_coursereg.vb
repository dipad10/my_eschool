﻿Imports Microsoft.VisualBasic

Public Class Cls_coursereg
    Dim DB As Enrollment.DataClassesDataContext

    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub

    Public Function Insert(ByVal G As Enrollment.CourseReg) As ResponseInfo
        Try
            DB.CourseRegs.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As Enrollment.CourseReg) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectAllregs() As List(Of Enrollment.CourseReg)
        Try
            Return (From U In DB.CourseRegs Order By U.SN Descending).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.CourseReg)
        End Try
    End Function

    Public Function SelectThisReg(ByVal IDNo As String) As List(Of Enrollment.CourseReg)
        Try
            Return (From R In DB.CourseRegs Where R.RegID = IDNo).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.CourseReg)

        End Try
    End Function
End Class
