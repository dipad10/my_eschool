﻿Imports Microsoft.VisualBasic

Public Class Cls_subjectallocation
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.SubjectAllocation) As ResponseInfo
        Try
            DB.SubjectAllocations.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.SubjectAllocation) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisclass(ByVal classname As String) As Enrollment.SubjectAllocation
        Try
            Return (From R In DB.SubjectAllocations Where R.class = classname).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.SubjectAllocation

        End Try
    End Function
    
    Public Function SelectAllsubjectallocation() As List(Of Enrollment.SubjectAllocation)
        Try
            Return (From U In DB.SubjectAllocations).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.SubjectAllocation)
        End Try
    End Function

    Public Function Selectsubjectlist(ByVal classname As String) As List(Of Enrollment.SubjectAllocation)
        Try
            Return (From U In DB.SubjectAllocations Where U.class = classname).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.SubjectAllocation)
        End Try
    End Function
End Class
