﻿Imports Microsoft.VisualBasic

Public Class Cls_StudentRegistration
    'Dim DB As linq_layer.DataClassesDataContext
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub

    Public Function Insert(ByVal G As Enrollment.StudentRegistration) As ResponseInfo
        Try
            DB.StudentRegistrations.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.StudentRegistration) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThisIDNo(ByVal IDNo As String) As Enrollment.StudentRegistration
        Try
            Return (From SR In DB.StudentRegistrations Where SR.IDNo = IDNo And SR.Active = 1).ToList(0)
        Catch ex As Exception
            Return New Enrollment.StudentRegistration
        End Try

    End Function

    Public Function SelectAll() As List(Of Enrollment.StudentRegistration)
        Try
            Return (From U In DB.StudentRegistrations Where U.Active = 1 And U.Deleted = 0).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.StudentRegistration)
        End Try
    End Function

    Public Function SelectAllFilter(ByVal Filter As String) As Object
        Try
            Return (From U In DB.StudentRegistrations Where (U.FirstName.Contains(Filter)) Or (U.IDNo.Contains(Filter)) Select U).Take(10).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.StudentRegistration)
        End Try
    End Function
    Public Function SelectThisID(ByVal studentid As String) As List(Of Enrollment.StudentRegistration)
        Try
            Return (From P In DB.StudentRegistrations Where P.IDNo = studentid).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.StudentRegistration)
        End Try
    End Function
End Class
