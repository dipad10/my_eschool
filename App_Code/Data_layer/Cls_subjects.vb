﻿Imports Microsoft.VisualBasic

Public Class Cls_subjects
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.Subject) As ResponseInfo
        Try
            DB.Subjects.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.Subject) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThissubject(ByVal SN As String) As Enrollment.Subject
        Try
            Return (From R In DB.Subjects Where R.SN = SN).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Subject

        End Try
    End Function
    Public Function SelectThissubjectname(ByVal name As String) As Enrollment.Subject
        Try
            Return (From R In DB.Subjects Where R.Subjectname = name).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Subject

        End Try
    End Function
    Public Function SelectAllsubject() As List(Of Enrollment.Subject)
        Try
            Return (From U In DB.Subjects).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Subject)
        End Try
    End Function
End Class
