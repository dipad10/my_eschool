﻿Imports Microsoft.VisualBasic

Public Class Cls_Accounts
    Dim DB As Enrollment.DataClassesDataContext

    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub

    Public Function Insert(ByVal G As Enrollment.Account) As ResponseInfo
        Try
            DB.Accounts.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As Enrollment.Account) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectAllaccounts() As List(Of Enrollment.Account)
        Try
            Return (From U In DB.Accounts Order By U.SN Descending).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Account)
        End Try
    End Function
    Public Function SelectThisIDtypeA(ByVal accountID As String) As Enrollment.Account
        Try
            Return (From R In DB.Accounts Where R.AccountID = accountID And R.Type = "A").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Account

        End Try
    End Function

    Public Function SelectThisIDtypeB(ByVal accountID As String) As Enrollment.Account
        Try
            Return (From R In DB.Accounts Where R.AccountID = accountID And R.Type = "B").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Account

        End Try
    End Function


    Public Function SelectThisID(ByVal accountID As String) As Enrollment.Account
        Try
            Return (From R In DB.Accounts Where R.AccountID = accountID).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Account

        End Try
    End Function
End Class
