﻿Imports Microsoft.VisualBasic

Public Class Cls_secusers
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.SecUser) As ResponseInfo
        Try
            DB.SecUsers.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.SecUser) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal username As String) As Enrollment.SecUser
        Try
            Return (From R In DB.SecUsers Where R.Username = username).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.SecUser

        End Try
    End Function

   
    Public Function SelectThispassword(ByVal password As String) As Enrollment.SecUser
        Try
            Return (From A In DB.SecUsers Where A.Password = password).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.SecUser
        End Try
    End Function


    Public Function SelectAllusers() As List(Of Enrollment.SecUser)
        Try
            Return (From U In DB.SecUsers).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.SecUser)
        End Try
    End Function
End Class
