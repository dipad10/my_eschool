﻿Imports Microsoft.VisualBasic

Public Class Cls_Assetpurchase
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub

    Public Function Delete(ByVal Asset_Code As String) As ResponseInfo
        Try
            Dim row As Enrollment.AssetPurchase = (From A In DB.AssetPurchases Where A.AssetCode = Asset_Code).ToList()(0)
            'row.Deleted = 1
            DB.AssetPurchases.DeleteOnSubmit(row)
            db.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function Insert(ByVal G As Enrollment.AssetPurchase) As ResponseInfo
        Try
            DB.AssetPurchases.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function Update(ByVal A As Enrollment.AssetPurchase) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function UpdateAssetTransferStatus(ByVal AssetCode As String, ByVal vendorname As String, ByVal vendoraddress As String, ByVal transferDate As Date) As ResponseInfo
        Try
            Dim c As Enrollment.AssetPurchase = SelectThis(AssetCode)
            c.VendorName = vendorname
            c.VendorAddress = vendoraddress
            c.TransactionDate = transferDate
            DB.SubmitChanges()

            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function SelectThis(ByVal Asset_Code As String) As Enrollment.AssetPurchase
        Try
            Return (From A In DB.AssetPurchases Where A.AssetCode = Asset_Code And A.Trans_Type = "PUR").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.AssetPurchase
        End Try
    End Function

    Public Function SelectThisDisposal(ByVal Asset_Code As String) As Enrollment.AssetPurchase
        Try
            Return (From A In DB.AssetPurchases Where A.AssetCode = Asset_Code And A.Trans_Type = "DISPOSED").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.AssetPurchase
        End Try
    End Function
End Class
