﻿Imports Microsoft.VisualBasic

Public Class Cls_Aggregates
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub

    Public Function Insert(ByVal G As Enrollment.Aggregate) As ResponseInfo
        Try
            DB.Aggregates.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As Enrollment.Aggregate) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function SelectThis(ByVal RegID As String) As Enrollment.Aggregate
        Try
            Return (From A In DB.Aggregates Where A.RegID = RegID).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Aggregate
        End Try
    End Function

End Class
