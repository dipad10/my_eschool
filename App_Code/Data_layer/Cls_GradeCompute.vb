﻿Imports Microsoft.VisualBasic

Public Class Cls_GradeCompute
    Dim DB As Enrollment.DataClassesDataContext

    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub

    Public Function Insert(ByVal G As Enrollment.GradeCompute) As ResponseInfo
        Try
            DB.GradeComputes.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Update(ByVal R As Enrollment.GradeCompute) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisRegID(ByVal RegID As String) As List(Of Enrollment.GradeCompute)
        Try
            Return (From R In DB.GradeComputes Where R.RegID = RegID).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.GradeCompute)

        End Try
    End Function
    Public Function SelectThis(ByVal RegID As String, ByVal subject As String) As Enrollment.GradeCompute
        Try
            Return (From A In DB.GradeComputes Where A.RegID = RegID And A.Subject = subject).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.GradeCompute
        End Try
    End Function
    Public Function SelectThisIDNew(ByVal RegID As String) As List(Of Enrollment.GradeCompute)
        Try
            Return (From A In DB.GradeComputes Where A.RegID = RegID).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.GradeCompute)
        End Try
    End Function
    Public Function SelectThisSN(ByVal SN As String) As Enrollment.GradeCompute
        Try
            Return (From A In DB.GradeComputes Where A.SN = SN).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.GradeCompute
        End Try
    End Function
End Class
