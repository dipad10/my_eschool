﻿Imports Microsoft.VisualBasic

Public Class Cls_Receivable
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.Receivable) As ResponseInfo
        Try
            DB.Receivables.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.Receivable) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisIdNo(ByVal IDNo As String) As Enrollment.Receivable
        Try
            Return (From R In DB.Receivables Where R.IDNo = IDNo).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Receivable

        End Try
    End Function

    Public Function SelectThisIdNoAll(ByVal IDNo As String) As List(Of Enrollment.Receivable)
        Try
            Return (From R In DB.Receivables Where R.IDNo = IDNo And R.Notetype = "RCP").ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Receivable)

        End Try
    End Function

    Public Function SelectThisByRefRcp(ByVal RefRcp As String) As List(Of Enrollment.Receivable)
        Try
            Return (From A In DB.Receivables Where A.RefRcp = RefRcp And A.Notetype = "RCP").ToList()

        Catch ex As Exception
            Return New List(Of Enrollment.Receivable)
        End Try
    End Function
    Public Function SelectThis(ByVal ReceiptNo As String) As Enrollment.Receivable
        Try
            Return (From A In DB.Receivables Where A.ReceiptNo = ReceiptNo And A.Notetype = "RCP").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Receivable
        End Try
    End Function

    Public Function SelectThisIDandRcp(ByVal idno As String, ByVal receiptno As String) As Enrollment.Receivable
        Try
            Return (From A In DB.Receivables Where A.IDNo = idno And A.ReceiptNo = receiptno).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Receivable
        End Try
    End Function

    Public Function SelectAllFilter(ByVal Filter As String) As Object
        Try
            Return (From U In DB.Receivables Where (U.FirstName.Contains(Filter)) Or (U.ReceiptNo.Contains(Filter)) Select U).Take(10).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Receivable)
        End Try
    End Function
    Public Function SelectAll() As List(Of Enrollment.Receivable)
        Try
            Return (From U In DB.Receivables Where U.Active = 1 And U.Deleted = 0).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Receivable)
        End Try
    End Function

    Public Function SelectThisID(ByVal receivableid As String) As List(Of Enrollment.Receivable)
        Try
            Return (From P In DB.Receivables Where P.ReceiptNo = receivableid).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Receivable)
        End Try
    End Function

    Public Function Deletefinal(ByVal idno As String) As ResponseInfo
        Try
            Dim row As List(Of Enrollment.Receivable) = (From A In DB.Receivables Where A.IDNo = idno).ToList()

            For Each V In row
                DB.Receivables.DeleteOnSubmit(V)
            Next
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function Deleteone(ByVal receiptno As String) As ResponseInfo
        Try
            Dim row As Enrollment.Receivable = (From A In DB.Receivables Where A.ReceiptNo = receiptno).ToList()(0)


            DB.Receivables.DeleteOnSubmit(row)

            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function
End Class
