﻿Imports Microsoft.VisualBasic

Public Class Cls_staffs
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.Staff) As ResponseInfo
        Try
            DB.Staffs.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.Staff) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal staffid As String) As Enrollment.Staff
        Try
            Return (From R In DB.Staffs Where R.StaffID = staffid).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Staff

        End Try
    End Function
    Public Function SelectThisByRefstaffid(ByVal staffid As String) As List(Of Enrollment.Staff)
        Try
            Return (From A In DB.Staffs Where A.StaffID = staffid).ToList()

        Catch ex As Exception
            Return New List(Of Enrollment.Staff)
        End Try
    End Function

    Public Function SelectAllstaffs() As List(Of Enrollment.Staff)
        Try
            Return (From U In DB.Staffs).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Staff)
        End Try
    End Function
    Public Function SelectAllFilter(ByVal Filter As String) As Object
        Try
            Return (From U In DB.Staffs Where (U.Name.Contains(Filter)) Or (U.Remarks.Contains(Filter)) Select U).Take(10).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Staff)
        End Try
    End Function
End Class
