﻿Imports Microsoft.VisualBasic

Public Class Cls_Term
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.Term) As ResponseInfo
        Try
            DB.Terms.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.Term) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisterm(ByVal term As String) As Enrollment.Term
        Try
            Return (From R In DB.Terms Where R.Term = term).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Term

        End Try
    End Function
    Public Function Selectcurrentterm() As Enrollment.Term
        Try
            Return (From R In DB.Terms Where R.Active = "1").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Term

        End Try
    End Function
    Public Function SelectAllterms() As List(Of Enrollment.Term)
        Try
            Return (From U In DB.Terms).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Term)
        End Try
    End Function
End Class
