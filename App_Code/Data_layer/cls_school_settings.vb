﻿Imports Microsoft.VisualBasic

Public Class cls_school_settings

    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub



    Public Function Insert(ByVal G As Enrollment.School_setting) As ResponseInfo
        Try
            DB.School_settings.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.School_setting) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function




    Public Function Selectschool() As List(Of Enrollment.School_setting)
        Try
            Return (From U In DB.School_settings).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.School_setting)
        End Try
    End Function
End Class
