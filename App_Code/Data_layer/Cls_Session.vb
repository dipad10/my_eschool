﻿Imports Microsoft.VisualBasic

Public Class Cls_Session
    Dim DB As Enrollment.DataClassesDataContext
    Public Sub New(Optional ByVal Context As Enrollment.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New Enrollment.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As Enrollment.Session) As ResponseInfo
        Try
            DB.Sessions.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As Enrollment.Session) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThissession(ByVal Session As String) As Enrollment.Session
        Try
            Return (From R In DB.Sessions Where R.Session = Session).ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Session

        End Try
    End Function
    Public Function Selectcurrentsession() As Enrollment.Session
        Try
            Return (From R In DB.Sessions Where R.Active = "1").ToList()(0)
        Catch ex As Exception
            Return New Enrollment.Session

        End Try
    End Function
    Public Function SelectAllsessions() As List(Of Enrollment.Session)
        Try
            Return (From U In DB.Sessions).ToList()
        Catch ex As Exception
            Return New List(Of Enrollment.Session)
        End Try
    End Function
End Class
