﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DateTimeWidget.ascx.vb" Inherits="Docking_Widgets_DateTimeWidget" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<script type="text/javascript">
    // <![CDATA[
    function PrepareTimeValue(value) {
        if (value < 10)
            value = "0" + value;
        return value;
    }
    function UpdateTime(s, e) {
        var dateTime = new Date();
        var timeString = PrepareTimeValue(dateTime.getHours()) + ":" + PrepareTimeValue(dateTime.getMinutes()) + ":" +
            PrepareTimeValue(dateTime.getSeconds());
        timeLabel.SetText(timeString);
    }
    // ]]> 
</script>
<dx:ASPxTimer runat="server" ID="Timer" ClientInstanceName="timer" Interval="1000">
    <ClientSideEvents Init="UpdateTime" Tick="UpdateTime" />
</dx:ASPxTimer>
<div class="timeContainer">
    <dx:ASPxLabel runat="server" ID="TimeLabel" ClientInstanceName="timeLabel" Font-Bold="true"
        Font-Size="X-Large">
    </dx:ASPxLabel>
</div>
<div class="dateContainer">
    <dx:ASPxLabel runat="server" ID="DateLabel" ClientInstanceName="dateLabel" Font-Size="14px">
    </dx:ASPxLabel>
</div>