﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CalendarWidget.ascx.vb" Inherits="Docking_Widgets_CalendarWidget" %>
<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<dx:ASPxCalendar runat="server" ID="Calendar" ShowClearButton="false" ShowHeader="false"
    ShowTodayButton="false" ShowWeekNumbers="false" HighlightToday="false" Width="100%">
    <Border BorderStyle="None" />
</dx:ASPxCalendar>
